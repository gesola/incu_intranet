<?php
header('content-type:text/css');
include ("css_conf.php");
echo <<<FINCSS

.droplinebar{
overflow: hidden;
}

.droplinebar ul{
margin: 0;
padding: 0;
float: left;
width: 100%;
/*font: bold 13px Arial;*/
font: 14px/120% Arial, Helvetica, sans-serif;
/*background: #242c54 url(bluedefault.gif) center center repeat-x; *//*default background of menu bar*/
background: $menu1; /*default background of menu bar*/
border-top: 1px solid #A5BAC8;
}

.droplinebar ul li{
display: inline;
}

.droplinebar ul li a{
float: left;
color: white;
padding: 9px 11px;
text-decoration: none;
}

.droplinebar ul li a:visited{
/*color: white;*/
}

.droplinebar ul li a:hover, .droplinebar ul li .current{ /*background of main menu bar links onMouseover*/
color: white;
background: $menuhov;
}


/* Sub level menus*/
.droplinebar ul li ul{
position: absolute;
z-index: 100;
left: 0;
top: 0;
background: $menu2; /*sub menu background color */
visibility: hidden;
}

/* Sub level menu links style */
.droplinebar ul li ul li a{
font: normal 12px Arial, Helvetica, sans-serif;
color: #666;
padding: 6px;
padding-right: 8px;
margin: 0;
border-bottom: 1px solid navy;
}

.droplinebar ul li ul li a:hover{ /*sub menu links' background color onMouseover */
background: $menuhov;
}

/* Sub Sub level menus*/
.droplinebar ul li ul li ul{
position: absolute;
z-index: 100;
left: 0;
top: 0;
background: $menu3; /*sub sub menu background color */
visibility: hidden;
}


/* Sub Sub level menu links style */
.droplinebar ul li ul li ul li a{
font: normal 10px Arial, Helvetica, sans-serif;
color: $menu1;
padding: 6px;
padding-right: 8px;
margin: 0;
border-bottom: 1px solid navy;
}

.droplinebar ul li ul li ul li a:hover{ /*sub menu links' background color onMouseover */
background: $menu1;
}

FINCSS;
?>