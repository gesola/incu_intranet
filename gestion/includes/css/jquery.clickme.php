<?phpheader('content-type:text/css'); include ("css_conf.php");echo <<<FINCSS/* @group nav */
.containerNav {
	float:left;
	width: 800px;
}
.nav {
	width:800px;
	float:left;
	position: relative;
	background-color: $menu1;
	border-top: 1px solid $menu3;	
}
.nav, .nav ul {
	list-style-type:none;
	padding:0;
	margin: 0;
}
.nav li {
	float:left;
	margin:0;
	padding: 0;
}
.nav a {
	display:block;
	color: $txt1;
	font: 14px/120% Arial, Helvetica, sans-serif;
	padding: 4px 15px 6px 15px;
	text-decoration:none;
}
.nav a:hover {	
	background-color: $menuhov;
}
.nav ul a {
	color: $menuhov;
	padding: 4px 15px 4px 15px;
	font-size: 11px;
	background:$menu2;
	
} 
.nav ul a:hover  {
	background: $menuhov;
	color: $txt1;
	padding: 4px 15px 4px 15px;
}
.nav ul {
	position:absolute;
	padding:0;
	text-transform: none;
	left: 0;
	width: 800px;
	background:$menu2;
	display: none;
}
.current {
	background-color: $menu3;	
}
.current a {
	color: $menuhov;
}
.current a:hover {
	color: $menu1;
	background-color: $menu3;
}
/* @end nav */
FINCSS;?>
