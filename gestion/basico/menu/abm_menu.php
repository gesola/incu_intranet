<?php 
## ABM MENU. ##
//Reanudamos la sesión
@session_start();

$nombre="abm_menu";
$descripcion="ABM del Menu";

// Configuro permisos.
$listado_su=1;
$modifica_su=1;
$alta_su=1;
$elimina_su=1;

$listado_ad=1;
$modifica_ad=1;
$alta_ad=1;
$elimina_ad=1;

$listado_op=1;
$modifica_op=0;
$alta_op=0;
$elimina_op=0;

$listado_us=0;
$modifica_us=0;
$alta_us=0;
$elimina_us=0;

$binario_su=$elimina_su.$alta_su.$modifica_su.$listado_su;
$binario_ad=$elimina_ad.$alta_ad.$modifica_ad.$listado_ad;
$binario_op=$elimina_op.$alta_op.$modifica_op.$listado_op;
$binario_us=$elimina_us.$alta_us.$modifica_us.$listado_us;

$su=bindec($binario_su);
$ad=bindec($binario_ad);
$op=bindec($binario_op);
$us=bindec($binario_us);

// Fin configuración de permisos.

include ("../../includes/estructura.inc");
include ("../../includes/validacion.inc");


##########################
### FUNCION validado() ###
##########################

function validado() {
	if (!isset($_POST['reincide']) || $_POST['reincide']==0)
            {
                main(); // Va al listado.
            } else {
			if (isset($_POST['formulario']) && $_POST['formulario']==1) { dbalta(); } // Voy a insertar registro en DB.
			if (isset($_POST['formulario']) && $_POST['formulario']==2) { dbmodif(); } // Voy a editar registro en DB. 
		    } //fin del if/else de reincide.
} ### FIN FUNCION validado ###


######################
### FUNCION main() ###
######################

function main() {
    
if (!isset($_POST['returning'])) {include ("../../includes/html5_head.inc");
$htitulo="Menu:";
$stitulo="Gesti&oacute;n de Menu";

?>
<!-- page content -->
<!-- page content Head -->
<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3><?php print $htitulo;?> <small><?php print $stitulo;?></small></h3>
              </div>              
            </div>

<?php
}

if (isset($_POST['id'])) {$_POST['id']=$_POST['id'];}

if (!isset($_POST['reincide']))
        {
            if (!isset($_GET['fun'])){listado();} // Voy a listado.
            if (isset($_GET['fun']) && $_GET['fun']==1){listado();} // Voy a listado.    
            if (isset($_GET['fun']) && $_GET['fun']==2)
                {
                    if($_POST["ver"]==1){ver();}// Voy a consultar.
                        else {noauth();}//Voy a noauth.
                }
                
            if (isset($_GET['fun']) && $_GET['fun']==3 )
                {
                    if($_POST["alta"]==1){formulario(1);}// Voy a Alta.
                        else {noauth();}//Voy a noauth.                              
                } 
            
            if (isset($_GET['fun']) && $_GET['fun']==4)
                {
                    if($_POST["modif"]==1){formulario(2);} // Voy a Edicion.
                        else {noauth();}//Voy a noauth.                 
                }
                         
            if (isset($_GET['fun']) && $_GET['fun']==5)
                {
                    if($_POST["elimina"]==1){elimina();} // Voy a Eliminar.
                        else {noauth();}//Voy a noauth.                  
                } 
            
        } else {
            if ($_POST['reincide']==2)
            {
                if (isset($_POST['ver']) && $_POST['ver']==1){
                    ver();  
                    formulario(1);
                } // Muestro nuevo registro creado y formulario de alta.
                if (isset($_POST['ver']) && $_POST['ver']==2){ver();} // Muestro el registro editado.                
                
                if (isset($_POST['falta']) && $_POST['falta']==1){alta();} // Error en ingreso de datos alta.              
            }
        }

?>
            
          </div>
        </div>
<!-- /page content -->    
<?php                      
include ("../../includes/html5_foot_menu.inc");
} ### FIN DE main ###


########################
### FUNCION noauth() ###
########################
function noauth() {

$subtitulo="Menu";  

if (isset($_POST['merror'])) { $mensaje="<span class=\"error\">".$_POST['merror']."</span>";}
if (isset($_POST['mconfirma'])) { $mensaje="<span class=\"listconfirma\">".$_POST['mconfirma']."</span>";}  

?>              
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php if(isset($subtitulo)){print $subtitulo;}?><small><?php if(isset($mensaje)) {print $mensaje;} ?></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>                    
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
   
                  <div class="x_content"> 
                      
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>No tiene permisos para ingresar a este m&oacute;dulo.</h3>
            </div>
        </div>
    </div>
</div>  
                    
<!--                   <div class="ln_solid"></div> -->
                    <div class="col-md-12 col-md-offset-3">
                     <a href="abm_menu.php?fun=1" class="btn btn-primary"></i> Volver </a>
                    </div> 
                  </div>
                 </div>                
              </div>
            </div>
<!--
          </div>
        </div>
-->
<?php
} ### Fin de noauth ###


#########################
### FUNCION listado() ###
#########################

function listado() {
  
$subtitulo="Listado de menu";

if (isset($_POST['merror'])) { $mensaje="<span class=\"error\">".$_POST['merror']."</span>";}
if (isset($_POST['mconfirma'])) { $mensaje="<span class=\"listconfirma\">".$_POST['mconfirma']."</span>";}

$parent=0;
require_once(dirname(__FILE__) . "/../../clases/validacion.class.inc");			    
$miValidacion = new Validacion();

$resultreg = $miValidacion->CreaMenu($parent); 
$cantreg   = $miValidacion->cantidadUltimaTabla();

if ($cantreg==0)
{
?>
<div class="clearfix"></div>    
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2><?php if(isset($subtitulo)){print $subtitulo;}?><small><?php if(isset($mensaje)) {print $mensaje;} ?></small></h2>
            <ul class="nav navbar-right panel_toolbox"> 
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>                   
                <li class="dropdown">
                <?php if(isset($_POST["alta"]) && $_POST["alta"]==1){ ?> <a href="abm_menu.php?fun=3" title="Nuevo Registro"><i class="fa fa-file"></i></a> <?php } ?>
                </li>
                     
                <li><a class="close-link"><i class="fa fa-close"></i></a></li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <h3 align="center"><?php print "NO HAY REGISTROS PARA LISTAR.";?></h3>                
        </div>
    </div>
</div>
<?php 
} else {
?>
            <div class="clearfix"></div>    
                <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php if(isset($subtitulo)){print $subtitulo;}?><small><?php if(isset($mensaje)) {print $mensaje;} ?></small></h2>
                    <ul class="nav navbar-right panel_toolbox"> 
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                      <li class="dropdown">
                        <!--<a href="usuario_alta.php" title="Nuevo Usuario"><i class="fa fa-eye"></i></a>
                         
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                        -->
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                   <!-- <table id="datatable-buttons" class="table table-striped table-bordered"> -->                      
                   <table class="table table-striped table-bordered">                                          
                      <thead>
                        <tr>
                          <th><?php if(isset($_POST["alta"]) && $_POST["alta"]==1){ ?> <a href="abm_menu.php?fun=3" class="btn btn-dark btn-xs" data-toggle="tooltip" data-placement="top" title="Nuevo Registro"><i class="fa fa-file"></i></a> <?php } ?>&nbsp;</th>                     
                          <th>Id</th>
                          <th>Nombre</th>
                          <th>Link</th>
                          <th>Permisos</th>
                        </tr>
                      </thead>
                      <tfoot>
                        <tr>
                          <th>&nbsp;</th>
                          <th>Id</th>
                          <th>Nombre</th>
                          <th>Link</th>
                          <th>Permisos</th>                  
                        </tr>
                      </tfoot>                  
                      <tbody>
<?php
while($rowreg = $miValidacion->fetch_array($resultreg))         
    {
        $level=0;      
        $id=$rowreg["id"];
        $nombre="-&nbsp;".$rowreg["nombre"];
        $link=$rowreg["link"];
        $permisos=$rowreg["su"]." - ".$rowreg["ad"]." - ".$rowreg["op"]." - ".$rowreg["us"];                       
?>
  <tr>
    <td>
      
        <?php

        if(isset($_POST["ver"]) && $_POST["ver"]==1){ ?>
            <a href="abm_menu.php?fun=2&id=<?php print $id;?>" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="Ver"><i class="fa fa-eye"></i></a>
        <?php }

        if(isset($_POST["modif"]) && $_POST["modif"]==1){ ?>
            <a href="abm_menu.php?fun=4&id=<?php print $id;?>" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil"></i></a>
        <?php }

        if($rowreg['Count'] == 0 && isset($_POST["elimina"]) && $_POST["elimina"]==1){
?>                
            <span data-toggle="tooltip" data-placement="top" title="Eliminar">
            <a href="abm_menu.php?fun=5&id=<?php print $id;?>" class="btn btn-danger btn-xs" data-toggle="confirmation" data-btn-ok-label="Continuar" data-btn-ok-icon="glyphicon glyphicon-share-alt"
        data-btn-ok-class="btn-success" data-btn-cancel-label="Cancelar" data-btn-cancel-icon="glyphicon glyphicon-ban-circle" data-btn-cancel-class="btn-danger" 
        data-title="¿Est&aacute; Seguro?" data-content=""><i class="fa fa-trash-o"></i></a>
            </span>         
        <?php } ?>       
    </td>        
    <td><?php echo $id;?></td>   
    <td><?php echo $nombre;?></td>
    <td><?php echo $link;?></td>    
    <td><?php echo $permisos;?></td>    
</tr>

<?php
        $modo=1; // Variable para determinar de que función proviene la llamada a menu_display_children() -- 1=listado 2=formulario
        if ($rowreg['Count'] > 0 ) {  menu_display_children($rowreg['id'], $level + 1, $modo); }        
    } // Fin While 
?>

</tbody>
                    </table>
                  </div>
                </div>
              </div>
<!--            
           </div>
        </div>
-->
        <!-- /page content -->  
<?php       
} // Final del if que muestra listado si es que hay 1 o mas menus de usuarios.

} ### FINAL DE abm_menuplantillalist


#####################
### FUNCION ver() ###
#####################
function ver() {

$subtitulo="Ver menu";  

if (isset($_POST['merror'])) { $mensaje="<span class=\"error\">".$_POST['merror']."</span>";}
if (isset($_POST['mconfirma'])) { $mensaje="<span class=\"listconfirma\">".$_POST['mconfirma']."</span>";}  

?>              
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php if(isset($subtitulo)){print $subtitulo;}?><small><?php if(isset($mensaje)) {print $mensaje;} ?></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>                    
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
   
                  <div class="x_content">        
<?php 
require_once(dirname(__FILE__) . "/../../clases/validacion.class.inc");

$miValidacion = new Validacion();

if (isset($_GET['id'])){$resultusu = $miValidacion->listaItemMenu($_GET['id']); }
if (isset($_POST['id'])){$resultusu = $miValidacion->listaItemMenu($_POST['id']); }
$cantusu = $miValidacion->cantidadUltimaTabla(); 

while($rowusu = $miValidacion->fetch_array($resultusu))
    { 
		if ($rowusu["su"]==1) {$su="SI";} else {$su="NO";}
		if ($rowusu["ad"]==1) {$ad="SI";} else {$ad="NO";}				
		if ($rowusu["op"]==1) {$op="SI";} else {$op="NO";}
		if ($rowusu["us"]==1) {$us="SI";} else {$us="NO";}
?>                    
                <!-- Datos a Mostrar -->                     
                    <div class="col-md-6 col-sm-12 col-xs-12">                      
                       <blockquote>
                       <label>Nombre:&nbsp;&nbsp;</label><?php print $rowusu["nombre"];?>
                      </blockquote>
                    </div>                  
                    <div class="col-md-6 col-sm-12 col-xs-12">                      
                       <blockquote> 
                       <label>Link:&nbsp;&nbsp;</label><?php print $rowusu["link"];?>
                      </blockquote>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">                      
                       <blockquote>
                       <label>Permisos:&nbsp;&nbsp;</label><?php print "SuperUsuario: ".$su." - Administrador: ".$ad." - Operador: ".$op." - Usuario: ".$us;?>
                      </blockquote>
                    </div>                
<?php
    } // Fin del While
?>                       
<!--                   <div class="ln_solid"></div> -->
                    <div class="col-md-4 col-sm-12 col-xs-12 form-group has-feedback"></div>
                    <div class="col-md-4 col-sm-12 col-xs-12 form-group has-feedback">
                     <a href="abm_menu.php?fun=1" class="btn btn-primary"></i> Volver </a>
                    </div> 
                  </div>
                 </div>                
              </div>
            </div>
<!--
          </div>
        </div>
-->
<?php
} ### Fin de ver ###


############################
### FUNCION formulario() ###
############################

function formulario($form) {

if ($form==1){$subtitulo="Alta de Menu";}
if ($form==2){$subtitulo="Edici&oacute;n de Menu";}
  
if (isset($_POST['merror'])) { $mensaje="<span class=\"error\">".$_POST['merror']."</span>";}
if (isset($_POST['mconfirma'])) { $mensaje="<span class=\"listconfirma\">Crear nuevo registro.</span>";}

$modo=2; // Variable para determinar de que función proviene la llamada a menu_display_children() -- 1=listado 2=formulario
$parent=0;

require_once(dirname(__FILE__) . "/../../clases/validacion.class.inc");

$miValidacion = new Validacion();
    if ($form==2) { $resultusu = $miValidacion->listaItemMenu($_GET['id']); }
    else { $resultusu = $miValidacion->CreaMenu($parent); }       

    $cantusu = $miValidacion->cantidadUltimaTabla(); 

    if ($form==2) {         
        while($rowusu = $miValidacion->fetch_array($resultusu))
            {
        $id=$rowusu["id"];
        $id_padre=$rowusu["id_padre"];
        $nombre=$rowusu["nombre"];
        $nombre_padre=$rowusu["nombre_padre"];        
        $link=$rowusu["link"];
        $su=$rowusu["su"];
        $ad=$rowusu["ad"];
        $op=$rowusu["op"];
        $us=$rowusu["us"];
        
            } // fin de while
                }               
?>    
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php if(isset($subtitulo)){print $subtitulo;}?><small><?php if(isset($mensaje)) {print $mensaje;} ?></small></h2>

                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>

                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>

                    <div class="clearfix"></div>
                  </div>
                <!-- /page content Head -->

                <!-- page content Form -->                      
                  <div class="x_content">

                    <form class="form-horizontal form-label-left" name="form" action="abm_menu.php" method="POST">                      

                    <!-- Campos del Formulario -->

                      <div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback">
                        <label>Nombre</label>                       
                        <input type="text" class="form-control" placeholder="Nombre" name="nombre" value="<?php if(isset($nombre)){print $nombre;}?>" required="required">                        
                      </div>                   
                    <!--   <div class="form-group"></div> -->
                       
                    <?php if ($form==1){ ?>
                        <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                          <label>Men&uacute; Padre</label>  
                          <select class="form-control" name="id_padre" required="required">
                            <option value="">Seleccione</option>
                            <option value="0">/</option>                            
<?php        while($rowusu = $miValidacion->fetch_array($resultusu))
            {
                $level=0;    
?>                       
				<option value="<?php print $rowusu["id"]; ?>"><?php print $rowusu["nombre"]; ?></option>
<?php 
                if ($rowusu['Count'] > 0 ) {  menu_display_children($rowusu['id'], $level + 1, $modo); }
            } ?>                                
                          </select>
                        </div>
                    <?php } ?>
                    
                    <?php if ($form==2){ ?>
                    <div class="col-md-6 col-sm-12 col-xs-12">                      
                       <blockquote> 
                       <label>Men&uacute; Padre:&nbsp;&nbsp;</label><?php if(isset($nombre_padre)){print $nombre_padre;}?>
                      </blockquote>
                    </div>
                    <?php } ?>
                    
                      <div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback">
                        <label>Link al Archivo</label>                       
                        <input type="text" class="form-control" placeholder="Link al archivo" name="link" value="<?php if(isset($link)){print $link;}?>" required="required">                        
                      </div>
                      <div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback">
                       <label>Permisos a Grupos de Usuarios:</label>                         
                        <div class="checkbox">
                            <label>
                              <input type="checkbox" class="flat" name="su" <?php if(isset($su) && $su==1){?> checked="checked" <?php } ?>> Super Usuario
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                              
                              <input type="checkbox" class="flat" name="ad" <?php if(isset($ad) && $ad==1){?> checked="checked" <?php } ?>> Administrador
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              <input type="checkbox" class="flat" name="op" <?php if(isset($op) && $op==1){?> checked="checked" <?php } ?>> Operador
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                              
                              <input type="checkbox" class="flat" name="us" <?php if(isset($us) && $us==1){?> checked="checked" <?php } ?>> Usuario
                             
                            </label>
                        </div>
                    </div>
                       <div class="form-group"></div>

                    <!-- Campos del Formulario --> 
                    
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        
                    <div class="col-md-4 col-sm-12 col-xs-12 form-group has-feedback"></div>
                    <div class="col-md-4 col-sm-12 col-xs-12 form-group has-feedback">
                          <input type="hidden" name="reincide" value="1">                         
                          <input type="hidden" name="formulario" value="<?php print $form;?>">
                          <?php if ($form==2) { ?>
                          <input type="hidden" name="id" value="<?php if(isset($id)){print $id;}?>">
                          <input type="hidden" name="id_padre" value="<?php if(isset($id_padre)){print $id_padre;}?>">
                          <?php } ?>
                          <button type="reset" class="btn btn-primary">Reiniciar</button>                                                  
                          <button id="send" type="submit" class="btn btn-success" name="formulario">Grabar</button>
                          <a href="abm_menu.php?fun=1" class="btn btn-primary"></i> Volver </a>
                        </div>
                      </div>
                    </form>
                  </div>
                <!-- page content Form -->

                </div>
              </div>
            </div>    
<!-- /page content -->

<?php 
} ### FIN DE formulario ###


########################
### FUNCION dbalta() ###
########################

function dbalta() { 

    ## Configuro checkbox
    if (isset($_POST["su"])=="on") {$su=1;} else { $su=0;}
    if (isset($_POST["ad"])=="on") {$ad=1;} else { $ad=0;}	
    if (isset($_POST["op"])=="on") {$op=1;} else { $op=0;}
    if (isset($_POST["us"])=="on") {$us=1;} else { $us=0;} 

    require_once(dirname(__FILE__) . "/../../clases/validacion.class.inc");	
    $miValidacion = new Validacion();
    
    ## INGRESO DATOS.
    $miValidacion->agregaMenu($_POST['nombre'],$_POST['id_padre'],$_POST['link'],$su,$ad,$op,$us,'1');

    ## Configuro la variable para obtener el Id que se le acaba de asisgnar al registro reci�n ingresado.
    $abm_menuid= $miValidacion->insert_id();

    $my_error1 = $miValidacion->error();

    if(!empty($my_error1)) 
        {
            //$htitulo="Alta de Menu";
            $_POST['reincide']=2;
            $_POST['falta']=1;
            $_POST['merror']="Ha habido un error al insertar los valores: <small>".$my_error1."</small>";                                        
            main();
	} else {
            $uresult = $miValidacion->listaUsuarios(); 

            while($urow = $miValidacion->fetch_array($uresult))
		{
			if ($urow["tipo"]==1) {$acceso=$su;}
			if ($urow["tipo"]==2) {$acceso=$ad;}				
			if ($urow["tipo"]==3) {$acceso=$op;}
			if ($urow["tipo"]==4) {$acceso=$us;}

			## INGRESO DATOS EN LA TABLA PERMISOS.
                        $miValidacion->agregaAcceso($abm_menuid,$urow['Id'],$acceso); 
                }           
            $my_error2 = $miValidacion->error();    
            
            if(!empty($my_error2)) 
                {            
                    //$htitulo="Alta de Menu";
                    $_POST['reincide']=2;
                    $_POST['falta']=1;
                    $_POST['merror']="Ha habido un error al insertar los valores: <small>".$my_error2."</small>";                                        
                    main();
                } else {                       
                    $_POST['id']=$abm_menuid;
                    $_POST['reincide']=2;                                        
                    $_POST['ver']=1;
                    $_POST['mconfirma']="El Registro ha sido creado satisfactoriamente.";
                    main();
                }  // Fin If 2                                    
        } // Fin If 1
      
} ### Fin de dbalta ###


#########################
### FUNCION dbmodif() ###
#########################

function dbmodif() {

   ## Configuro checkbox
    if (isset($_POST["su"])=="on") {$su=1;} else { $su=0;}
    if (isset($_POST["ad"])=="on") {$ad=1;} else { $ad=0;}	
    if (isset($_POST["op"])=="on") {$op=1;} else { $op=0;}
    if (isset($_POST["us"])=="on") {$us=1;} else { $us=0;}
    
    require_once(dirname(__FILE__) . "/../../clases/validacion.class.inc");	
    $miValidacion = new Validacion();

    ## INGRESO DATOS.

    $miValidacion->modificaMenu($_POST['nombre'],$_POST['id_padre'],$_POST['link'],$su,$ad,$op,$us,'1',$_POST['id']); 
    // Ahora comprobaremos que todo ha ido correctamente
    $my_error1 = $miValidacion->error();
    
    $uresult = $miValidacion->listaUsuarios(); 

    while($urow = $miValidacion->fetch_array($uresult))
	{
            if ($urow["tipo"]==1) {$acceso=$su;}
            if ($urow["tipo"]==2) {$acceso=$ad;}				
            if ($urow["tipo"]==3) {$acceso=$op;}
            if ($urow["tipo"]==4) {$acceso=$us;}

            ## INGRESO DATOS EN LA TABLA PERMISOS.

            $miValidacion->actualizaAcceso($acceso,$_POST['id'],$urow['Id']); 
        }

    $my_error2 = $miValidacion->error();

    if(!empty($my_error1) || !empty($my_error2)) 
        {
            //$htitulo="Edici&oacute;n de Menu";
            $_POST['reincide']=2;
            $_POST['falta']=1;
            $_POST['merror']="Ha habido un error al editar los valores: <small>".$my_error1."<br>".$my_error2."</small>";                                        
            main();
	} else {                                       
            $_POST['id']=$_POST['id'];
            $_POST['reincide']=2;                                        
            $_POST['ver']=2;
            $_POST['mconfirma']="El Registro ha sido editado satisfactoriamente.";
            main();
    	}  
} ### Fin de dbmodif ###


#########################
### FUNCION elimina() ###
#########################

function elimina() {

$htitulo="Baja de menu";

require_once(dirname(__FILE__) . "/../../clases/validacion.class.inc");
$miValidacion = new Validacion();

$miValidacion->eliminaMenu($_GET['id']);
$my_error1 = $miValidacion->error();

$miValidacion->eliminaMacceso($_POST['id']);
$my_error2 = $miValidacion->error();

if(!empty($my_error1) || !empty($my_error2)) 
    {
        $_POST['merror']="Ha habido un error al eliminar el registro: <small>".$my_error1."<br>".$my_error2."</small>";                                        
        listado();
    } else {
        $_POST['mconfirma']="El Registro ha sido eliminado satisfactoriamente.";
        listado();
    } 
} ### FINAL DE elimina ###

#####################################
### FUNCION menu_display_children ###
#####################################

function menu_display_children($parent, $level, $modo) {
//echo "<br>Funcion Children"." - "."Parent: ".$parent." - "."Level: ".$level." - "."Modo: ".$modo;

require_once(dirname(__FILE__) . "/../../clases/validacion.class.inc");			    
$miValidacion = new Validacion();
$rresult = $miValidacion->creaMenu($parent);

$sangria="";
$tab="&nbsp;&nbsp;&nbsp;&nbsp;";
for ($i = 1; $i <= $level; $i++)
    {
        $sangria=$sangria.$tab;    
    }


while($row = $miValidacion->fetch_array($rresult))
    {
    if ($modo==1) {        
        $id=$row["id"];
        $nombre=$sangria."-&nbsp;".$row["nombre"];
        $link=$row["link"];
        $permisos=$row["su"]." - ".$row["ad"]." - ".$row["op"]." - ".$row["us"];       
?>
 <tr>
    <td>
        <?php
        if(isset($_POST["ver"]) && $_POST["ver"]==1){ ?>
            <a href="abm_menu.php?fun=2&id=<?php print $id;?>" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="Ver"><i class="fa fa-eye"></i></a>
        <?php }
        
        if(isset($_POST["modif"]) && $_POST["modif"]==1){ ?>
            <a href="abm_menu.php?fun=4&id=<?php print $id;?>" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil"></i></a>
        <?php }
        
        if($row['Count'] == 0 && isset($_POST["elimina"]) && $_POST["elimina"]==1){ ?>
            <span data-toggle="tooltip" data-placement="top" title="Eliminar">
            <a href="abm_menu.php?fun=5&id=<?php print $id;?>" class="btn btn-danger btn-xs" data-toggle="confirmation" data-btn-ok-label="Continuar" data-btn-ok-icon="glyphicon glyphicon-share-alt"
        data-btn-ok-class="btn-success" data-btn-cancel-label="Cancelar" data-btn-cancel-icon="glyphicon glyphicon-ban-circle" data-btn-cancel-class="btn-danger" 
        data-title="¿Est&aacute; Seguro?" data-content=""><i class="fa fa-trash-o"></i></a>
            </span>
        <?php } ?>       
    </td>      
    <td><?php echo $id;?></td>    
    <td><?php echo $nombre;?></td>
    <td><?php echo $link;?></td>    
    <td><?php echo $permisos;?></td>    
</tr>
<?php
    } // Fin if
    if ($modo==2) {
 ?>                       
	<option value="<?php print $row["id"]; ?>"><?php print $sangria.$row["nombre"]; ?></option>
<?php       
    } // Fin if        

    if ($row['Count'] > 0) { menu_display_children($row['id'], $level + 1, $modo); }  

} // Fin while

} ### Fin funcion menu_display_children ###