<?php 
//Reanudamos la sesion
@session_start();

if ($_SESSION["Cod"] ) ## Verifico validacion. Si no esta validado vuelve al loguin.
{
	if ($_SESSION["Cod"]=='config') 
	{ 
		header("Location: ../../basico/usuarios/config.php"); 
	} else {
	
		if ($_SESSION["Valid"]==0)
		{ 
			header("Location: ../../modulos/usuarios/contrasena_alta.php");
		}else{
			header("Location: ../../modulos/main/main_1.php");
		}
	}
}
else
{
//	header("Location: ../../modulos/mensajes/noauth.php"); ## Informo que el usuario no esta autorizado para ingresar.
	header("Location: ../../modulos/login/logout.php"); ## Vuelve al login.    
}
?>
