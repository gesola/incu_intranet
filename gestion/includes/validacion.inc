<?php 
/********* VALIDACION DE USUARIO **********/

// Query para determinar si ya se encuentra el id del archivo.
require_once(dirname(__FILE__) . "/../clases/validacion.class.inc");			    
$miValidacion = new Validacion();

$eresult = $miValidacion->listaEstructura($nombre); 
$ecant   = $miValidacion->cantidadUltimaTabla();

while($erow = $miValidacion->fetch_array($eresult)) { $pgid=$erow["id"]; }

// Query para validar usuario.
$vresult = $miValidacion->listaPermisos($pgid,$_SESSION["Id"]); 
$vcant   = $miValidacion->cantidadUltimaTabla();

while($vrow = $miValidacion->fetch_array($vresult)) { $acceso=$vrow["acceso"];}


if ($_SESSION["Cod"] && $_SESSION["Act"]==1 && $_SESSION["Valid"]==1 && $acceso>0) ## Verifico validacion. Si no esta validado vuelve al loguin.
{
//Establezco parámetros de acceso
$binario = sprintf('%04b',  $acceso); //Convierto a binario los permisos de acceso en decimal.

$_POST["ver"]=substr($binario, -1, 1); // Extraigo el valor ver en binario.
$_POST["modif"]=substr($binario, -2, 1); // Extraigo el valor modificar en binario.
$_POST["alta"]=substr($binario, -3, 1); // Extraigo el valor alta en binario.
$_POST["elimina"]=substr($binario, -4, 1); // Extraigo el valor elimina en binario.
      
//Echo "Usuario validado";
validado();
}
else
{
//	header("Location: ../../login.php"); ## Reboto al login ya que no fue validado.
	header("Location: ../../modulos/mensajes/noauth.php"); ## Lo envio a la pagina denegada ya que no fue validado.
}

/********* FIN VALIDACION DE USUARIO ********/
?>
