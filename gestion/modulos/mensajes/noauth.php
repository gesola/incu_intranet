<?php 
//Reanudamos la sesion
@session_start();
$htitulo="Acceso denegado";

if ($_SESSION["Cod"] ) ## Verifico validacion. Si no esta validado vuelve al loguin.
    {
        include ("../../includes/html5_head.inc");

        ?>
<!-- page content -->
<div class="right_col" role="main">

    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>No tiene permisos para ingresar a este m&oacute;dulo.</h3>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->

<?php 
        include ("../../includes/html5_foot.inc");
    } else {
	header("Location: ../../modulos/login/logout.php");         
    }
?>
