<?php 

//Reanudamos la sesión
session_start();

$nombre="main_1";
$descripcion="Pantalla de inicio del sistema";

// Configuro permisos.
$listado_su=1;
$modifica_su=1;
$alta_su=1;
$elimina_su=1;

$listado_ad=1;
$modifica_ad=1;
$alta_ad=1;
$elimina_ad=1;

$listado_op=1;
$modifica_op=0;
$alta_op=0;
$elimina_op=0;

$listado_us=1;
$modifica_us=0;
$alta_us=0;
$elimina_us=0;

$binario_su=$elimina_su.$alta_su.$modifica_su.$listado_su;
$binario_ad=$elimina_ad.$alta_ad.$modifica_ad.$listado_ad;
$binario_op=$elimina_op.$alta_op.$modifica_op.$listado_op;
$binario_us=$elimina_us.$alta_us.$modifica_us.$listado_us;

$su=bindec($binario_su);
$ad=bindec($binario_ad);
$op=bindec($binario_op);
$us=bindec($binario_us);

// Fin configuración de permisos.

include ("../../includes/estructura.inc");
include ("../../includes/validacion.inc");


##########################
### FUNCION validado() ###
##########################

function validado() {
	mainsus();
} ### FIN FUNCION validado ###

/*
########################
### FUNCION logout() ###
########################

function logout() {

$htitulo="Gesti&oacute;n Administrativa"; //Texto de la barra de t�tulo
include ("../../includes/head2.inc");
?>

<div id="boxes">

<div id="dialog" class="window">
<strong>Su usuario no es valido para realizar la acci&oacute;n solicitada.</strong> 
<br><br>
<a href="#" class="close" id="boton">Cerrar</a>
</div>
  
<!-- Mask to cover the whole screen -->
  <div id="mask"></div>
</div>

<form name="volver" method="post" action="../../logout.php"></form>

<?php 
include ("../../includes/foot.inc");

} 
### Fin funcion logout() ###
*/

#########################
### FUNCION mainsus() ###
#########################
	
function mainsus() {

include ("../../includes/html5_head.inc");

$htitulo="Gesti&oacute;n Administrativa:";
$stitulo="P&aacute;gina de Inicio";

?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3><?php print $htitulo;?> <small><?php print $stitulo;?></small></h3>
              </div>              
            </div>
              
<?php

/* SUPERUSUARIO */

/*
?>
            
           <!-- top tiles -->
 
          <div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total Users</span>
              <div class="count">2500</div>
              <span class="count_bottom"><i class="green">4% </i> From last Week</span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-clock-o"></i> Average Time</span>
              <div class="count">123.50</div>
              <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>3% </i> From last Week</span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total Males</span>
              <div class="count green">2,500</div>
              <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total Females</span>
              <div class="count">4,567</div>
              <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i>12% </i> From last Week</span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total Collections</span>
              <div class="count">2,315</div>
              <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total Connections</span>
              <div class="count">7,325</div>
              <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span>
            </div>
          </div>
          <!-- /top tiles -->
*/ ?>

<!-- Botonera de Notificaciones -->              

            <div class="x_content"> 
              <ul class="nav navbar-nav navbar-right">
<!--                  
                  <li class="dropdown btn">                  
                    <a class="btn btn-app">
                      <i class="fa fa-edit"></i> Edit
                    </a>
                  </li>
                  <li class="dropdown btn">
                    <a class="btn btn-app">
                      <i class="fa fa-play"></i> Play
                    </a>
                  </li>
                  <li class="dropdown btn">
                    <a class="btn btn-app">
                      <i class="fa fa-pause"></i> Pause
                    </a>
                  </li>
                  <li class="dropdown btn">                  
                    <a class="btn btn-app">
                      <i class="fa fa-save"></i> Save
                    </a>
                  </li>
--> 
<?php       
// Aviso Vencimientos.
include ("../../operaciones/widget/vencimientos_widget.php");
// Aviso Contratos Vencidos.
include ("../../operaciones/widget/vencidos_widget.php");
?>
<!--                  
                  <li class="dropdown btn">                  
                    <a class="btn btn-app">
                      <i class="fa fa-repeat"></i> Repeat
                    </a>
                  </li>

                  <li class="dropdown btn">
                    <a class="btn btn-app">
                      <span class="badge bg-green">211</span>
                      <i class="fa fa-users"></i> Users
                    </a>
                  </li>
                  <li class="dropdown btn">
                    <a class="btn btn-app">
                      <span class="badge bg-orange">32</span>
                      <i class="fa fa-inbox"></i> Orders
                    </a>
                  </li>

                <li role="presentation" class="dropdown btn"> 
                    <a href="javascript:;" class="dropdown-toggle info-number btn btn-app" data-toggle="dropdown" aria-expanded="false">

                      <span class="badge bg-orange">12</span>
                      <i class="fa fa-envelope"></i> Inbox
                    </a>
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                    <li>
                      <a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <div class="text-center">
                        <a>
                          <strong>See All Alerts</strong>
                          <i class="fa fa-angle-right"></i>
                        </a>
                      </div>
                    </li>
                  </ul>
                </li>

               <li role="presentation" class="dropdown btn">
                    <a class="btn btn-app">
                      <span class="badge bg-blue">102</span>
                      <i class="fa fa-heart-o"></i> Likes
                    </a>
               </li>
-->               
              </ul>                    
                 </div>

<!-- /Botonera de Notificaciones --> 

<!-- Tablas informativas -->

            <div class="x_content">
                
<!-- <div class="col-md-12 col-sm-12 col-xs-12 row"> -->
<?php    
// Informes de saldos deudores de clientes.
if ($_SESSION["Tip"]==1)
    {
        require_once(dirname(__FILE__) . "/../../cont/clases/cont.class.inc");
        $miCont = new Cont();
        $cmresult = $miCont->listaClienteMoroso();
        $cmcant = $miCont->cantidadUltimaTabla();
        
        if ($cmcant>0) {include ("../../cont/widget/morosos_widget.php");}  
?>
<!-- </div>       -->      

<?php       
// Clientes con contratos.
include ("../../operaciones/widget/contratos_widget.php");

?>
            </div>
<!-- /Tablas informativas -->

<?php /*
           <!-- top tiles -->
 
          <div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total Users</span>
              <div class="count">2500</div>
              <span class="count_bottom"><i class="green">4% </i> From last Week</span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-clock-o"></i> Average Time</span>
              <div class="count">123.50</div>
              <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>3% </i> From last Week</span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total Males</span>
              <div class="count green">2,500</div>
              <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total Females</span>
              <div class="count">4,567</div>
              <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i>12% </i> From last Week</span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total Collections</span>
              <div class="count">2,315</div>
              <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total Connections</span>
              <div class="count">7,325</div>
              <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span>
            </div>
          </div>
          <!-- /top tiles -->
*/                  
    }            
            
  ?>
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
<?php
        include ("../../includes/html5_foot.inc");
?>      
        <!-- /footer content -->
<?php

} 
### FIN DE mainsus ###
?>
