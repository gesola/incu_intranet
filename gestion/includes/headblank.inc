<html>
<head>
	<title>GAD - Gesti&oacute;n Administrativa (by GeSist)</title>
	<link rel="shortcut icon" href="images/favicon.gif" >

<link rel="stylesheet" type="text/css" href="../../includes/css/gadstyle.php" media="all">
<link rel="stylesheet" type="text/css" href="../../includes/css/nivelstyle.css" media="all">
<link rel="stylesheet" type="text/css" href="../../includes/css/main.css" media="all">
<link rel="stylesheet" type="text/css" href="../../includes/css/jquery.alerts.php" media="all">
<link rel="stylesheet" type="text/css" href="../../includes/css/blue/style.php" media="print, projection, screen" />
<link rel="stylesheet" type="text/css" href="../../includes/css/jquery.tablesorter.pager.php" media="all">

<script src="../../includes/js/jquery.js" language="javascript" type="text/javascript"></script> 
<script src="../../includes/js/jquery.alerts.js" language="javascript" type="text/javascript"></script> 
<script src="../../includes/js/jquery.form.js" language="javascript" type="text/javascript"></script> 
<script src="../../includes/js/jquery.tablesorter.js" language="javascript" type="text/javascript"></script>
<script src="../../includes/js/jquery.tablesorter.pager.js" language="javascript" type="text/javascript"></script>

</head>
<body leftmargin="2" topmargin="5" marginwidth="0" marginheight="0" class="default">

<table width="1024" border="1" style="border-collapse: collapse; border-color: #003366;" align="center" cellspacing="0" cellpadding="0">
  <tr> 
	 <td align="center">
