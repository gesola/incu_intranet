<?php
/*require_once(dirname(__FILE__) . "../../clases/validacion.class.inc");			    
$miValidacion = new Validacion();

$resultusu = $miValidacion->validaUsuario($_SESSION["Cod"]); 
$cantusu   = $miValidacion->cantidadUltimaTabla();                     

while($rowusu = $miValidacion->fetch_array($resultusu))
    {
        $usuario=$rowusu["nombres"]." ".$rowusu["apellidos"];
        $foto=$rowusu["foto"];
        
        $raiz="../../archivos/usuarios/"; // Establezco el directorio raiz.
        $perfil=$raiz.$rowusu["codigo"]."/".$rowusu["foto"];  
    } // Fin del while
 */   
?>
<!DOCTYPE html>
<html lang="en">
  
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>INTRANET INCUCAI</title>
	<link rel="shortcut icon" href="../../images/favicon.png" >

    <!-- Bootstrap -->
    <!-- <link href="../../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"> -->
    <link href="../../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">    
    <!-- Font Awesome -->
    <link href="../../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Dropzone.js -->
    <link href="../../vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../../vendors/iCheck/skins/flat/blue.css" rel="stylesheet"> 
    <link href="../../vendors/iCheck/skins/flat/green.css" rel="stylesheet"> 
    <!-- bootstrap-wysiwyg -->
    <link href="../../vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet"> 
    <!-- Select2 -->
    <link href="../../vendors/select2/dist/css/select2.min.css" rel="stylesheet"> 
    <!-- Switchery -->
    <link href="../../vendors/switchery/dist/switchery.min.css" rel="stylesheet"> 
    <!-- starrr -->
    <link href="../../vendors/starrr/dist/starrr.css" rel="stylesheet"> 
    <!-- bootstrap-progressbar -->
    <link href="../../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="../../vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="../../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Datatables -->
    <link href="../../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet"> 
    <link href="../../vendors/datatables.net-RowGroup-1.1.2/css/rowGroup.dataTables.min.css" rel="stylesheet">
    
    <!-- bootstrap-datetimepicker -->
    <link href="../../vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">

    <!-- Ion.RangeSlider -->
    <link href="../../vendors/normalize-css/normalize.css" rel="stylesheet">
    <link href="../../vendors/ion.rangeSlider/css/ion.rangeSlider.css" rel="stylesheet">
    <link href="../../vendors/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css" rel="stylesheet">
    <!-- Bootstrap Colorpicker -->
    <link href="../../vendors/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">

    <link href="../../vendors/cropper/dist/cropper.min.css" rel="stylesheet">
    
    <!-- Custom Theme Style -->
    <link href="../../build/css/custom.min.css" rel="stylesheet">
    
    <!-- Subida de archivos -->
    <link href="../../build/css/subida_archivos.css" rel="stylesheet">    
    
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
                <a href="../../../index.html" class="site_title"><!--<img src="../../images/Iso_Incu_01.png">-->&nbsp;&nbsp;<span><img src="../../images/logo-incucai.png"></span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
<?php /*           <div class="profile clearfix">

<?php if($foto==NULL) { ?>                
              <div class="profile_pic">
                <img src="../../images/user.png" alt="..." class="img-circle profile_img">
              </div>
<?php } else { ?>
              <div class="profile_pic">
                <img src="<?php print $perfil; ?>" alt="..." class="img-circle profile_img">
              </div>                
<?php } ?>                
              <div class="profile_info">
                <span>Bienvenido,</span>
                <h2><?php print $usuario;?></h2>
              </div>
            </div> 
 * 
 */ ?>
            <!-- /menu profile quick info -->

            <br />

            
            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                
<?php /*
		if (isset($_SESSION["Valid"]) == 1) 
			{                
*/?>               
              <div class="menu_section">
                <h3>Menu Principal</h3>
<?php 
                include ("../../includes/html5_menupri.inc");
?> 
              </div> 
<?php /*                           
                        } else {
 ?>               
              <div class="menu_section">
                <h3>Menu Principal</h3>
<?php                
		include ("../../includes/html5_menumi.inc");
?> 
              </div> 
<?php                               
			}                        
 
		if (isset($_SESSION["Valid"]) == 1 && isset($_SESSION["Ope"]) == 1) 
                                { 
?>
              <div class="menu_section">              
                <h3>Operaciones</h3> 
<?php 
                    include ("../../includes/html5_menuope.inc");
?> 
              </div> 
<?php                                     
                                }                                

## Menu Administracion ##

$archivoadmin = '../../cont/includes/html5_menuadm.inc'; 

		if (file_exists($archivoadmin)) 
                                { 
?>
              <div class="menu_section">              
                <h3>Administración</h3> 
<?php 
                    include ("../../cont/includes/html5_menuadm.inc");
?> 
              </div> 
<?php                                     
                                }                                
*/
# /Menu Administracion #

# Menu General #
/* ?>                                
<div class="menu_section">
    <h3>General</h3>
   <ul class="nav side-menu">
        <li><a href="../../modulos/usuarios/contrasena_modif.php" TARGET="_top"><i class="fa fa-key"></i> Cambiar Contrase&ntilde;a</a></li>        
	<li><a href="../../modulos/login/logout.php" TARGET="_top"><i class="fa fa-sign-out"></i> Salir del sitio</a></li>
    </ul>
</div>    
<?php    */
# /Menu General #                                
?>                
            </div>
            <!-- /sidebar menu -->
 
                
            <!-- /menu footer buttons -->
<!--            
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="../../modulos/login/logout.php">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
-->            
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
<?php /*        
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                      <?php if($foto==NULL) { ?>                
                        <img src="../../images/user.png" alt="">
                      <?php } else { ?>
                        <img src="<?php print $perfil; ?>" alt="">              
                      <?php } ?>
                    <?php print $usuario;?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;"> Profile</a></li>
                    <li>
                      <a href="javascript:;">
                        <span class="badge bg-red pull-right">50%</span>
                        <span>Settings</span>
                      </a>
                    </li>
                    <li><a href="javascript:;">Help</a></li>
                    <li><a href="../../modulos/login/logout.php"><i class="fa fa-sign-out pull-right"></i> Salir del Sitio</a></li>
                  </ul>
                </li>
<!--
                <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-envelope-o"></i>
                    <span class="badge bg-blue">6</span>
                  </a>
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                    <li>
                      <a>
                        <span class="image"><img src="../../images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="../../images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="../../images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="../../images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <div class="text-center">
                        <a>
                          <strong>See All Alerts</strong>
                          <i class="fa fa-angle-right"></i>
                        </a>
                      </div>
                    </li>
                  </ul>
                </li>
-->                
              </ul>
            </nav>
          </div>
        </div>
 * 
 */ ?>
        <!-- /top navigation -->
        <!-- page content -->
<?php
// unset($_SESSION["Ope"]);

