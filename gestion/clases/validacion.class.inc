<?php
########################################################
##                                                    ##
##           ACTUALIZACIONES DE VERSION               ##
##                                                    ##
##  - Actualización 22-10-2014 - GES- Version 4.1.1   ##
##  - Actualización 21-04-2015 - GES- Version 4.2.2   ##
##  - Actualización 22-04-2015 - GES- Version 4.2.3   ##
##  - Actualización 23-04-2015 - GES- Version 4.2.4   ##
##                                                    ##
########################################################
##  Clase: validación                                 ##
##  Descripcion: Crea la conexion con la base de datos##
##   al contruir el objeto, y se encarga de realizar  ##
##   las consultas utilizadas por la aplicacion       ##
########################################################
## error_reporting(E_ALL ^ E_DEPRECATED);
//echo "Estoy en Validacion";

class Validacion { 
## Atributos
	private $connect;           ## Conexion con la base de datos
	private $sql;               ## Cadena sql de la ultima consulta
	private $tabla;             ## RecordSet con los datos de la ultima consulta
	private $camposVariables;   ## Valor para consultas con cantidad de campos variables
	private $cuadroNotasTitulo; ## titulos para metodo cuadroDeNotasDeCurso
	private $seleccion;	        ## columna seleccionada en metodo cuadroDeNotas
	private $tipoMotor;         ## identifica el motor utilizado 
	private $debug;

## Constructor
	function __construct(){
		$this->tipoMotor = "mysqli";		## Aqui se elije el motor de base de datos a usar(mysql|mysqli)
		@require(dirname(__FILE__) . "/../includes/" . $this->tipoMotor . ".inc");
		$this->connect = $connect;
		$this->debug = true;
	}

########################## 
##                      ##
##  C O N S U L T A S   ##
##                      ##
#############################################################################################################################
## Momenclatura: Cuando los metodos comienzan con lista se refiere a un conjunto de registros
##       Cuando comienzan con cuadro se trata de un conjunto de registros, que aparete un solo registro puede
##       contener datos de muchos registros (ej todas las notas de un alumno, en la base cada nota es un registro)
##################################################################################################################################


## METODO:  listaUsuarios (USADO EN: usuario_list, clientes_alta.php)
## Retorna registros del usuarios.
	function listaUsuarios(){
                $parametro = "";
		if(func_num_args() == 1)
                    { //si la funcion recibio 1 parametro
                        $p0 = func_get_arg(0);                    
                        $parametro = " AND activo='" . $this->snt($p0) . "'"; //lo concidera como el id de pais
                    }
		$this->sql = "SELECT * FROM usuarios WHERE tipo <> '0'".$parametro." ORDER BY tipo DESC, apellidos ASC";
		$this->query();
		return $this->tabla;
	}        
        
        
## METODO:  validaUsuarios (USADO EN: loguin.php)
## Entrada: recibe codigo de usuario y contraseña.
## Salida:  |Todos los registros de la tabla usuario|
## Retorna registro del usuario a validar.
	function validaUsuarios($codUsuario, $clave){
		$this->snt($codUsuario);
		$this->snt($clave);                
		$this->sql = "SELECT * FROM usuarios WHERE codigo='{$codUsuario}' AND clave='{$clave}'";
		$this->query();
		return $this->tabla;
	}
 
        
## METODO:  listaUsuario (USADO EN: usuario_alta.php, contrasena_modif.php)
## Retorna registros de usuarios.        
	function listaUsuario(){ 
		$parametro = "";
		if(func_num_args() == 1){ //si la funcion recibio 1 parametro
                        $p0 = func_get_arg(0);                    
			$parametro = "WHERE Id='" . $this->snt($p0) . "'"; //lo concidera como el id de usuario
		}
    		if(func_num_args() == 2){ //si la funcion recibio 2 parametro
                        $p0 = func_get_arg(0);
                        $p1 = func_get_arg(1);                    
			$parametro = "WHERE Id='" . $this->snt($p0)."' AND clave='" . $this->snt($p1) . "'"; 
		}               
		$this->sql = "SELECT * FROM usuarios " . $parametro . " ORDER BY Id ASC";		
		$this->query();
		return $this->tabla;
	} 

        
## METODO:  validaUsuario (USADO EN: loguin.php, contrasena_alta.php)
## Entrada: recibe codigo de usuario y contraseña.
## Salida:  |Todos los registros de la tabla usuario|
## Retorna registro del usuario a validar.
	function validaUsuario(){            
		$parametro = "";
		if(func_num_args() == 1){ //si la funcion recibio 1 parametro
                        $p0 = func_get_arg(0);                    
			$parametro = "WHERE codigo='" . $this->snt($p0) . "'"; 
		}
   		if(func_num_args() == 2){ //si la funcion recibio 1 parametro
                	$p0 = func_get_arg(0);                
                        $p1 = func_get_arg(1);      
			$parametro = "WHERE codigo='" . $this->snt($p0)."' AND clave='" . $this->snt($p1) . "'"; 
		}             
		$this->sql = "SELECT * FROM usuarios " . $parametro ;
		$this->query();
		return $this->tabla;
	}       
        
        
 ## METODO:  listaPermisos (USADO EN: validacion.inc)
## Entrada: recibe codigo de usuario y contraseña.
## Salida:  |Todos los registros de la tabla usuario|
## Retorna registro del usuario a validar.
	function listaPermisos($pgid, $Id){
		$this->snt($pgid);
		$this->snt($Id);                
                $this->sql = "SELECT acceso FROM permisos WHERE id_estructura='{$pgid}' AND id_usuario='{$Id}'";             
		$this->query();
		return $this->tabla;
	}
        
## METODO:  listaEstructuraPermisos (USADO EN: permisos_modif.php)
## Retorna registros de usuarios.        
	function listaEstructuraPermisos(){
            
		$parametro = "";
		if(func_num_args() == 1){ //si la funcion recibio 1 parametro
                    $p0 = func_get_arg(0);                    
                    $parametro = "AND permisos.id_usuario='" . $this->snt($p0) . "'"; //lo concidera como el id de usuario
		}
		//$this->sql = "SELECT Id,tipo,nombres,apellidos,activo,codigo,observaciones FROM usuarios " . $parametro . " ORDER BY Id ASC";
		$this->sql = "SELECT estructura.id AS eid,estructura.nombre,estructura.descripcion,permisos.acceso,permisos.id AS pid FROM estructura,permisos WHERE (permisos.id_estructura = estructura.id) " . $parametro . " ORDER BY estructura.nombre";                
		$this->query();
		return $this->tabla;
	}


## METODO:  validaModulo (USADO EN: clientes_alta.php)
	function validaModulo(){
            	$parametro = "";
		if(func_num_args() == 1){ //si la funcion recibio 1 parametro
                    $p0 = func_get_arg(0);                    
                    $parametro = " WHERE id='" . $this->snt($p0) . "'"; //lo concidera como el id de usuario
		}  
		$this->sql = "SELECT id,valor FROM config".$parametro;
		$this->query();
		return $this->tabla;
	}

## METODO:  listaMenu 
	function listaMenu(){
            	$parametro = "";
		if(func_num_args() == 1){ //si la funcion recibio 1 parametro
                    $p0 = func_get_arg(0);                    
                    $parametro = " WHERE id_padre='" . $this->snt($p0) . "'"; //lo concidera como el id de usuario
		}            
		$this->sql = "SELECT * FROM menu".$parametro;
		$this->query();
		return $this->tabla;
	}     

 ## METODO:  listarEstructura 
	function listarEstructura(){
		$this->sql = "SELECT id,su,ad,op,us FROM estructura";
		$this->query();
		return $this->tabla;
	} 
        
        
## METODO:  validaAccesoMenu (USADO EN: menupri.inc)
## Entrada: recibe id del menu y id del usuario.
## Salida:  |los registros de la tabla menu|
## Retorna Todos los registros correpondientes al menu del usuario.
	function validaAccesoMenu($rowid, $usuarioid){
		$this->snt($rowid);
		$this->snt($usuarioid);                
		$this->sql = "SELECT acceso FROM macceso WHERE id_menu='{$rowid}' AND id_usuario='{$usuarioid}'";
		$this->query();
		return $this->tabla;
	}
	
## METODO:  listaEstructura (USADO EN: estructura.inc, validacion.inc)
	function listaEstructura(){               
		$parametro = "";
		if(func_num_args() == 1){ //si la funcion recibio 1 parametro
                    $p0 = func_get_arg(0);
                    $parametro = "WHERE nombre='" . $this->snt($p0) . "'"; 
		}
		$this->sql = "SELECT * FROM estructura " . $parametro;                
		$this->query();
		return $this->tabla;
	}

        
## METODO:  listadoMenu (USADO EN: menu_list.php, menu_modif)
## Salida:  |los registros de la tabla menu|
## Retorna Todos los registros correpondientes al menu.
	function listadoMenu(){
            	$parametro = "";
		if(func_num_args() == 1){ //si la funcion recibio 1 parametro
                    $p0 = func_get_arg(0);                    
                    $parametro = " WHERE t1.id_padre='" . $this->snt($p0) . "'"; 
		}
//		$this->sql = "SELECT t1.id as id1,t1.nombre AS nombre1,t1.nivel as nivel1,t1.link as link1,t1.activo as activo1,t1.id_padre as id_padre1,t1.su as su1,t1.ad as ad1,t1.op as op1,t1.us as us1,t2.id as id2,t2.nombre AS nombre2,t2.nivel as nivel2,t2.link as link2,t2.id_padre as id_padre2,t2.su as su2,t2.ad as ad2,t2.op as op2,t2.us as us2,t3.id as id3,t3.nombre AS nombre3,t3.nivel as nivel3,t3.link as link3,t3.id_padre as id_padre3,t3.su as su3,t3.ad as ad3,t3.op as op3,t3.us as us3 FROM menu AS t1 LEFT JOIN menu AS t2 ON (t1.id=t2.id_padre) LEFT JOIN menu AS t3 ON (t2.id=t3.id_padre) WHERE t1.id_padre='0' ORDER BY id_padre3,id1 ASC";
		$this->sql = "SELECT t4.nombre as t4, t3.nombre as t3, t2.nombre as t2, t1.nombre as nombre,t1.id AS id, t1.nivel AS nivel, t1.id_padre AS id_padre, t1.link AS link, t1.activo AS activo, t1.su AS su, t1.ad AS ad, t1.op AS op, t1.us AS us FROM menu t1 LEFT JOIN menu t2 ON t1.id_padre=t2.id LEFT JOIN menu t3 ON t2.id_padre=t3.id LEFT JOIN menu t4 ON t3.id_padre=t4.id" . $parametro;
                $this->query();
		return $this->tabla;
	} 

        
## METODO:  listaItemMenu (USADO EN: menu_alta.php, menu_modif.php)
	function listaItemMenu(){

		$parametro = "";
		if(func_num_args() == 1){ //si la funcion recibio 1 parametro
                    $p0 = func_get_arg(0);                    
                    $parametro = "WHERE t1.id='" . $this->snt($p0) . "'"; 
		}
//		$this->sql = "SELECT * FROM menu " . $parametro;
		$this->sql = "SELECT t1.nombre as nombre, t1.id AS id, t1.nivel AS nivel, t1.id_padre AS id_padre, t1.link AS link, t1.activo AS activo, t1.su AS su, t1.ad AS ad, t1.op AS op, t1.us AS us, t2.nombre as nombre_padre FROM menu t1 LEFT JOIN menu t2 ON t1.id_padre=t2.id " . $parametro;                
		$this->query();
		return $this->tabla;
	}

## METODO:  modificaPermisoBlanqueo (USADO EN: permisos_modif.php)
## Entrada: Recibe los campos para actualizar y el id.
## Salida:  
## Actualiza el Acceso al menu
	function modificaPermisoBlanqueo($id){
                $this->snt($id);
                $this->sql = "UPDATE permisos SET acceso='0' WHERE id_usuario='{$id}'";
		$this->query();
	}

## METODO:  listaItemPermiso (USADO EN: abm_permisos.php)
	function listaItemPermiso(){
                $parametro="";
		if(func_num_args() == 1){ //si la funcion recibio 1 parametro
                        $p0 = func_get_arg(0);                    
			$parametro = " WHERE t1.id_usuario='" . $this->snt($p0) . "'"; //lo concidera como el id de usuario
		}
    		if(func_num_args() == 2){ //si la funcion recibio 2 parametro
                        $p0 = func_get_arg(0);
                        $p1 = func_get_arg(1);                    
			$parametro = " WHERE t1.id='" . $this->snt($p1) . "'"; //toma id 
		}               
                $this->sql = "SELECT t1.id AS id,t1.id_estructura AS id_estructura,t1.id_usuario AS id_usuario,t1.acceso AS acceso,t2.nombre AS nombre,t2.descripcion AS descripcion FROM permisos t1 LEFT JOIN estructura t2 ON t1.id_estructura=t2.id".$parametro;
                $this->query();
		return $this->tabla;
	}
        
        
#####################
##                 ##
## C R E A C I O N ##
##                 ##
#####################

## METODO:  creaMenu (USADO EN: html5_menupri.inc, abm_menu.php)
## Entrada: recibe parent.
## Salida:  |Todos los registros de la tabla menu|
## Retorna Todos los registros correpondientes al menu del usuario.
	function creaMenu($parent){
                $this->snt($parent);
//		$this->sql = "SELECT a.id, a.nombre, a.link, Deriv1.Count FROM menu a LEFT OUTER JOIN (SELECT id_padre, COUNT(*) AS Count FROM menu GROUP BY id_padre) Deriv1 ON a.id = Deriv1.id_padre WHERE a.id_padre='{$parent}'";
                $this->sql = "SELECT a.id, a.nombre, a.link, a.su, a.ad, a.op, a.us, Deriv1.Count FROM menu a LEFT OUTER JOIN (SELECT id_padre, COUNT(*) AS Count FROM menu GROUP BY id_padre) Deriv1 ON a.id = Deriv1.id_padre WHERE a.id_padre='{$parent}'";
                $this->query();
		return $this->tabla;
	}

## METODO:  creaMenuOpe (USADO EN: html5_menuope.inc)
## Entrada: recibe parent.
## Salida:  |Todos los registros de la tabla menu|
## Retorna Todos los registros correpondientes al menu del usuario.
	function creaMenuOpe($parent){
                $this->snt($parent);
		$this->sql = "SELECT a.id, a.nombre, a.link, Deriv1.Count FROM menu_ope a LEFT OUTER JOIN (SELECT id_padre, COUNT(*) AS Count FROM menu_ope GROUP BY id_padre) Deriv1 ON a.id = Deriv1.id_padre WHERE a.id_padre='{$parent}'";
		$this->query();
		return $this->tabla;
	} 

## METODO:  creaMenuAdm (USADO EN: html5_menuadm.inc)
## Entrada: recibe parent.
## Salida:  |Todos los registros de la tabla menu|
## Retorna Todos los registros correpondientes al menu del usuario.
	function creaMenuAdm($parent){
                $this->snt($parent);
		$this->sql = "SELECT a.id, a.nombre, a.link, Deriv1.Count FROM cont_menu_adm a LEFT OUTER JOIN (SELECT id_padre, COUNT(*) AS Count FROM cont_menu_adm GROUP BY id_padre) Deriv1 ON a.id = Deriv1.id_padre WHERE a.id_padre='{$parent}'";
		$this->query();
		return $this->tabla;
	} 
        
## METODO:  agregaEstructura (USADO EN: estructura.inc)
## Entrada: Recibe los campos para agregar un registro a la tabla estructura.
## Salida:  
## Agrega estructura.
	function agregaEstructura($nombre,$descripcion,$su,$ad,$op,$us){
                $this->snt($nombre);
                $this->snt($descripcion);
                $this->snt($su);
                $this->snt($ad);
                $this->snt($op);
                $this->snt($us);
                $this->sql = "INSERT INTO estructura (nombre,descripcion,su,ad,op,us) VALUES ('{$nombre}','{$descripcion}','{$su}','{$ad}','{$op}','{$us}')";              
		$this->query();
	}	

## METODO:  agregaPermisos (USADO EN: estructura.inc, usuario_alta.php)
## Entrada: Recibe los campos para agregar un registro a la tabla estructura.
## Salida:  
## Agrega estructura.
	function agregaPermisos($estructuraid,$Id,$acceso){
                $this->snt($estructuraid);
                $this->snt($Id);
                $this->snt($acceso);
                $this->sql = "INSERT INTO permisos (id_estructura,id_usuario,acceso) VALUES ('{$estructuraid}','{$Id}','{$acceso}')";  
		$this->query();
	}  
  
## METODO:  agregaUsuario (USADO EN: usuario_alta.php)
## Entrada: Recibe los campos para agregar un registro a la tabla usuarios.
## Salida:  
## Agrega Usuario.
	function agregaUsuario($nombre,$apellido,$codigo,$observaciones,$pass,$tipo){
                $this->snt($nombre);
                $this->snt($apellido);
                $this->snt($codigo);
                $this->snt($observaciones);
                $this->snt($pass);
                $this->snt($tipo);
                $this->sql = "INSERT INTO usuarios (nombres,apellidos,codigo,observaciones,clave,tipo) VALUES ('{$nombre}','{$apellido}','{$codigo}','{$observaciones}','{$pass}','{$tipo}')";
                $this->query();
	}
        
## METODO:  agregaAcceso (USADO EN: usuario_alta.php, menu_alta.php)
## Entrada: Recibe los campos para agregar un registro a la tabla macceso.
## Salida:  
## Agrega acceso al menu.
	function agregaAcceso($id,$usid,$macceso){
                $this->snt($id);
                $this->snt($usid);
                $this->snt($macceso);
                $this->sql = "INSERT INTO macceso (id_menu,id_usuario,acceso) VALUES ('{$id}','{$usid}','{$macceso}')";  
                $this->query();
	}
        
## METODO:  agregaMenu (USADO EN: menu_alta.php)
## Entrada: Recibe los campos para agregar un registro a la tabla menu.
## Salida:  
## Agrega menu.
	function agregaMenu($nombre,$id_padre,$link,$su,$ad,$op,$us,$activo){
                $this->snt($nombre);
                $this->snt($id_padre);
                $this->snt($link);
                $this->snt($su);
                $this->snt($ad);
                $this->snt($op);
                $this->snt($us);
                $this->snt($activo);
                $this->sql = "INSERT INTO menu (nombre,id_padre,link,su,ad,op,us,activo) VALUES ('{$nombre}','{$id_padre}','{$link}','{$su}','{$ad}','{$op}','{$us}','{$activo}')";                
                $this->query();
	} 
        
#############################
##                         ##
## M O D I F I C A C I O N ##
##                         ##
#############################        
        
## METODO:  desactivaUsuario (USADO EN: usuario_list.php)
## Entrada: Recibe los campos para actualizar y el id.
## Salida:  
## Actualiza el estado del usuario.
	function desactivaUsuario($id){
                $this->snt($id);
		//$this->sql = "UPDATE pais SET nombre_pais='{$nombre}'  WHERE id='{$id}'";
 		$this->sql = "UPDATE usuarios SET activo='0' WHERE Id='{$id}'";               
		$this->query();
	}        
 
## METODO:  activaUsuario (USADO EN: usuario_list.php)
## Entrada: Recibe los campos para actualizar y el id.
## Salida:  
## Actualiza el estado del usuario.
	function activaUsuario($id){
                $this->snt($id);
 		$this->sql = "UPDATE usuarios SET activo='1' WHERE Id='{$id}'";               
		$this->query();
	}       
 
        
## METODO:  actualizaUsuario (USADO EN: usuario_modif.php)
## Entrada: Recibe los campos para actualizar y el id.
## Salida:  
## Actualiza el Usuario.
	function actualizaUsuario($id, $nombre, $apellido, $observaciones, $tipo){
                $this->snt($id);
                $this->snt($nombre);
                $this->snt($apellido);
                $this->snt($observaciones);
                $this->snt($tipo);
		//$this->sql = "UPDATE pais SET nombre_pais='{$nombre}'  WHERE id='{$id}'";
 		$this->sql = "UPDATE usuarios SET nombres='{$nombre}',apellidos='{$apellido}',observaciones='{$observaciones}',tipo='{$tipo}' WHERE Id='{$id}'";               
		$this->query();
	}
 
## METODO:  actualizaFoto (USADO EN: abm_usuario.php)
	function actualizaFoto($id, $foto){
                $this->snt($id);
                $this->snt($foto);
 		$this->sql = "UPDATE usuarios SET foto='{$foto}' WHERE Id='{$id}'";               
		$this->query();
	}        
        
 ## METODO:  actualizaAcceso (USADO EN: usuario_modif.php)
## Entrada: Recibe los campos para actualizar y el id.
## Salida:  
## Actualiza el Acceso al menu
	function actualizaAcceso($macceso,$mid, $uid){
                $this->snt($macceso);
                $this->snt($mid);
                $this->snt($uid);
                $this->sql = "UPDATE macceso SET acceso='{$macceso}' WHERE id_menu='{$mid}' AND id_usuario='{$uid}'";               
		$this->query();
	}       
        
## METODO:  actualizaPermisos (USADO EN: usuario_modif.php)
## Entrada: Recibe los campos para actualizar y el id.
## Salida:  
## Actualiza el Acceso al menu
	function actualizaPermisos($eacceso,$eid, $uid){
                $this->snt($eacceso);
                $this->snt($eid);
                $this->snt($uid);
                $this->sql = "UPDATE permisos SET acceso='{$eacceso}' WHERE id_estructura='{$eid}' AND id_usuario='{$uid}'";
		$this->query();
	}
        
## METODO:  modificaPermiso (USADO EN: permisos_modif.php)
## Entrada: Recibe los campos para actualizar y el id.
## Salida:  
## Actualiza el Acceso al menu
	function modificaPermiso($acceso,$id){
                $this->snt($acceso);
                $this->snt($id);
                $this->sql = "UPDATE permisos SET acceso='{$acceso}' WHERE id='{$id}'";
		$this->query();
	} 
        
## METODO:  modificaMenu (USADO EN: menu_modif.php)
## Entrada: Recibe los campos para actualizar y el id.
## Salida:  
## Actualiza el Acceso al menu
	function modificaMenu($nombre,$id_padre,$link,$su,$ad,$op,$us,$activo,$id){
                $this->snt($nombre);
                $this->snt($id_padre);
                $this->snt($link);
                $this->snt($su);
                $this->snt($ad);
                $this->snt($op);
                $this->snt($us);
                $this->snt($activo);
                $this->snt($id);
                $this->sql = "UPDATE menu SET nombre='{$nombre}',id_padre='{$id_padre}',link='{$link}',su='{$su}',ad='{$ad}',op='{$op}',us='{$us}',activo='{$activo}' WHERE id='{$id}'";                
		$this->query();
	}        

## METODO:  modificaClave (USADO EN: contrasena_alta.php)
## Entrada: Recibe los campos para actualizar la clave.
## Salida:  
## Actualiza clave de usuario
	function modificaClave($contrasena,$valid,$Cod){
                $this->snt($contrasena);
                $this->snt($valid);
                $this->snt($Cod);
                $this->sql = "UPDATE usuarios SET clave='{$contrasena}', valid='{$valid}' WHERE codigo='{$Cod}'";                
		$this->query();
	}        
        
## METODO:  actualizaClave (USADO EN: contrasena_modif.php)
## Entrada: Recibe los campos para actualizar la clave.
## Salida:  
## Actualiza clave de usuario
	function actualizaClave($contrasena,$Id){
                $this->snt($contrasena);
                $this->snt($Id);
                $this->sql = "UPDATE usuarios SET clave='{$contrasena}' WHERE Id='{$Id}'";                
		$this->query();
	} 
        
###########################
##                       ##
## E L I M I N A C I O N ##
##                       ##
###########################
        
## METODO:  eliminaMenu (USADO EN: menu_list.php)
## Entrada: Id de menu.
## Salida:  
## Elimina el item de menu elegido.
	function eliminaMenu($id){
                $this->snt($id);
		$this->sql = "DELETE FROM menu WHERE id='{$id}'";
		$this->query();
	}        

## METODO:  eliminaMacceso (USADO EN: menu_list.php)
## Entrada: Id de macceso.
## Salida:  
## Elimina el acceso de menu elegido.
	function eliminaMacceso($id){
                $this->snt($id);
		$this->sql = "DELETE FROM macceso WHERE id='{$id}'";
		$this->query();
	}
        
## METODO:  eliminaUsuario (USADO EN: abm_usuarios.php)
## Entrada: Id de macceso.
## Salida:  
## Elimina el acceso de menu elegido.
	function eliminaUsuario($id){
                $this->snt($id);
		$this->sql = "DELETE FROM usuarios WHERE id='{$id}'";
		$this->query();
	}        

## METODO:  eliminaAcceso (USADO EN: abm_usuarios.php)
## Entrada: Id de macceso.
## Salida:  
## Elimina el acceso de menu elegido.
	function eliminaAcceso($id){
                $this->snt($id);
		$this->sql = "DELETE FROM macceso WHERE id_usuario='{$id}'";
		$this->query();
	}

## METODO:  eliminaPermisos (USADO EN: abm_usuarios.php)
## Entrada: Id de macceso.
## Salida:  
## Elimina el acceso de menu elegido.
	function eliminaPermisos($id){
                $this->snt($id);
		$this->sql = "DELETE FROM permisos WHERE id_usuario='{$id}'";
		$this->query();
	} 
        
        
################################
##                            ##
## METODOS PARA MANEJAR MYSQL ##
##                            ##
################################

## Devuelve un resaltador cuando $indice coincida con la evaluacion seleccionada en cuadroDeNotasDeCurso
	function cuadroSeleccion($indice, $inicioFin){
		return ($this->seleccion == $indice+1)? (($inicioFin == "FIN")?"</strong>":"<strong>"):"";
	}

## Titulos de cuadroDeNotasDeCurso
	function cuadroTitulos($indice){
		return "Nota: ".$this->cuadroNotasTitulo[$indice];
	}
## Camtidad de registros de la ultima consulta
	function cantidadUltimaTabla(){
		return $this->cantidad($this->tabla);
	}
## Cantidad de registros
	function cantidad($registros){
		if($this->tipoMotor == "mysql"){
			return @mysql_num_rows($registros);
		}elseif($this->tipoMotor == "mysqli"){
			return $registros->num_rows;
		}
	}
	
	function cantidadCamposVariables(){
		return $this->camposVariables;
	}
## Procesa consulta
	private function query(){
		if($this->tipoMotor == "mysql"){
			$this->tabla = @mysql_query($this->sql, $this->connect);
		}elseif($this->tipoMotor == "mysqli"){
			$this->tabla = $this->connect->query($this->sql);
		}
		if($this->debug){
			echo($this->error());
		}
	}
## Errores
	function error(){
		if($this->tipoMotor == "mysql"){
			return @mysql_error($this->connect);
		}elseif($this->tipoMotor == "mysqli"){
			return $this->connect->error;
		}
	}
## Debug
	function reportar($texto){
		file_put_contents('php://stderr', print_r('                               Debug: '.$texto, TRUE));
	}

	function mostrarCelda($texto,$alineacion){
		$align = "";
		if($alineacion=="center"){
			$align = "style='text-align: center;'";
		}
		echo "<abbr title='{$texto}'><div {$align} class='celda'>{$texto}</div></abbr>";
	}

	function fetch_array($registros){
		if($this->tipoMotor == "mysql"){
			return @mysql_fetch_array($registros);
		}elseif($this->tipoMotor == "mysqli"){
			return $registros->fetch_array(MYSQLI_BOTH);
		}

	}

	function data_seek($registros, $pos){
		if($this->tipoMotor == "mysql"){
			return @mysql_data_seek($registros, $pos);
		}elseif($this->tipoMotor == "mysqli"){
			return $registros->data_seek($pos);
		}
	}

	//$miConsulta->fetch_assoc($fresult) Ejemplo de uso
	function fetch_assoc($registros){
		if($this->tipoMotor == "mysql"){
			return @mysql_fetch_assoc($registros);
		}elseif($this->tipoMotor == "mysqli"){
			return $registros->fetch_assoc();
		}

	}

	function insert_id(){
		if($this->tipoMotor == "mysql"){
			return @mysql_insert_id($this->connect);
		}elseif($this->tipoMotor == "mysqli"){
			return $this->connect->insert_id;
		}

	}
        
        //Sanityza el texto para evitar la inyeccion de comandos sql
	function snt(&$a){
		if(!is_array($a)){
			$a = $this->connect->real_escape_string($a);
			$a = htmlspecialchars($a, ENT_QUOTES);
		}
		return $a;
	}

}
