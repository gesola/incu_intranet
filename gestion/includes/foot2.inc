 
<html>

<?php 
include ("../../config/config.inc");
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"> 
<!-- <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> -->
<html>
<head>
	<title>GAD - Gesti&oacute;n Administrativa (by GeSist)</title>
	<link rel="shortcut icon" href="../../images/favicon.gif" >

<link rel="stylesheet" type="text/css" href="../../includes/css/gadstyle.css" media="all">
<link rel="stylesheet" type="text/css" href="../../includes/css/nivelstyle.css" media="all">
<link rel="stylesheet" type="text/css" href="../../includes/css/main.css" media="all">
<link rel="stylesheet" type="text/css" href="../../includes/css/jquery.clickme.css" media="all">
<link rel="stylesheet" type="text/css" href="../../includes/css/jquery.alerts.css" media="all">
<link rel="stylesheet" type="text/css" href="../../includes/css/blue/style.css" media="print, projection, screen" />
<link rel="stylesheet" type="text/css" href="../../includes/css/jquery.tablesorter.pager.css" media="all">

<script src="../../includes/js/jquery.js" language="javascript" type="text/javascript"></script> 
<script src="../../includes/js/jquery.clickme.js" language="javascript" type="text/javascript"></script> 
<script src="../../includes/js/jquery.alerts.js" language="javascript" type="text/javascript"></script> 
<script src="../../includes/js/jquery.form.js" language="javascript" type="text/javascript"></script> 
<script src="../../includes/js/jquery.tablesorter.js" language="javascript" type="text/javascript"></script>
<script src="../../includes/js/jquery.tablesorter.pager.js" language="javascript" type="text/javascript"></script>



</head>
<?php
// Configuro formato de fecha.
	if( ! ini_get('date.timezone') )
{
  date_default_timezone_set('GMT');
  //date_default_timezone_set(date_default_timezone_get());
}
?>
<body>
<table width="800" align="center" border="1" style="border-collapse: collapse; border-color: #003366;" cellspacing="0" cellpadding="0">
  <tr align="center"> 
    <td class="footbarra"><b>GAD - Gesti&oacute;n Administrativa - Versi&oacute;n 4.2 | Desde 2005 | &copy; <?php echo date('Y')."&nbsp;&nbsp;GeSist.";?>  </b></td>
  </tr>
  <tr align="right"> 
	  <td class="foot"><b>Desarrollo:</b>&nbsp;&nbsp;<a href="www.gesist.com.ar">Gesist - Gesti&oacute;n en Sistemas.</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br> 
		<b>E-Mail:</b>&nbsp;&nbsp;<a href="mailto:info@gesist.com.ar">info@gesist.com.ar</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;
	  </td>  
  </tr>	
</table>	
</body>
</html>
