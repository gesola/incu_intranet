-- phpMyAdmin SQL Dump
-- version 5.1.1-2.fc35
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 25-11-2021 a las 14:01:03
-- Versión del servidor: 10.5.12-MariaDB
-- Versión de PHP: 8.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `gestion_incu`
--

-- --------------------------------------------------------


--
-- Estructura de tabla para la tabla `internos`
--

CREATE TABLE `internos` (
  `Id` int(11) NOT NULL,
  `area` text DEFAULT NULL,
  `oficina` text DEFAULT NULL,
  `prefijo` text DEFAULT NULL,
  `apellido` text DEFAULT NULL,
  `nombre` text DEFAULT NULL,
  `interno` text DEFAULT NULL,
  `telefono` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `internos`
--

INSERT INTO `internos` (`Id`, `area`, `oficina`, `prefijo`, `apellido`, `nombre`, `interno`, `telefono`) VALUES
(1, 'RECEPCIÓN', '', '', '', '', '*534', ''),
(2, 'CONMUTADOR', '', '', 'GARAGUSO', 'Ignacio', '*0', ''),
(3, 'CONMUTADOR', '', '', 'NOEL', 'María', '*0', ''),
(4, 'CONMUTADOR', '', '', 'GUTKOWSKI', 'Carla', '*0', ''),
(5, 'SECRETARÍA DE PRESIDENCIA', '', 'Srta.', 'ATRIO', 'Marina', '*104', ''),
(6, 'SECRETARÍA DE PRESIDENCIA', '', 'Srta.', 'SEBOK', 'Bárbara', '*105', ''),
(7, 'SECRETARÍA DE PRESIDENCIA', '', 'Sr.', 'MANES', 'Carlos', '*105', ''),
(8, 'RELACIONES INTERNACIONALES', '', 'Dr.', 'MADERA', 'Sergio', '*240', ''),
(9, 'RELACIONES INTERNACIONALES', '', 'Lic.', 'CAMPOS', 'Valeria', '*240', ''),
(10, 'INVESTIGACIÓN', '', 'Dr.', 'TANUS', 'Eduardo', '*130', ''),
(11, 'INVESTIGACIÓN', '', 'Dr.', 'PERALTA', 'Jorge', '*130', ''),
(12, 'INVESTIGACIÓN', '', 'Srta.', 'VALENTI', 'Silvina', '*130', ''),
(13, 'INVESTIGACIÓN', '', 'Dra.', 'FRANSECE', 'Miriam', '*130', ''),
(14, 'UNIDAD AUDITORÍA INTERNA', '', 'Lic.', 'KOMAID VAN GELDEREN', 'José', '*190', ''),
(15, 'UNIDAD AUDITORÍA INTERNA', '', 'Lic.', 'EZCURRA JAHN', 'Victoria', '*191', ''),
(16, 'UNIDAD AUDITORÍA INTERNA', '', 'Dr.', 'SAIED', 'Gustavo', '*191', ''),
(17, 'ASUNTO JURÍDICOS', '', 'Dra.', 'CARBALLA', 'Adriana', '*501', ''),
(18, 'ASUNTO JURÍDICOS', '', 'Dr.', 'IUDICISSA', 'Héctor', '*500', ''),
(19, 'ASUNTO JURÍDICOS', '', 'Srta.', 'DÍAZ', 'Gabriela', '*500', ''),
(20, 'ASUNTO JURÍDICOS', '', 'Srta.', 'GENOVESE', 'Carolina', '*500', ''),
(21, 'SOPORTE TÉCNICO', '', 'Sra.', 'JADRA TAU', 'Roxana', '*188 y *189', ''),
(22, 'SOPORTE TÉCNICO', '', 'Sr.', 'BATISTA', 'Rosario', '*188 y *189', ''),
(23, 'SOPORTE TÉCNICO', '', 'Sr.', 'VIDAURRE', 'Rafael', '*188 y *189', ''),
(24, 'SOPORTE TÉCNICO', '', 'Sr.', 'EBSONOSKY', 'Lucas', '*188 y *189', ''),
(25, 'BANCO DE INMUNOSUPRESORES', '', 'Farm.', 'GARCÍA', 'Julieta', '*560', ''),
(26, 'BANCO DE INMUNOSUPRESORES', '', 'Sr.', 'MANES', 'Carlos', '*563', ''),
(27, 'COMUNICACIÓN SOCIAL', '', 'Lic.', 'FONTANA', 'Roxana', '*510', ''),
(28, 'COMUNICACIÓN SOCIAL', '', 'Sr.', 'ESPINOSA', 'Walter', '*510', ''),
(29, 'COMUNICACIÓN SOCIAL', 'Secretaría', 'Srta.', 'CASTRO', 'Gabriela', '*511', ''),
(30, 'COMUNICACIÓN SOCIAL', 'Secretaría CODEIN', 'Srta.', 'GARCÍA NAYA', 'Fernanda', '*522', ''),
(31, 'COMUNICACIÓN SOCIAL', 'Prensa', 'Lic.', 'RABOTNIKOF', 'Pablo', '*520', ''),
(32, 'COMUNICACIÓN SOCIAL', 'Prensa', 'Lic.', 'ROUTUROU', 'Alejandro', '*521', ''),
(33, 'COMUNICACIÓN SOCIAL', 'Prensa', 'Tec.', 'MIRAMONTES', 'Hilen', '*521', ''),
(34, 'COMUNICACIÓN SOCIAL', 'Prensa', 'Sr.', 'IMPARATO', 'Juan Carlos', '*521', ''),
(35, 'COMUNICACIÓN SOCIAL', 'Diseño Gráfico', 'Diseñadora', 'SUED', 'Yamila', '*517', ''),
(36, 'COMUNICACIÓN SOCIAL', 'Diseño Gráfico', 'Diseñadora', 'BEST', 'Lorena', '*517', ''),
(37, 'COMUNICACIÓN SOCIAL', 'Orientación a Pacientes', 'Lic.', 'BINDSTEIN', 'Geraldine', '*524', ''),
(38, 'COMUNICACIÓN SOCIAL', 'Orientación a Pacientes', 'Lic.', 'TONELLI', 'Andrea', '*526', ''),
(39, 'COMUNICACIÓN SOCIAL', 'Orientación a Pacientes', 'Lic.', 'PACÍFICO', 'Mariela', '*527', ''),
(40, 'COMUNICACIÓN SOCIAL', 'Orientación a Pacientes', 'Lic.', 'DRIMA', 'Daniela', '*514', ''),
(41, 'COMUNICACIÓN SOCIAL', 'Orientación a Pacientes', '', 'URGENCIAS (9:00 a 16:00)', '', '', '15 2154 8518'),
(42, 'COMUNICACIÓN SOCIAL', 'Materiales', 'Sr.', 'NUOZZI', 'Juan', '*519', ''),
(43, 'COMUNICACIÓN SOCIAL', 'Materiales', 'Lic.', 'VILLAR', 'Hugo', '*519', ''),
(44, 'COMUNICACIÓN SOCIAL', 'Materiales', 'Srta.', 'VILLAR', 'Eva', '*519', ''),
(45, 'DIRECCIÓN CIENTÍFICO TÉCNICA', '', 'Dra.', 'BISIGNIANO', 'Liliana', '*400', ''),
(46, 'DIRECCIÓN CIENTÍFICO TÉCNICA', '', 'Sr.', 'GARCÍA GLITZ', 'Pablo', '*400', ''),
(47, 'DIRECCIÓN CIENTÍFICO TÉCNICA', 'Lista de Espera', 'Dra.', 'MAGURNO', 'Marcela', '*405', ''),
(48, 'DIRECCIÓN CIENTÍFICO TÉCNICA', 'Lista de Espera', 'Sra.', 'CRESMANI', 'Claudia', '*406', ''),
(49, 'DIRECCIÓN CIENTÍFICO TÉCNICA', 'Lista de Espera', 'Dr.', 'ANTIK', 'Ariel', '*407', ''),
(50, 'DIRECCIÓN CIENTÍFICO TÉCNICA', 'Lista de Espera', 'Srta.', 'VALIÑO', 'Leonora', '*407', ''),
(51, 'DIRECCIÓN CIENTÍFICO TÉCNICA', 'Lista de Espera', 'Sra.', 'NOSEDA', 'Mónica', '*407', ''),
(52, 'DIRECCIÓN CIENTÍFICO TÉCNICA', 'Lista de Espera', 'Sra.', 'RÍOS', 'María Marta', '*407', ''),
(53, 'DIRECCIÓN CIENTÍFICO TÉCNICA', 'Seguimiento', 'Sra.', 'FAGUNDES', 'Ester', '*401', ''),
(54, 'DIRECCIÓN CIENTÍFICO TÉCNICA', 'Seguimiento', 'Lic.', 'TAGLIAFICHI', 'Viviana', '*403', ''),
(55, 'DIRECCIÓN CIENTÍFICO TÉCNICA', 'Habilitación', 'Sra.', 'BROCOLO', 'Ana Laura', '*402', ''),
(56, 'DIRECCIÓN CIENTÍFICO TÉCNICA', 'Habilitación', 'Sra.', 'CASTILLA', 'Candela', '*402', ''),
(57, 'DIRECCIÓN CIENTÍFICO TÉCNICA', 'Fiscalización', 'Bioq.', 'BUITRAGO', 'Emiliano', '*401', ''),
(58, 'DIRECCIÓN CIENTÍFICO TÉCNICA', 'Fiscalización', 'Sr.', 'HORTA', 'Francisco', '*401', ''),
(59, 'DIRECCIÓN MÉDICA', '', 'Dr.', 'ORLANDI', 'Gabriel ', '*200', ''),
(60, 'DIRECCIÓN MÉDICA', 'Jefe Dpto. Médico Operativo', 'Dr.', 'YANKOWSKI', 'Alejandro', '*211', ''),
(61, 'DIRECCIÓN MÉDICA', 'Secretaría / Insumos / Neurología', '', 'BAU', 'Romina', '', ''),
(62, 'DIRECCIÓN MÉDICA', 'Secretaría / Insumos / Neurología', '', 'CASABE', 'Karina', '', ''),
(63, 'DIRECCIÓN MÉDICA', 'Secretaría / Insumos / Neurología', 'Sr.', 'GARCÍA GLITZ', 'Mariano', '*208', ''),
(64, 'DIRECCIÓN MÉDICA', 'Secretaría / Insumos / Neurología', 'Sr.', 'PILOTTO', 'Juan Pablo', '*207', ''),
(65, 'DIRECCIÓN MÉDICA', 'Guardia Médico Operativa', '', 'GUARDIAS ROTATIVAS', '', '*201', ''),
(66, 'DIRECCIÓN MÉDICA', 'Guardia Médico Operativa', '', 'GUARDIAS ROTATIVAS', '', '*202', ''),
(67, 'DIRECCIÓN MÉDICA', 'Guardia Médico Operativa', '', 'GUARDIAS ROTATIVAS', '', '*203', ''),
(68, 'DIRECCIÓN MÉDICA', 'Guardia Médico Operativa', '', 'GUARDIAS ROTATIVAS', '', '*204', ''),
(69, 'DIRECCIÓN MÉDICA', 'Guardia Médico Operativa', '', 'FAX', '', '*205', ''),
(70, 'DIRECCIÓN MÉDICA', 'Asesores', 'Dra.', 'BARONE', 'María Elisa', '*211', ''),
(71, 'DIRECCIÓN MÉDICA', 'Asesores', 'Sra.', 'CALICCHIO', 'Andrea', '*211', ''),
(72, 'DIRECCIÓN MÉDICA', 'Calidad', 'Dra.', 'COBOS', 'Marisa', '*211', ''),
(73, 'DIRECCIÓN DE ADMINISTRACIÓN', '', 'Lic.', 'KELSEY', 'Mariana', '*300', ''),
(74, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Asistente', '', 'BAU', 'Romina', '*301', ''),
(75, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Recursos Humanos', 'Dr.', 'FRANCO', 'José', '*310 y *311', ''),
(76, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Recursos Humanos', 'Dra.', 'SOTO', 'Laura', '*310 y *311', ''),
(77, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Recursos Humanos', 'Sra.', 'GUERREIRO', 'Liliana', '*310 y *311', ''),
(78, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Recursos Humanos', 'Sra.', 'RÍOS', 'Laura', '*310 y *311', ''),
(79, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Recursos Humanos', 'Sr.', 'COLLIA', 'Ariel', '*310 y *311', ''),
(80, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Recursos Humanos', 'Sra:', 'HAY', 'Solange', '*310 y *311', ''),
(81, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Liquidación de Haberes', 'Sr.', 'PUELLA', 'Patricio', '*311', ''),
(82, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Compras', 'Dra.', 'PETRAGLIA', 'Micaela', '*322', ''),
(83, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Compras', 'Sr.', 'CAMPOS', 'Luciano', '*321', ''),
(84, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Compras', 'Sra.', 'GEHL', 'Laura', '*321', ''),
(85, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Compras', 'Sra.', 'RODRIGUEZ', 'Patricia', '*320', ''),
(86, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Compras', '', 'FAX', '', '*320', ''),
(87, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Tesorería', 'Sra.', 'HORAK', 'Adriana', '*330', ''),
(88, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Tesorería', 'Sra.', 'DEMORTIER', 'Marcela', '*330', ''),
(89, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Tesorería', 'Sr.', 'GARCÍA PUGA', 'Cristian', '*331', ''),
(90, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Tesorería', 'Sr.', 'ZERBONIA', 'Nicolás', '*331', ''),
(91, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Presupuesto', 'Cdor.', 'CLEMENTI', 'Adrián', '*340', ''),
(92, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Presupuesto', 'Sr.', 'GARAY', 'Ariel', '*340', ''),
(93, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Presupuesto', 'Sra.', 'PETRINA', 'Soledad', '*342', ''),
(94, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Presupuesto', 'Sra.', 'MACCHIO', 'Laura', '*342', ''),
(95, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Facturación', 'Cdor.', 'HEINDRIKXS', 'Pedro', '*350', ''),
(96, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Facturación', 'Sr.', 'GUERRA', 'Fernando', '*350', ''),
(97, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Facturación', 'Sra.', 'DE RIVAS', 'Miriam', '*350', ''),
(98, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Facturación', 'Sr.', 'GUILLAUME', 'Fernando', '*350', ''),
(99, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Facturación', 'Sr.', 'CATAROSSI', 'Carlos', '*350', ''),
(100, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Mesa de Entradas', 'Sr.', 'DI GIROLAMO', 'Hugo', '*305', ''),
(101, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Mesa de Entradas', 'Sr.', 'STEFANI', 'Marcelo', '*305', ''),
(102, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Mesa de Entradas', 'Sra.', 'SOTO', 'Cecilia', '*305', ''),
(103, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Pasajes', 'Srta.', 'MEZA', 'Fátima', '*360', ''),
(104, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Pasajes', 'Srta.', 'CORREA', 'Yanina', '*360', ''),
(105, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Capacitación', 'Sra.', 'ZAMUDIO', 'Paula', '*361', ''),
(106, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Mantenimiento y Servicios Generales', 'Sr.', 'PARDO', 'Héctor', '*371', ''),
(107, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Mantenimiento y Servicios Generales', 'Sr.', 'FERNÁNDEZ', 'Benjamín', '*371', ''),
(108, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Mantenimiento y Servicios Generales', 'Sr.', 'GUTIERREZ', 'Marcos', '*371', ''),
(109, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Mantenimiento y Servicios Generales', 'Sr.', 'LASBISTES', 'Nazareno', '*370', ''),
(110, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Mantenimiento y Servicios Generales', 'Sr.', 'ROSAS', 'Jorge', '*370', ''),
(111, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Mantenimiento y Servicios Generales', 'Sr.', 'ROLÓN', 'Alberto', '*370', ''),
(112, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Choferes', 'Sr.', 'CAMPOS', 'Emilio', '*373', ''),
(113, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Choferes', 'Sr.', 'RIZZI', 'Luis', '*373', ''),
(114, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Choferes', 'Sr.', 'MARTINELLI', 'Daniel ', '*373', ''),
(115, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Choferes', 'Sr.', 'IBARRA', 'Ezequiel', '*373', ''),
(116, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Choferes', 'Sr.', 'ORTONE', 'Néstor', '*373', ''),
(117, 'DIRECCIÓN DE ADMINISTRACIÓN', 'Choferes', 'Sr.', 'GUTIERREZ', 'Marcos', '*373', ''),
(118, 'GREMIOS', '', '', 'ATE', '', '*549', ''),
(119, 'GREMIOS', '', '', 'UPCN', '', '*380', ''),
(120, 'DIRECCIÓN CPH', 'Dirección', 'Bioq.', 'RODRIGUEZ CARDOZO', 'B', '700', '5533-1300 y 4304-2038/3852/4059'),
(121, 'DIRECCIÓN CPH', 'Dirección', 'Dr.', 'CANEL', 'Oscar', '700', '5533-1300 y 4304-2038/3852/4060'),
(122, 'DIRECCIÓN CPH', 'Dirección', 'Cdora.', 'TROBETTA', 'Roxana', '700', '5533-1300 y 4304-2038/3852/4061'),
(123, 'DIRECCIÓN CPH', 'Dirección', 'Bioq.', 'GALARZA', 'Pablo', '700', '5533-1300 y 4304-2038/3852/4062'),
(124, 'DIRECCIÓN CPH', 'Dirección', 'Lic.', 'DI TOCCO', 'Florencia', '700', '5533-1300 y 4304-2038/3852/4063'),
(125, 'DIRECCIÓN CPH', 'Captación Donantes', 'Sra.', 'ARIAS', 'Laura', '704/705', '5533-1300 y 4304-2038/3852/4064'),
(126, 'DIRECCIÓN CPH', 'Captación Donantes', 'Tec.', 'DÍAZ', 'Mariela', '704/705', '5533-1300 y 4304-2038/3852/4065'),
(127, 'DIRECCIÓN CPH', 'Captación Donantes', 'Sr.', 'ROMERO', 'Sebastián', '704/705', '5533-1300 y 4304-2038/3852/4066'),
(128, 'DIRECCIÓN CPH', 'Captación Donantes', 'Lic.', 'CUBERO', 'Graciela', '714', '5533-1300 y 4304-2038/3852/4067'),
(129, 'DIRECCIÓN CPH', 'Captación Donantes', 'Sr.', 'CARCABAL', 'Fernando', '714', '5533-1300 y 4304-2038/3852/4068'),
(130, 'DIRECCIÓN CPH', 'Captación Donantes', 'Sr.', 'ROMERO', 'Osmar', '714', '5533-1300 y 4304-2038/3852/4069'),
(131, 'DIRECCIÓN CPH', 'Búsqueda Nacional', 'Lic.', 'ONOFRI', 'Adriana', '708/709', '5533-1300 y 4304-2038/3852/4070'),
(132, 'DIRECCIÓN CPH', 'Búsqueda Nacional', 'Srta.', 'BAZÁN', 'Rocío', '708/709', '5533-1300 y 4304-2038/3852/4071'),
(133, 'DIRECCIÓN CPH', 'Búsqueda Nacional', 'Tec.', 'CORREA', 'Andrea', '708/709', '5533-1300 y 4304-2038/3852/4072'),
(134, 'DIRECCIÓN CPH', 'Búsqueda Nacional', 'Dra:', 'FORMISANO', 'Sandra', '708/709', '5533-1300 y 4304-2038/3852/4073'),
(135, 'DIRECCIÓN CPH', 'Búsqueda Nacional', 'Sr.', 'SAGUIER', 'Cristian', '708/709', '5533-1300 y 4304-2038/3852/4074'),
(136, 'DIRECCIÓN CPH', 'Búsqueda Internacional', 'Sr.', 'ALFARO', 'Gervasio', '711', '5533-1300 y 4304-2038/3852/4075'),
(137, 'DIRECCIÓN CPH', 'Búsqueda Internacional', 'Srta.', 'LASTRA', 'Julieta', '711', '5533-1300 y 4304-2038/3852/4076'),
(138, 'DIRECCIÓN CPH', 'Búsqueda Internacional', 'Sr.', 'FONTANA', 'Fernando', '711', '5533-1300 y 4304-2038/3852/4077'),
(139, 'DIRECCIÓN CPH', 'Búsqueda Internacional', 'Lic.', 'ZORILLA', 'Diego', '711', '5533-1300 y 4304-2038/3852/4078'),
(140, 'DIRECCIÓN CPH', 'Búsqueda Internacional', 'Srta.', 'FERNÁNDEZ', 'Maite', '711', '5533-1300 y 4304-2038/3852/4079'),
(141, 'DIRECCIÓN CPH', 'Facturación', 'Sr.', 'CANEDA', 'Juan Manuel', '706/707', '5533-1300 y 4304-2038/3852/4080'),
(142, 'DIRECCIÓN CPH', 'Facturación', 'Sra.', 'MADRID', 'Sandra', '706/707', '5533-1300 y 4304-2038/3852/4081'),
(143, 'DIRECCIÓN CPH', 'Facturación', 'Sra.', 'RIZZI', 'Teresa', '706/707', '5533-1300 y 4304-2038/3852/4082'),
(144, 'DIRECCIÓN CPH', 'Facturación', 'Lic.', 'BORDA', 'Ignacio', '706/707', '5533-1300 y 4304-2038/3852/4083'),
(145, 'DIRECCIÓN CPH', 'Facturación', 'Lic.', 'FARIAS', 'Ignacio', '713', '5533-1300 y 4304-2038/3852/4084'),
(146, 'DIRECCIÓN CPH', 'Facturación', 'Sra.', 'GRATTONI', 'Alejandra', '713', '5533-1300 y 4304-2038/3852/4085'),
(147, 'DIRECCIÓN CPH', 'Facturación', 'Sra.', 'TRAUT', 'Nancy', '713', '5533-1300 y 4304-2038/3852/4086'),
(148, 'DIRECCIÓN CPH', 'Facturación', 'Sra.', 'GARCÍA DÍAZ', 'Marcela', '713', '5533-1300 y 4304-2038/3852/4087'),
(149, 'DIRECCIÓN INFORMÁTICA', 'Dirección', 'Lic.', 'HANSEN KROG', 'Daniela', '800', '5533-1300 y 4304-2038/3852/4088'),
(150, 'DIRECCIÓN INFORMÁTICA', 'Coordinación de Proyectos', 'Lic.', 'DI MARE', 'Andrea', '812', '5533-1300 y 4304-2038/3852/4089'),
(151, 'DIRECCIÓN INFORMÁTICA', 'Coordinación de Proyectos', 'Ing.', 'PROCOPIO', 'Démian', '812', '5533-1300 y 4304-2038/3852/4090'),
(152, 'DIRECCIÓN INFORMÁTICA', 'Análisis y Desarrollo', '', 'BONOMI', 'Christian', '810', '5533-1300 y 4304-2038/3852/4091'),
(153, 'DIRECCIÓN INFORMÁTICA', 'Análisis y Desarrollo', '', 'CHACHO TAYLOR', 'Esteban', '810', '5533-1300 y 4304-2038/3852/4092'),
(154, 'DIRECCIÓN INFORMÁTICA', 'Análisis y Desarrollo', '', 'JADRA TAU', 'Celeste', '810', '5533-1300 y 4304-2038/3852/4093'),
(155, 'DIRECCIÓN INFORMÁTICA', 'Análisis y Desarrollo', '', 'KESSLER', 'Sergio', '810', '5533-1300 y 4304-2038/3852/4094'),
(156, 'DIRECCIÓN INFORMÁTICA', 'Análisis y Desarrollo', '', 'LOPEZ', 'Manuel', '810', '5533-1300 y 4304-2038/3852/4095'),
(157, 'DIRECCIÓN INFORMÁTICA', 'Análisis y Desarrollo', '', 'MENDEZ', 'Alejandro', '810', '5533-1300 y 4304-2038/3852/4096'),
(158, 'DIRECCIÓN INFORMÁTICA', 'Análisis y Desarrollo', '', 'PETELSKI', 'Juan', '810', '5533-1300 y 4304-2038/3852/4097'),
(159, 'DIRECCIÓN INFORMÁTICA', 'Soporte Técnico', '', 'VESPALI', 'Ignacio', '810', '5533-1300 y 4304-2038/3852/4098'),
(160, 'DIRECCIÓN INFORMÁTICA', 'Atención Usuarios SINTRA', '', 'LUNA', 'Rocío', '830', '5533-1300 y 4304-2038/3852/4099');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `nombre` text DEFAULT NULL,
  `nivel` int(11) DEFAULT NULL,
  `id_padre` int(11) DEFAULT NULL,
  `link` text DEFAULT NULL,
  `activo` tinyint(1) DEFAULT NULL,
  `su` tinyint(1) DEFAULT NULL,
  `ad` tinyint(1) DEFAULT NULL,
  `op` tinyint(1) DEFAULT NULL,
  `us` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--


--
-- Indices de la tabla `internos`
--
ALTER TABLE `internos`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Id` (`Id`);

--
-- Indices de la tabla `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de la tabla `internos`
--
ALTER TABLE `internos`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=161;

--
-- AUTO_INCREMENT de la tabla `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;