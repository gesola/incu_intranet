<?php 
include ("../../config/config.inc");
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"> 

<html>
<head>
	<title>GAD - Gesti&oacute;n Administrativa (by GeSist)</title>
	<link rel="shortcut icon" href="../../images/favicon.gif" >

<link rel="stylesheet" type="text/css" href="../../includes/css/gadstyle.php" media="all">
<link rel="stylesheet" type="text/css" href="../../includes/css/nivelstyle.css" media="all">
<link rel="stylesheet" type="text/css" href="../../includes/css/main.css" media="all">
<link rel="stylesheet" type="text/css" href="../../includes/css/jquery.alerts.php" media="all">
<link rel="stylesheet" type="text/css" href="../../includes/css/blue/style.php" media="print, projection, screen" />
<link rel="stylesheet" type="text/css" href="../../includes/css/jquery.tablesorter.pager.php" media="all">

<script src="../../includes/js/jquery.js" language="javascript" type="text/javascript"></script> 
<script src="../../includes/js/jquery.alerts.js" language="javascript" type="text/javascript"></script> 
<script src="../../includes/js/jquery.form.js" language="javascript" type="text/javascript"></script> 
<script src="../../includes/js/jquery.tablesorter.js" language="javascript" type="text/javascript"></script>
<script src="../../includes/js/jquery.tablesorter.pager.js" language="javascript" type="text/javascript"></script>


</head>
<body bgcolor="<?php echo $colbodfond;?>" text="<?php echo $colbodfont;?>" leftmargin="2" topmargin="5" marginwidth="0" marginheight="0">

