<?php 
## GUIA DE INTERNOS. ##
//Reanudamos la sesión
@session_start();

$nombre="guia_internos";
$descripcion="Internos";

/*
// Configuro permisos.
$listado_su=1;
$modifica_su=1;
$alta_su=1;
$elimina_su=1;

$listado_ad=1;
$modifica_ad=1;
$alta_ad=1;
$elimina_ad=1;

$listado_op=1;
$modifica_op=0;
$alta_op=0;
$elimina_op=0;

$listado_us=0;
$modifica_us=0;
$alta_us=0;
$elimina_us=0;

$binario_su=$elimina_su.$alta_su.$modifica_su.$listado_su;
$binario_ad=$elimina_ad.$alta_ad.$modifica_ad.$listado_ad;
$binario_op=$elimina_op.$alta_op.$modifica_op.$listado_op;
$binario_us=$elimina_us.$alta_us.$modifica_us.$listado_us;

$su=bindec($binario_su);
$ad=bindec($binario_ad);
$op=bindec($binario_op);
$us=bindec($binario_us);

// Fin configuración de permisos.

include ("../../includes/estructura.inc");
include ("../../includes/validacion.inc");
*/

##########################
### FUNCION validado() ###
##########################
/*
function validado() {
	if (!isset($_POST['reincide']) || $_POST['reincide']==0)
            {
                main(); // Va al listado.
            } else {
			if (isset($_POST['formulario']) && $_POST['formulario']==1) { dbalta(); } // Voy a insertar registro en DB.
			if (isset($_POST['formulario']) && $_POST['formulario']==2) { dbmodif(); } // Voy a editar registro en DB. 
		    } //fin del if/else de reincide.
} ### FIN FUNCION validado ###
*/ 
    
main(); // Va al listado.
                
######################
### FUNCION main() ###
######################

function main() {
    
if (!isset($_POST['returning'])) {include ("../../includes/html5_head.inc");
$htitulo="Internos:";
$stitulo="Guia de Internos";

?>
<!-- page content -->
<!-- page content Head -->
<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3><?php print $htitulo;?> <small><?php print $stitulo;?></small></h3>
              </div>              
            </div>

<?php
}

if (isset($_POST['id'])) {$_POST['id']=$_POST['id'];}

if (!isset($_POST['reincide']))
        {
            if (!isset($_GET['fun'])){listado();} // Voy a listado.
            if (isset($_GET['fun']) && $_GET['fun']==1){listado();} // Voy a listado.    
            if (isset($_GET['fun']) && $_GET['fun']==2)
                {
                    if($_POST["ver"]==1){ver();}// Voy a consultar.
                        else {noauth();}//Voy a noauth.
                }
                
            if (isset($_GET['fun']) && $_GET['fun']==3 )
                {
                    if($_POST["alta"]==1){formulario(1);}// Voy a Alta.
                        else {noauth();}//Voy a noauth.                              
                } 
            
            if (isset($_GET['fun']) && $_GET['fun']==4)
                {
                    if($_POST["modif"]==1){formulario(2);} // Voy a Edicion.
                        else {noauth();}//Voy a noauth.                 
                }
                         
            if (isset($_GET['fun']) && $_GET['fun']==5)
                {
                    if($_POST["elimina"]==1){elimina_contacto_entidad();} // Voy a Eliminar.
                        else {noauth();}//Voy a noauth.                  
                } 
            if (isset($_GET['fun']) && $_GET['fun']==6)
                {
                    if($_POST["elimina"]==1){desactiva();} // Voy a Desactivar.
                        else {noauth();}//Voy a noauth.                  
                }
            if (isset($_GET['fun']) && $_GET['fun']==7)
                {
                    if($_POST["elimina"]==1){reactiva();} // Voy a Reactivar.
                        else {noauth();}//Voy a noauth.                  
                }                
        } else {
            if ($_POST['reincide']==2)
            {
                if (isset($_POST['ver']) && $_POST['ver']==1){
                    ver();  
                    formulario(1);
                } // Muestro nuevo registro creado y formulario de alta.
                if (isset($_POST['ver']) && $_POST['ver']==2){ver();} // Muestro el registro editado.                
                
                if (isset($_POST['falta']) && $_POST['falta']==1){alta();} // Error en ingreso de datos alta. 
                
                if (isset($_POST['modif']) && $_POST['modif']==1){
                    $_POST['id']=$_POST['id'];  
                    formulario(2);
                }
            }
        }

?>
            
          </div>
        </div>
<!-- /page content -->    
<?php                      
include ("../../includes/html5_foot.inc");
} ### FIN DE main ###


########################
### FUNCION noauth() ###
########################
function noauth() {

$subtitulo="Internos";  

if (isset($_POST['merror'])) { $mensaje="<span class=\"error\">".$_POST['merror']."</span>";}
if (isset($_POST['mconfirma'])) { $mensaje="<span class=\"listconfirma\">".$_POST['mconfirma']."</span>";}  

?>              
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php if(isset($subtitulo)){print $subtitulo;}?><small><?php if(isset($mensaje)) {print $mensaje;} ?></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>                    
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
   
                  <div class="x_content"> 
                      
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>No tiene permisos para ingresar a este m&oacute;dulo.</h3>
            </div>
        </div>
    </div>
</div>  
                    
<!--                   <div class="ln_solid"></div> -->
                    <div class="col-md-12 col-md-offset-3">
                     <a href="guia_internos.php?fun=1" class="btn btn-primary"></i> Volver </a>
                    </div> 
                  </div>
                 </div>                
              </div>
            </div>
<!--
          </div>
        </div>
-->
<?php
} ### Fin de noauth ###


#########################
### FUNCION listado() ###
#########################
function listado() {

$subtitulo="Listado de Internos";

if (isset($_POST['merror'])) { $mensaje="<span class=\"error\">".$_POST['merror']."</span>";}
if (isset($_POST['mconfirma'])) { $mensaje="<span class=\"listconfirma\">".$_POST['mconfirma']."</span>";}

require_once(dirname(__FILE__) . "/../../clases/cliente.class.inc");
$miCliente = new Cliente();
//$resultreg = $miCliente->listaCliList();
$resultreg = $miCliente->listaInterno();
$cantreg = $miCliente->cantidadUltimaTabla();

if ($cantreg==0)
{
?>
<div class="clearfix"></div>    
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2><?php if(isset($subtitulo)){print $subtitulo;}?><small><?php if(isset($mensaje)) {print $mensaje;} ?></small></h2>
            <ul class="nav navbar-right panel_toolbox"> 
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>                   
                <li class="dropdown">
                <?php if(isset($_POST["alta"]) && $_POST["alta"]==1){ ?> <a href="guia_internos.php?fun=3" title="Nuevo Registro"><i class="fa fa-file"></i></a> <?php } ?>
                </li>
                     
                <li><a class="close-link"><i class="fa fa-close"></i></a></li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <h3 align="center"><?php print "NO HAY REGISTROS PARA LISTAR.";?></h3>                
        </div>
    </div>
</div>
<?php 
} else {
?>
            <div class="clearfix"></div>    
                <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php if(isset($subtitulo)){print $subtitulo;}?><small><?php if(isset($mensaje)) {print $mensaje;} ?></small></h2>
                    <ul class="nav navbar-right panel_toolbox"> 
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                      <li class="dropdown">
                        <!--<a href="usuario_alta.php" title="Nuevo Usuario"><i class="fa fa-eye"></i></a>
                         
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                        -->
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                   <table id="datatable-buttons" class="table table-striped table-bordered">                                          
                      <thead>
                        <tr>
<!--                          <th><?php if(isset($_POST["alta"]) && $_POST["alta"]==1){ ?> <a href="guia_internos.php?fun=3" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="Nuevo Registro"><i class="fa fa-file-o"></i></a> <?php } ?></th> -->
                          <th>&Aacute;rea</th>                           
                          <th>Oficina</th>                    
                          <th>Agente</th>
                          <th>Interno</th>
                          <th>Tel&eacute;fono</th>
                          <!-- <th>Proveedor</th> -->
                          <!-- <th>Contacto</th> -->
                          <!-- <th>Activo</th> -->
                          <!-- <th><?php if(isset($_POST["alta"]) && $_POST["alta"]==1){ ?> <a href="guia_internos.php?fun=3" class="btn btn-dark btn-xs" data-toggle="tooltip" data-placement="top" title="Nuevo Registro"><i class="fa fa-eye"></i></a> <?php } ?></th> -->
                       
                        </tr>
                      </thead>
                      <tfoot>
                        <tr>
                          <th>&Aacute;rea</th>                           
                          <th>Oficina</th>                    
                          <th>Agente</th>
                          <th>Interno</th>
                          <th>Tel&eacute;fono</th>                      
                        </tr>
                      </tfoot> 
                      
                      <tbody>
<?php
while($rowreg = $miCliente->fetch_array($resultreg))         
			{       
?>

  <tr>
<?php /*      
        <td>
        <?php if(isset($_POST["ver"]) && $_POST["ver"]==1){ ?>
            <a href="guia_internos.php?fun=2&id=<?php print $rowreg["id"];?>" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="Ver"><i class="fa fa-eye"></i></a>
        <?php }
  
        if(isset($_POST["modif"]) && $_POST["modif"]==1 && $rowreg["activo"]=='1'){ ?>
            <a href="guia_internos.php?fun=4&id=<?php print $rowreg["id"];?>" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil"></i></a>
        <?php } 

        if(isset($_POST["elimina"]) && $_POST["elimina"]==1){        
            if ( $rowreg["activo"]=='1') { ?>
        <span data-toggle="tooltip" data-placement="top" title="Desactivar">
        <a href="guia_internos.php?fun=6&id=<?php print $rowreg["id"];?>" class="btn btn-danger btn-xs" class="btn btn-danger btn-xs" data-toggle="confirmation" data-btn-ok-label="Continuar" data-btn-ok-icon="glyphicon glyphicon-share-alt"
        data-btn-ok-class="btn-success" data-btn-cancel-label="Cancelar" data-btn-cancel-icon="glyphicon glyphicon-ban-circle" data-btn-cancel-class="btn-danger" 
        data-title="¿Est&aacute; Seguro?" data-content=""><i class="fa fa-times"></i></a>
        </span>
<?php } else { ?>
        <span data-toggle="tooltip" data-placement="top" title="Reactivar">     
        <a href="guia_internos.php?fun=7&id=<?php print $rowreg["id"];?>" class="btn btn-success btn-xs" class="btn btn-danger btn-xs" data-toggle="confirmation" data-btn-ok-label="Continuar" data-btn-ok-icon="glyphicon glyphicon-share-alt"
        data-btn-ok-class="btn-success" data-btn-cancel-label="Cancelar" data-btn-cancel-icon="glyphicon glyphicon-ban-circle" data-btn-cancel-class="btn-danger" 
        data-title="¿Est&aacute; Seguro?" data-content=""><i class="fa fa-check"></i></a>        
        </span>
<?php } // Fin if activo 
    } // Fin if elimina 
    ?>
            
    </td> 
*/ ?>
    <td><?php echo $rowreg["area"];?></td>      
    <td><?php echo $rowreg["oficina"];?></td>
    <td><?php echo $rowreg["prefijo"]." ".$rowreg["apellido"].", ".$rowreg["nombre"];?></td>
    <td><?php echo $rowreg["interno"];?></td> 
    <td><?php echo $rowreg["telefono"];?></td>       
</tr>
	<?php } // Fin While ?>

</tbody>
</table>                  
                    
                  </div>
                </div>
              </div>
<!--            
           </div>
        </div>
-->
        <!-- /page content -->  
<?php       
} // Final del if que muestra listado si es que hay 1 o mas tipos de usuarios.

} ### FINAL DE guia_internosplantillalist

/*
#####################
### FUNCION ver() ###
#####################
function ver() {
//Echo "LLegue hasta Ver";

$subtitulo="Ver Entidad";  

if (isset($_POST['merror'])) { $mensaje="<span class=\"error\">".$_POST['merror']."</span>";}
if (isset($_POST['mconfirma'])) { $mensaje="<span class=\"listconfirma\">".$_POST['mconfirma']."</span>";}  

?>              
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php if(isset($subtitulo)){print $subtitulo;}?><small><?php if(isset($mensaje)) {print $mensaje;} ?></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>                    
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
   
                  <div class="x_content">        
<?php 
require_once(dirname(__FILE__) . "/../../clases/cliente.class.inc");

$miCliente = new Cliente();
//if (isset($_GET['id'])){echo "<br>Get: ".$_GET['id'];}
//if (isset($_POST['id'])){echo "<br>Post: ".$_POST['id'];}

if (isset($_GET['id'])){$idcliente = $_GET['id']; }
if (isset($_POST['id'])){$idcliente = $_POST['id']; }

if (isset($_GET['id'])){$resultusu = $miCliente->listaEntidadPri($_GET['id']); }
if (isset($_POST['id'])){$resultusu = $miCliente->listaEntidadPri($_POST['id']); }
$cantusu = $miCliente->cantidadUltimaTabla(); 
//echo "<br> Cantidad: ".$cantusu;

// Query para Domicilio.
$resultdom = $miCliente->listaDomicilio($idcliente);
$cantdom = $miCliente->cantidadUltimaTabla();

// Query para Medio de Comunicación.
$resultmed = $miCliente->listaMedio($idcliente);
$cantmed = $miCliente->cantidadUltimaTabla();

// Query para contactos Empresa.
$resultctcempresa = $miCliente->listaContactosEmpresa($idcliente);
$cantctcempresa = $miCliente->cantidadUltimaTabla();

// Query para Empresas contacto.
$resultempresactc = $miCliente->listaEmpresasContacto($idcliente);
$cantempresactc = $miCliente->cantidadUltimaTabla();

$resultempresact2 = $miCliente->listaEmpresasContacto2($idcliente);
$cantempresact2 = $miCliente->cantidadUltimaTabla();

while($rowusu = $miCliente->fetch_array($resultusu))
    { 
?>
                    <!-- Datos a Mostrar --> 
                    <div class="col-md-4 col-sm-12 col-xs-12 form-group has-feedback">
                        <h4>
                            <label>Relaci&oacute;n con la Entidad:&nbsp;&nbsp;
                                <?php $re=0; ?>
                                <i><?php if($rowusu["cliente"]==1){?> Cliente <?php $re++;} ?>
                                <?php if($rowusu["proveedor"]==1){ if ($re>0){ print " / ";}?> Proveedor <?php $re++;} ?>
                                <?php if($rowusu["contacto"]==1){ if ($re>0){ print " / ";}?> Contacto <?php $re++; } ?></i>
                        </h4>
                            </label>

                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                        <h4>
                            <label>Tipo de Entidad:&nbsp;&nbsp;<i><?php print $rowusu["tipo_cli"];?></i></label>
                        </h4>
                    </div>
                  
                     <div class="col-md-4 col-sm-12 col-xs-12 form-group has-feedback">
                         <h4>
                            <label>Clase de Entidad:&nbsp;&nbsp;
                                <i><?php if ($rowusu['fis_jud']=='1') {?> Persona F&iacute;sica.<?php }?> 
                                <?php if ($rowusu['fis_jud']=='2') {?> Persona Jur&iacute;dica.<?php }?></i>
                            </label>
                         </h4>
                     </div>
                                      
                       <div class="form-group"></div>
<?php if ($rowusu['fis_jud']=='1') {?>
                        <div class="col-md-3 col-sm-12 col-xs-12 form-group has-feedback">                         
                            <h4><label>Apellido:&nbsp;&nbsp;<i><?php print $rowusu['apellido_particular'];?></i></label></h4>		
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12 form-group has-feedback">                         
                            <h4><label>Nombre:&nbsp;&nbsp;<i><?php print $rowusu['nombre_particular'];?></i></label></h4>
                        </div>
<?php }?>
                       
<?php if ($rowusu['fis_jud']=='2') {?>                       

                         <div class="col-md-3 col-sm-12 col-xs-12 form-group has-feedback">    
                             <h4><label>Empresa:&nbsp;&nbsp;<i><?php print $rowusu['nombre_empresa'];?></i></label></h4>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12 form-group has-feedback">
                            <h4><label>Raz&oacute;n Social:&nbsp;&nbsp;<i><?php print $rowusu['razon_social'];?></i></label></h4>
                        </div>
<?php }?>

                     <!-- <div class="ln_solid"></div> -->
                      <div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback">
                          <h4><label>Facturaci&oacute;n:&nbsp;&nbsp;<i><?php print $rowusu["iva"]." - ".$rowusu["tipo_doc"].": ".$rowusu['num_doc_afip'];?></i></label></h4>                                                                   
                      </div>
                   
<!--                    <div class="form-group"></div>                     
                    <div class="ln_solid"></div>                     
-->
<!-- TABLA DOMICILIOS -->
              <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                <div class="x_panel">
                  <div class="x_title">
                       <label>Domicilio</label>                      
                    <!-- <h2>Domicilio</h2> -->
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
<!--                      
                      <li class="dropdown">    
                          <a title="Nuevo Domicilio" id="agrega_dom"><i class="fa fa-plus-square"></i></a></li>

                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Setings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
-->
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <table class="table">
                      <thead>
                        <tr>
                          <th>Tipo</th>
                          <th>Calle</th>
                          <th>N&uacute;mero</th>
                          <th>Piso</th>
                          <th>Oficina/Local</th>
                          <th>C&oacute;digo Postal</th>
                          <th>Ciudad</th>
                          <th>Provincia</th>
                          <th>Pa&iacute;s</th>                                                 
                        </tr>
                      </thead>
                      <tbody>            
                    <?php while($rowdom = $miCliente->fetch_array($resultdom)) { ?>
                        <tr>                            
                            <td><label><?php print $rowdom["domicilio"]; ?></label></td>
                            <td><?php print $rowdom["calle"]; ?></td>
                            <td><?php print $rowdom["numero"]; ?></td>
                            <td><?php print $rowdom["piso"]; ?></td>            
                            <td><?php print $rowdom["of_local"]; ?></td>
                            <td><?php print $rowdom["cp"]; ?></td>
                            <td><?php print $rowdom["nombre_ciudad"]; ?></td>
                            <td><?php print $rowdom["nombre_provincia"]; ?></td>
                            <td><?php print $rowdom["nombre_pais"]; ?></td> 
                        </tr>                        
                    <?php  } ?>                        
                      </tbody>
                    </table>

                  </div>
                </div>
              </div>
 <!-- /TABLA DOMICILIOS --> 

 <!-- TABLA MEDIOS -->
               <div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback">
                <div class="x_panel"> 
                  <div class="x_title">
                       <label>Medio de Comunicaci&oacute;n</label>                      
                    <!-- <h2>Medio de Comunicaci&oacute;n</h2> -->
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
<!--                    <div class="clearfix"></div> -->
                  </div>
                  <div class="x_content">
                   <?php while($rowmed = $miCliente->fetch_array($resultmed)) { ?>
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">                                            
                          <label><?php print $rowmed["medio"]; ?>:&nbsp;&nbsp;</label><?php print $rowmed["datos_medio"]; ?> 
                        </div>                            
                    <?php  } ?>
                  </div>
                </div> 
              </div>
 <!-- /TABLA MEDIOS -->
 
<?php if ($rowusu['fis_jud']==1 && $cantempresact2 > 0) { ?> 
 <!-- TABLA CONTACTOS FISICO -->
               <div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback">
                <div class="x_panel">
                  <div class="x_title">
                       <label>Contacto de Empresas</label>                      
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <?php while($rowempresact2 = $miCliente->fetch_array($resultempresact2)) { ?>
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">                              
                            <label><?php print $rowempresact2["empresa"]; ?>:&nbsp;&nbsp;</label><?php print $rowempresact2["puesto"]; ?>                         
                        </div>                                                                            
<?php                     
                     } // Fin while 
?>


                  </div>
                </div>
              </div>
 <!-- /TABLA CONTACTOS FISICO --> 
<?php } ?>
 
<?php if ($rowusu['fis_jud']==2 && $cantctcempresa > 0) { ?> 
 <!-- TABLA CONTACTOS EMPRESA -->
               <div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback">
                <div class="x_panel">
                  <div class="x_title">
                       <label>Contactos</label>                      
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table class="table">
<?php
$contpuesto=0; 
                 while($rowctcempresa = $miCliente->fetch_array($resultctcempresa)) { 
                     if($contpuesto<>$rowctcempresa["id_contacto"]) {
                            if ($contpuesto<>0) {
?>
                    </tbody>
                        <?php                       
                            }                        
                        ?>                   
                      <thead>                        
                          <tr>                         
                                <th><?php print $rowctcempresa["contacto"]." - ".$rowctcempresa["puesto"]; ?></th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr>                              
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;<?php print $rowctcempresa["medio"].": ".$rowctcempresa["datos_medio"];?></td>                
                        </tr>                                                                            
                      </tbody>
<?php 
                    $contpuesto=$rowctcempresa["id_contacto"];                    
                    } else { 
?>                           
                          <tr>                              
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;<?php print $rowctcempresa["medio"].": ".$rowctcempresa["datos_medio"];?></td>                
                        </tr>
<?php
                    $contpuesto=$rowctcempresa["id_contacto"];
                            } //fin if                      
                     } // Fin while 
?>
                      </tbody>                        
                    </table>

                  </div>
                </div>
              </div>
 <!-- /TABLA CONTACTOS EMPRESA --> 
<?php } ?>
 
                    <div class="col-md-3 col-sm-12 col-xs-12 has-feedback">
                        <label>Fecha de Alta:&nbsp;&nbsp;</label><i><?php print $rowusu["fechavis"];?></i>
                    </div>
                  
                    <div class="col-md-3 col-sm-12 col-xs-12 form-group has-feedback">                         
                        <label>Operador:&nbsp;&nbsp;</label><i><?php print $rowusu["nombres"]." ".$rowusu["apellidos"];?></i>
                    </div>
                   
                    <div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback">                         
                        <label>Observaciones:&nbsp;&nbsp;</label><i><?php print $rowusu["observaciones"];?></i>
                    </div>                       
                    <!-- Campos del Formulario --> 
<?php

    } // Fin del While
?>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback"></div> 
                    
                    <div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback"></div>
                    <div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback">                        
                    <!-- <div class="col-md-12 col-md-offset-3"> -->                       
                     <a href="guia_internos.php?fun=1" class="btn btn-primary"></i> Volver </a>
                    </div> 
                  </div>
                 </div>                
              </div>
            </div>
<!--
          </div>
        </div>
-->
<?php 
} ### Fin de ver ###


############################
### FUNCION formulario() ###
############################

function formulario($form) {

if ($form==1){$subtitulo="Alta de Entidad";}
if ($form==2){$subtitulo="Edici&oacute;n de Entidad";}
  
if (isset($_POST['merror'])) { $mensaje="<span class=\"error\">".$_POST['merror']."</span>";}
if (!isset($_POST['mconfirma'])) 
    { 
        if ($form==1){$mensaje="<span class=\"listconfirma\">Crear nuevo registro.</span>";}
        if ($form==2){$mensaje="<span class=\"listconfirma\">Editar registro.</span>";}        
    } else {
        $mensaje="<span class=\"listconfirma\"> ".$_POST['mconfirma']." </span>";
    }        
//$_SESSION["contprov"] = 0; // Configuro contador de domicilio

require_once(dirname(__FILE__) . "/../../clases/cliente.class.inc");
$miCliente = new Cliente();

require_once(dirname(__FILE__) . "/../../clases/administracion.class.inc");
$miAdministracion = new Administracion();

require_once(dirname(__FILE__) . "/../../clases/validacion.class.inc");			    
$miValidacion = new Validacion();

require_once(dirname(__FILE__) . "/../../clases/localizacion.class.inc");
$miLocalizacion = new Localizacion();


// Query para tipo de cliente.
$resulttc = $miCliente->listaTipoCli();
$canttc = $miCliente->cantidadUltimaTabla();


// Query para tipo de Domicilio.
$resulttd = $miCliente->listaTipoDom();
$canttd = $miCliente->cantidadUltimaTabla();
//echo "Cantidad: ".$canttd;


// Query para condición de iva.
$resultci = $miAdministracion->listaCondIva();
$cantci = $miAdministracion->cantidadUltimaTabla();

// Query para tipo de documento (Afip)
$resulttdoc = $miAdministracion->listaTipoDoc();
$canttdoc = $miAdministracion->cantidadUltimaTabla();

// Query para seleccionar usuarios activos.
$uresult = $miValidacion->listaUsuarios(1); 
$ucant   = $miValidacion->cantidadUltimaTabla();

// Querys para listar paises, provinicias y ciudades.
$paisresult = $miLocalizacion->listaPaises();
$pacant = $miLocalizacion->cantidadUltimaTabla();

$provinciaresult = $miLocalizacion->listaProvincias();
$prcant = $miLocalizacion->cantidadUltimaTabla();

$ciudadresult = $miLocalizacion->listaCiudades();
$cicant = $miLocalizacion->cantidadUltimaTabla();

// Query para tipo de Medio de Comunicacion.
$medioresult = $miCliente->listaTipoMedioCom();
$mediocant = $miCliente->cantidadUltimaTabla();

// Query para Contacto.
$resultcnt = $miCliente->listaContacto(2);
$cantcnt = $miCliente->cantidadUltimaTabla();

if ($form==2)
    {

if (!isset($_GET['id']) && isset($_POST['id'])){$_GET['id']=$_POST['id'];}
//if (isset($_GET['id'])){echo "<br>Get: ".$_GET['id'];}
//if (isset($_POST['id'])){echo "<br>Post: ".$_POST['id'];}

    // Query principal para edición    
    $resultusu = $miCliente->listaEntidadPri($_GET['id']);
    $cantusu = $miCliente->cantidadUltimaTabla();

    // Query listado domicilio
    $resultreg = $miCliente->listaDomicilio($_GET['id']);
    $cantreg = $miCliente->cantidadUltimaTabla();
    
    // Query listado medios
    $resultmed = $miCliente->listaMedio($_GET['id']);
    $cantmed = $miCliente->cantidadUltimaTabla();

        while($rowusu = $miCliente->fetch_array($resultusu))
            {
                $id=$rowusu["id"];            
                $cliente=$rowusu["cliente"];
                $proveedor=$rowusu["proveedor"];                
                $contacto=$rowusu["contacto"];
                $fis_jud=$rowusu["fis_jud"];
                $id_tipo_cliente=$rowusu["id_tipo_cliente"];
                $id_pos_iva=$rowusu["id_pos_iva"];
                $id_doc_afip=$rowusu["id_doc_afip"];
                $num_doc_afip=$rowusu["num_doc_afip"];
                $nombre_empresa=$rowusu["nombre_empresa"];
                $razon_social=$rowusu["razon_social"];
                $apellido_particular=$rowusu["apellido_particular"];
                $nombre_particular=$rowusu["nombre_particular"];
                //$fecha_alta=$rowusu["fecha_alta"];
                $fechavis=$rowusu["fechavis"];
                $observaciones=$rowusu["observaciones"];
                $id_operador=$rowusu["id_operador"];              
            } // fin de while

    if ($fis_jud==1) {        
        // Query para Empresas contacto.
        $resultempresactc = $miCliente->listaEmpresasContacto($id);
        $cantempresactc = $miCliente->cantidadUltimaTabla();

        while($rowempresactc = $miCliente->fetch_array($resultempresactc)) { 
            $id_juridico=$rowempresactc["id"];                          
            $empresa=$rowempresactc["empresa"];
            $id_fisico=$rowempresactc["id_contacto"];
            $puesto_juridico=$rowempresactc["puesto"];                                            
        } // Fin while
    } // Fin If Fis_jud 1
    
    if ($fis_jud==2) {        
            // Query para contactos Empresa.
            $resultctcempresa = $miCliente->listaContactosEntidad($id);
            $cantctcempresa = $miCliente->cantidadUltimaTabla();
    } // Fin If Fis_jud 2

    # Para tabla Domicilio
   
    // Lista Tipo de Domicilio       
    $a=0;
    while($ttdrow = $miCliente->fetch_array($resulttd))
        {
            $ttdid[$a]=$ttdrow["id"]; 
            $ttddomicilio[$a]=$ttdrow["domicilio"];
            $a++;                    
        } // Fin While resulttd 
        
    // Lista de Paises, Provicias y Ciudades
    $c=0;
    while($rowpais = $miCliente->fetch_array($paisresult))
        {
            $paisid[$c]=$rowpais["id"]; 
            $paisnombre[$c]=$rowpais["nombre"];
            $c++;                    
        } // Fin While paisresult         

    $e=0;
    while($rowprovincia = $miCliente->fetch_array($provinciaresult))
        {
            $provinciaid[$e]=$rowprovincia["id"]; 
            $provincianombre[$e]=$rowprovincia["nombre"];
            $provinciaidpais[$e]=$rowprovincia["id_pais"];
            $e++;                    
        } // Fin While provinciaresult
        
    $g=0;
    while($rowciudad = $miCliente->fetch_array($ciudadresult))
        {
            $ciudadid[$g]=$rowciudad["id"]; 
            $ciudadnombre[$g]=$rowciudad["nombre"];
            $ciudadidprovincia[$g]=$rowciudad["id_prov"];            
            $g++;                    
        } // Fin While ciudadresult 
        
    $i=0;
    while($rowmedio = $miCliente->fetch_array($medioresult))
        {
            $medioid[$i]=$rowmedio["id"]; 
            $medionombre[$i]=$rowmedio["medio"];          
            $i++;                    
        } // Fin While medioresult         
        
    } // Fin if form 2
/*    
echo "<br>Id_juridico: ".$id_juridico;
echo "<br>Empresa: ".$empresa;
echo "<br>Id_Fisico: ".$id_fisico;
echo "<br>Puesto Juridico: ".$puesto_juridico;
*/
/*    
?>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php if(isset($subtitulo)){print $subtitulo;}?><small><?php if(isset($mensaje)) {print $mensaje;} ?></small></h2>

                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>

                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>

                    <div class="clearfix"></div>
                  </div>
                <!-- /page content Head -->
               
                <!-- page content Form -->                      
                  <div class="x_content">

                      <form class="form-horizontal form-label-left" name="form" action="guia_internos.php" method="POST">                      

                    <!-- Campos del Formulario -->
                    <!--
                        <div class="col-md-12 col-sm-12 col-xs-12 input__row uploader">
                            <div id="inputval" class="input-value"></div>
                            <label for="file_1"></label>
                            <input id="file_1" class="upload" style="display: none;" type="file">
                        </div>
                    -->
                    
                     <div class="col-md-4 col-sm-12 col-xs-12 form-group has-feedback">
                        <label>Clase de Entidad:</label>
                        <div class="radio">
                            <label>

    			&nbsp;&nbsp;<input type="radio" class="flat" id="fisica" name="clasecli" value="1"<?php if (isset($fis_jud) && $fis_jud=='1') {?> checked<?php }?>>&nbsp;Persona F&iacute;sica.
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" class="flat" id="juridica" name="clasecli" value="2"<?php if (isset($fis_jud) && $fis_jud=='2') {?> checked<?php }?>>&nbsp;Persona Jur&iacute;dica.

                            </label>
                        </div>
                     </div>                    
                    
                    <div class="col-md-4 col-sm-12 col-xs-12 form-group has-feedback">
                       <label>Seleccione Entidad:</label>                         
                        <div class="checkbox">
                            <label>
                              <input type="checkbox" class="flat" id="cliente" name="ccliente" <?php if(isset($cliente) && $cliente==1){?> checked="checked" <?php } ?>> Cliente
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                              
                              <input type="checkbox" class="flat" id="proveedor" name="cproveedor" <?php if(isset($proveedor) && $proveedor==1){?> checked="checked" <?php } ?>> Proveedor
                              </label>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                        <label>Tipo de Entidad:</label>
                        <select class="select2_single form-control" tabindex="-1" name="tipo_cliente" required="required" id="country-list">
		            <option value="">Seleccione...</option>
<?php 
while($rowtc = $miCliente->fetch_array($resulttc))        
		{
?>
                            <option value="<?php print $rowtc["id"];?>"<?php if (isset($id_tipo_cliente) && $rowtc["id"]==$id_tipo_cliente) {?> selected<?php }?>><?php print $rowtc["tipo_cli"];?></option>
<?php }?>
	       			</select>                     
                      </div>
                   

                                      
                       <div class="form-group"></div>

                     <div id="pers_fisica" class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                        <div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback">                         
                        <label>Apellido:</label>		
                                <input type="text" class="form-control" placeholder="Apellido Particular" name="apellido_particular" value="<?php if(isset($apellido_particular)) {print $apellido_particular;}?>">
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback">                         
                                <label>Nombre:</label>
                                <input type="text" class="form-control" placeholder="Nombre Particular" name="nombre_particular" value="<?php if(isset($nombre_particular)) {print $nombre_particular;}?>">
                        </div>
                     </div>
                     <div id="pers_juridica" class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                         <div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback">    
                         <label>Empresa:</label>
                                <input type="text" class="form-control" id="nombre_empresa" placeholder="Nombre de la Empresa" name="nombre_empresa" value="<?php if(isset($nombre_empresa)) {print $nombre_empresa;}?>">
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback">
                                <label>Raz&oacute;n Social:</label>
                                <input type="text" class="form-control" id="razon_social" placeholder="Raz&oacute;n Social" name="razon_social" value="<?php if(isset($razon_social)) {print $razon_social;}?>">
                        </div>
                     </div>
                     <div class="form-group"></div>                       
                     <div class="ln_solid"></div>

                      <div class="col-md-4 col-sm-12 col-xs-12 form-group has-feedback">
                        <label>Posici&oacute;n ante el IVA:</label>                     
                            <select class="form-control" id="optional" name="pos_iva" required="required">
		                <option value="">Seleccione...</option>
<?php 
			while($rowci = $miAdministracion->fetch_array($resultci))
				{
?>
				    <option value="<?php print $rowci["id"];?>"<?php if (isset($id_pos_iva) && $rowci["id"]==$id_pos_iva) {?> selected<?php }?>><?php print $rowci["iva"];?></option>
<?php 
				} // fin del while.
?>
	       			</select>
                                             
                      </div>
                      <div class="col-md-4 col-sm-12 col-xs-12 form-group has-feedback">
                        <label>Tipo de Documento:</label>                     
                            <select class="form-control" id="optional" name="id_doc_afip" required="required">
		                <option value="">Seleccione...</option>
<?php 
			while($rowtdoc = $miAdministracion->fetch_array($resulttdoc))
				{
?>
				    <option value="<?php print $rowtdoc["id"];?>"<?php if (isset($id_doc_afip) && $rowtdoc["id"]==$id_doc_afip) {?> selected<?php }?>><?php print $rowtdoc["tipo_doc"];?></option>
<?php 
				} // fin del while.
?>
	       			</select>                                          
                      </div>
   
                         <div class="col-md-4 col-sm-12 col-xs-12 form-group has-feedback">                         
                                <label class="control-label">N&uacute;mero:</label>
                                <input type="text" class="form-control" placeholder="N&uacute;mero" name="num_doc_afip" value="<?php if(isset($num_doc_afip)) {print $num_doc_afip;}?>">                                                               
                        </div>

                    <div class="form-group"></div>                     
                    <div class="ln_solid"></div>                     

<!-- TABLA DOMICILIOS -->
              <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                <div class="x_panel">
                  <div class="x_title">
                       <label>Domicilio</label>                      
                    <!-- <h2>Domicilio</h2> -->
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                      
                      <li class="dropdown">    
                          <a title="Nuevo Domicilio" id="agrega_dom"><i class="fa fa-plus"></i></a>
                      </li>
                      
<!--
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Setings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
-->
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">  
                    <table class="table">
                      <thead>
                        <tr>
                          <th>Tipo Domicilio</th>
                          <th>Calle</th>
                          <th>N&uacute;mero</th>
                          <th>Piso</th>
                          <th>Oficina/Local</th>
                          <th>C&oacute;digo Postal</th>
                          <th>Pa&iacute;s</th>
                          <th>Provincia</th> 
                          <th>Ciudad</th>                          
                        </tr>
                      </thead>
                      <tbody id="domicilio">
<?php
$w=0; //Es una variable creada para el foreach

if (isset($resultreg)){ // se ejecuta si hay domicilios cargados.
    while($rowreg = $miCliente->fetch_array($resultreg))         
        {      
?>
  <tr>
    <td>
        <input type="hidden" name="dom_id[<?php $w?>]" value="<?php print $rowreg["id"];?>"> 
        <select name="dom_id_tipo_dom[<?php $w?>]" class="form-control" style="font-size:12px;padding:0px;margin-right:10px;height:20px;" required="required">
            <option value="">Seleccione...</option>
            <?php for ($b=0;$b<$a;$b++) { ?>
                <option value="<?php print $ttdid[$b]; ?>" <?php if(isset($rowreg["id_tipo_dom"]) && $rowreg["id_tipo_dom"]==$ttdid[$b]) {?> selected <?php } ?>><?php print $ttddomicilio[$b]; ?></option>                                        
            <?php } // Fin for ?>                        
        </select>
    </td>
    
    <td><input type="text" class="form-control" style="font-size:12px;padding:0px;margin-right:10px;height:20px;" name="dom_calle[<?php $w?>]" value="<?php print $rowreg["calle"];?>"></td>
    <td><input type="text" class="form-control" style="font-size:12px;padding:0px;margin-right:10px;height:20px;" name="dom_numero[<?php $w?>]" value="<?php print $rowreg["numero"];?>"></td>
    <td><input type="text" class="form-control" style="font-size:12px;padding:0px;margin-right:10px;height:20px;" name="dom_piso[<?php $w?>]" value="<?php print $rowreg["piso"];?>"></td>
    <td><input type="text" class="form-control" style="font-size:12px;padding:0px;margin-right:10px;height:20px;" name="dom_of_local[<?php $w?>]" value="<?php print $rowreg["of_local"];?>"></td>
    <td><input type="text" class="form-control" style="font-size:12px;padding:0px;margin-right:10px;height:20px;" name="dom_cp[<?php $w?>]" value="<?php print $rowreg["cp"];?>"></td>
    <td>
        <select name="dom_id_pais[<?php $w?>]" id="pais-list" class="form-control demoInputBox pais" style="font-size:12px;padding:0px;margin-right:10px;height:20px;" data-provincia_id="<?php print $w; ?>">
            <!-- <option value="">Seleccione Pa&iacute;s</option> -->
            <?php for ($d=0;$d<$c;$d++) { ?>
                <option value="<?php print $paisid[$d]; ?>" <?php if(isset($rowreg["id_pais"]) && $rowreg["id_pais"]==$paisid[$d]) {?> selected <?php $id_pais=$paisid[$d]; } ?>><?php print $paisnombre[$d]; ?></option> 
            <?php } // Fin for ?>                        
	</select>
    </td>
    
    <td>
        <select name="dom_id_provincia[<?php $w?>]" id="provincia-list<?php print $w; ?>" class="form-control demoInputBox provincia" style="font-size:12px;padding:0px;margin-right:10px;height:20px;" data-ciudad_id="<?php print $w; ?>">    
            <?php for ($f=0;$f<$e;$f++) { 
                if ($provinciaidpais[$f]==$id_pais){?>
                <option value="<?php print $provinciaid[$f]; ?>" <?php if(isset($rowreg["id_prov"]) && $rowreg["id_prov"]==$provinciaid[$f]) {?> selected <?php $id_provincia=$provinciaid[$f]; } ?>><?php print $provincianombre[$f]; ?></option> 
                 <?php 
                    } // Fin if
                } // Fin for ?>  
        </select>
    </td>
    <td>
        <select name="dom_id_ciudad[<?php $w?>]" id="ciudad-list<?php print $w; ?>" class="form-control demoInputBox" style="font-size:12px;padding:0px;margin-right:10px;height:20px;">    
            <?php for ($h=0;$h<$g;$h++) { 
                if ($ciudadidprovincia[$h]==$id_provincia){?>
                <option value="<?php print $ciudadid[$h]; ?>" <?php if(isset($rowreg["id_ciudad"]) && $rowreg["id_ciudad"]==$ciudadid[$h]) {?> selected <?php } ?>><?php print $ciudadnombre[$h]; ?></option> 
            <?php 
                    } // Fin if
                } // Fin for ?> 
	</select>
    </td>        
        
</tr>
<?php
    $w++;
        } // fin while
    } // Fin if resultreg - si hay domicilios cargados
?>
                      <input type="hidden" id="vueltadom" name="vueltadom" value="<?php print $w;?>">
                      </tbody>                       
                    </table>                      
                  </div>
                </div>
              </div>
 <!-- /TABLA DOMICILIOS --> 

 <!-- TABLA MEDIOS -->
               <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                <div class="x_panel">
                  <div class="x_title">
                       <label>Medio de Comunicaci&oacute;n</label>                      
                    <!-- <h2>Medio de Comunicaci&oacute;n</h2> -->
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">    
                          <a title="Nuevo Medio" id="agrega_medio"><i class="fa fa-plus"></i></a></li>
<!--
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Setings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
-->
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <table class="table">
                      <thead>
                        <tr>
                          <th>Tipo Medio</th>
                          <th>Detalle</th>                   
                        </tr>
                      </thead>
                     
                      <tbody id="medio">
<?php
$m=0; //Es una variable creada para el foreach

if (isset($resultmed)){ // se ejecuta si hay domicilios cargados.
    while($rowmed = $miCliente->fetch_array($resultmed)) {
?>
  <tr>
    <td>
        <input type="hidden" name="med_id[<?php $w?>]" value="<?php print $rowmed["id"];?>"> 
        <select name="med_id_tipo_medio[<?php $m?>]" class="form-control" required="required">
            <option value="">Seleccione...</option>
            <?php for ($j=0;$j<$i;$j++) { ?>
                <option value="<?php print $medioid[$j]; ?>" <?php if(isset($rowmed["id_tipo_medio"]) && $rowmed["id_tipo_medio"]==$medioid[$j]) {?> selected <?php } ?>><?php print $medionombre[$j]; ?></option>                                        
            <?php } // Fin for ?>                        
        </select>
    </td>
    <td><input type="text" class="form-control" name="med_datos_medio[<?php $m?>]" value="<?php print $rowmed["datos_medio"];?>"></td>
</tr>
<?php
    $m++;
        } // fin while
    } // Fin if resultreg - si hay domicilios cargados
?>
                      </tbody>                     
                    </table>

                  </div>
                </div>
              </div>

 <!-- /TABLA MEDIOS -->
 
                    <div class="form-group"></div>                     
                    <div class="ln_solid"></div> 

 <!-- TABLA CONTACTOS JURIDICOS-->
<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback" id="contacto_juridico">
                <div class="x_panel">
                  <div class="x_title">
                       <label>Contactos</label>                      
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">    
                          <a title="Nuevo Conctacto" id="agrega_contacto"><i class="fa fa-plus"></i></a></li>
<!--
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Setings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
-->
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <table class="table">
                      <thead>
                        <tr>
                          <th>Nombre Contacto</th>
                          <th>Puesto</th>
<?php if ($form==2 && isset($resultctcempresa)) { ?> <th>&nbsp;</th> <?php } ?>                        
                        </tr>
                      </thead>
 
<?php 
if ($form==2 && isset($resultctcempresa))
    {
        while($rowctcempresa = $miCliente->fetch_array($resultctcempresa)) { ?>
                      <tbody>
                        <tr>
                            <td><?php echo $rowctcempresa["nombre"];?></td>
                            <td><?php echo $rowctcempresa["puesto"];?></td>
                            <td><span data-toggle="tooltip" data-placement="top" title="Eliminar">
                                        <a href="guia_internos.php?fun=5&id=<?php print $id;?>&id_ctcent=<?php echo $rowctcempresa["id"];?>" class="btn btn-danger btn-xs" class="btn btn-danger btn-xs" data-toggle="confirmation" data-btn-ok-label="Continuar" data-btn-ok-icon="glyphicon glyphicon-share-alt"
        data-btn-ok-class="btn-success" data-btn-cancel-label="Cancelar" data-btn-cancel-icon="glyphicon glyphicon-ban-circle" data-btn-cancel-class="btn-danger" 
        data-title="¿Est&aacute; Seguro?" data-content=""><i class="fa fa-trash-o"></i></a>
                                </span>                            
                            </td>                                       
                        </tr>    
                      </tbody>                        
        <?php       } // Fin While
        }  // fin if form 2 
                      
// if ($form==1)
//    { 
?>
                      <tbody id="contacto">
                      </tbody>
<?php
//    } // fin if form 1
?>    
                    </table>

                  </div>
                </div>
              </div> 
 <!-- /TABLA CONTACTOS JURIDICOS-->
 
 <!-- TABLA CONTACTOS FISICOS-->
                    <div id="contacto_fisico" >
                        <div class="col-md-4 col-sm-12 col-xs-12 form-group has-feedback">                         
                                <label>Empresa:</label> 
                                <select class="form-control" name="id_juridico" required="required">
                                    <option value="0">Seleccione</option>                                    
<?php 
                                    while($rowcnt = $miCliente->fetch_array($resultcnt))        
                                        {
?>
				    <option value="<?php print $rowcnt["id"];?>"<?php if (isset($id_juridico) && $rowcnt["id"]==$id_juridico) {?> selected<?php }?>><?php print $rowcnt["nombre"];?></option>
<?php                                   }?>
				</select>
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12 form-group has-feedback">                         
                            <label>Puesto:</label>
                            <input type="text" class="form-control" placeholder="puesto" name="puesto_juridico" value="<?php if(isset($puesto_juridico)) {print $puesto_juridico;}?>">
                        </div>
                    </div>
 <!-- /TABLA CONTACTOS FISICOS-->
 
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                                <label>&nbsp;</label>                        
                        &nbsp;
                    </div>

                    <div class="col-md-3 col-sm-12 col-xs-12 has-feedback">
                        <label>Fecha de Alta</label>                       
                        <div class="form-group">
                            <div class='input-group date' id='myDatepicker2'>
                                <input type='text' class="form-control" name="fecha_alta" value="<?php if(isset($fechavis)){print $fechavis;} else {print date('d/m/Y');}?>" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                     </div>
                   
                    <div class="col-md-3 col-sm-12 col-xs-12 form-group has-feedback">                         
                                <label>Operador:</label>
                                <select class="form-control" name="id_operador" required="required">
<?php 
                                    while($rowu = $miValidacion->fetch_array($uresult))        
                                        {
                                        if ($form==1) {
                                        ?>
				    <option value="<?php print $rowu["Id"];?>"<?php if ($rowu["codigo"]==$_SESSION["Cod"]) {?> selected<?php }?>><?php print $rowu["nombres"]." ".$rowu["apellidos"];?></option>
<?php
                                        } // fin If form 1
                                        if ($form==2) {                                        
?>
                                    <option value="<?php print $rowu["Id"];?>"<?php if ($rowu["Id"]==$id_operador) {?> selected<?php }?>><?php print $rowu["nombres"]." ".$rowu["apellidos"];?></option>                                    
<?php                                   
                                        } // Fin if form 2
                                    } // Fin While
?>
				</select>
                    </div>                    
                    <div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback">                         
                        <label>Observaciones:</label>
                        <input type="text" class="form-control" placeholder="Observaciones" name="observaciones" value="<?php if (isset($observaciones)){print $observaciones;}?>">

                    </div>                       
                    <!-- Campos del Formulario --> 
  
 
                     <div class="form-group"></div>                     
                      <div class="ln_solid"></div>
                      <div class="form-group">
 
                    
                    <div class="col-md-4 col-sm-12 col-xs-12 form-group has-feedback"></div>
                    <div class="col-md-4 col-sm-12 col-xs-12 form-group has-feedback"> 
                          <input type="hidden" name="reincide" value="1">                         
                          <input type="hidden" name="formulario" value="<?php print $form;?>">
                          <input type="hidden" name="id" value="<?php if(isset($id)){print $id;}?>">                       
                          <button type="reset" class="btn btn-primary">Reiniciar</button>                                                  
                          <button id="send" type="submit" class="btn btn-success" name="formulario">Grabar</button>
                          <a href="guia_internos.php?fun=1" class="btn btn-primary"></i> Volver </a>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12 form-group has-feedback"></div>
                      </div>
                    </form>
                  </div>
                <!-- page content Form -->

                </div>
              </div>
            </div>    
<!-- /page content -->

<?php
//unset($_SESSION["contprov"]);
} ### FIN DE formulario ###


########################
### FUNCION dbalta() ###
########################

function dbalta() {
/*  
$countmedio=count($_POST['id_tipo_medio']);
$countcontacto=count($_POST['id_contacto']);
echo "Estoy en dbalta";
if (isset($_POST['id_tipo_medio'])) { echo "<br>Tipo Medio: SI - ".$countmedio;} else {echo "<br>Tipo Medio: NO";}
if (isset($_POST['id_contacto'])) { echo "<br>Id Contacto: SI - ".$countcontacto;} else {echo "<br>Id Contacto: NO";} 
if ($_POST['clasecli'] == 2) {echo "<br>Clasecli = 2";} else {echo "<br>Clasecli distinto de 2";}
if (isset($_POST['id_juridico'])) { echo "<br>Id Contacto: SI - ".$_POST['id_juridico'];} else {echo "<br>Id Contacto: NO";}
*/
/* 
// Configuro la variable Razon social si esta es nula y aplica. y defino contacto.
//if (isset($POST['clasecli']) && $_POST['clasecli'] == 1){$contacto=1;}    
    
if (isset($POST['clasecli']) && $_POST['clasecli'] == 2) 
    { 
        //$contacto="0";
        if (isset ($_POST['razon_social']) && !empty($_POST['razon_social']))
            {
                $_POST['razon_social']=$_POST['nombre_empresa'];
            }
    }
    
$_POST['nombre']=$_POST['nombre_empresa'].$_POST['nombre_particular']." ".$_POST['apellido_particular'];

## Configuro la variable para el campo cliente
if (isset($_POST["ccliente"]) && $_POST["ccliente"]=="on"){$cliente=1;}else{$cliente=0;}

## Configuro la variable para el campo proveedor
if (isset($_POST["cproveedor"]) && $_POST["cproveedor"]=="on"){$proveedor=1;}else{$proveedor=0;}  
 
## Configuro la variable para el campo contacto
//if (isset($_POST["ccliente"]) && $_POST["ccliente"]=="on"){$contacto=1;}else{$contacto=0;}

## Convertir fecha
$date = str_replace('/', '-', $_POST['fecha_alta']);
$fechaalta = date('Y-m-d', strtotime($date));
    
// Query principal

require_once(dirname(__FILE__) . "/../../clases/cliente.class.inc");
$miCliente = new Cliente();	

## INGRESO DATOS.
$miCliente->agregaEntidadPri($_POST['tipo_cliente'],$_POST['clasecli'],$_POST['nombre_empresa'],$_POST['razon_social'],$_POST['apellido_particular'],$_POST['nombre_particular'],$_POST['nombre'],$cliente,$proveedor,$_POST['pos_iva'],$_POST['id_doc_afip'],$_POST['num_doc_afip'],$_POST['observaciones'],$fechaalta,1,$fechaalta,$_POST['id_operador']);

## Configuro la variable para obtener el Id que se le acaba de asisgnar al registro reci�n ingresado.
$entidadid= $miCliente->insert_id();

$my_error1 = $miCliente->error();
     
## Comienzo INSERT para tabla de Entidad_dom.                
//$miCliente->agregaEntidadDomicilio($entidadid,$_POST['id_tipo_dom'],$_POST['calle'],$_POST['numero'],$_POST['piso'],$_POST['of_local'],$_POST['cp'],$_POST['id_ciudad']);
//$my_error3 = $miCliente->error();

# Ingreso datos de Domicilio
if (isset($_POST['calle'])) {

    ## Comienzo INSERT para tabla de Entidad_dom. 
    for($e=0; $e<count($_POST['calle']); $e++){
        $s_id_tipo_dom = $_POST['id_tipo_dom'][$e];
        $s_calle = $_POST['calle'][$e];
        $s_numero = $_POST['numero'][$e];
        $s_piso = $_POST['piso'][$e];
        $s_of_local = $_POST['of_local'][$e];
        $s_cp = $_POST['cp'][$e];
        $s_id_ciudad = $_POST['id_ciudad'][$e];        
        
        $miCliente->agregaEntidadDomicilio($entidadid,$s_id_tipo_dom,$s_calle,$s_numero,$s_piso,$s_of_local,$s_cp,$s_id_ciudad);

        $my_error3 = $miCliente->error();        
    } // Fin for
} // Fin if

# Ingreso datos de Medio de Comunicación.
if (isset($_POST['id_tipo_medio'])) {
    $idmediocontacto=0;
    ## Comienzo INSERT para tabla de Entidad_medio. 
    for($i=0; $i<count($_POST['id_tipo_medio']); $i++){
        $tipomedioid = $_POST['id_tipo_medio'][$i];
        $datosmedio = $_POST['datos_medio'][$i];

        $miCliente->agregaEntidadMedio($entidadid,$tipomedioid,$datosmedio,$idmediocontacto);
        $my_error4 = $miCliente->error();        
    } // Fin for
} // Fin if

# Ingreso datos de Contacto.
if (isset($_POST['id_contacto']) && isset($_POST['clasecli']) && $_POST['clasecli'] == 2) {
    ## Comienzo INSERT para tabla de Entidad_contacto.   
            for($j=0; $j<count($_POST['id_contacto']); $j++){
                $id_contacto = $_POST['id_contacto'][$j];                        
                $puesto = $_POST['puesto'][$j];
$valcontact="<br>Valor de j: ".$j." - id contacto: ".$id_contacto." - Puesto: ".$puesto;                 
//echo "<br>Valor de j: ".$j." - id contacto: ".$id_contacto." - Puesto: ".$puesto;        
                # Insert para Juridica
                $miCliente->agregaEntidadContacto($entidadid,$id_contacto,$puesto);
$my_error5 = $miCliente->error();                
            } // Fin for
} // Fin if

if (isset($_POST['id_juridico']) && $_POST['id_juridico']>0) {
    # Insert contacto para Fisica
    $miCliente->agregaEntidadContacto($_POST['id_juridico'],$entidadid,$_POST['puesto_juridico']);
    $my_error6 = $miCliente->error();       
} // Fin if

if(!empty($my_error1) || !empty($my_error3) || !empty($my_error4) || !empty($my_error5) || !empty($my_error6)) 
    {
        if(!empty($my_error1)){$my_error1="my_error1: ".$my_error1;}
        if(!empty($my_error3)){$my_error3="my_error3: ".$my_error3;}        
        if(!empty($my_error4)){$my_error4="my_error4: ".$my_error4;}
        if(!empty($my_error5)){$my_error5="my_error5: ".$my_error5;}
        if(!empty($my_error6)){$my_error6="my_error6: ".$my_error6;}

        $_POST['reincide']=2;
        $_POST['falta']=1;
        $_POST['merror']="Ha habido un error al insertar los valores: <small>".$my_error1.$my_error3.$my_error4.$my_error5.$my_error6."</small>";                                        
        main();
    } else {
        $_POST['id']=$entidadid;
        $_POST['reincide']=2;                                        
        $_POST['ver']=1;
        $_POST['mconfirma']="El Registro ha sido creado satisfactoriamente.";
        main();
    }

} ### Fin de dbalta ###


#########################
### FUNCION dbmodif() ###
#########################

function dbmodif() {

// Configuro la variable Razon social si esta es nula y aplica.
if (isset($POST['clasecli']) && $_POST['clasecli'] == 2) 
    { 
        if (isset ($_POST['razon_social']) && !empty($_POST['razon_social']))
            {
                $_POST['razon_social']=$_POST['nombre_empresa'];
            }
    }

$_POST['nombre']=$_POST['nombre_empresa'].$_POST['nombre_particular']." ".$_POST['apellido_particular'];

## Configuro la variable para el campo cliente
if (isset($_POST["ccliente"]) && $_POST["ccliente"]=="on"){$cliente=1;}else{$cliente=0;}

## Configuro la variable para el campo proveedor
if (isset($_POST["cproveedor"]) && $_POST["cproveedor"]=="on"){$proveedor=1;}else{$proveedor=0;}  
 
## Configuro la variable para el campo proveedor
//if (isset($_POST["ccontacto"]) && $_POST["ccontacto"]=="on"){$contacto=1;}else{$contacto=0;}

## Convertir fecha
$date = str_replace('/', '-', $_POST['fecha_alta']);
$fechaalta = date('Y-m-d', strtotime($date));

    require_once(dirname(__FILE__) . "/../../clases/cliente.class.inc");	
    $miCliente = new Cliente();

    ## ACTUALIZA DATOS.
    //$miCliente->actualizaCiud($_POST['id'],$_POST['nombre_entidad'],$_POST['provid']);
    $miCliente->actualizaEntidadPri($_POST['tipo_cliente'],$_POST['clasecli'],$_POST['nombre_empresa'],$_POST['razon_social'],$_POST['apellido_particular'],$_POST['nombre_particular'],$_POST['nombre'],$cliente,$proveedor,$_POST['pos_iva'],$_POST['id_doc_afip'],$_POST['num_doc_afip'],$_POST['observaciones'],$fechaalta,1,$fechaalta,$_POST['id_operador'],$_POST['id']);

    // Ahora comprobaremos que todo ha ido correctamente
    $my_error1 = $miCliente->error();

# Actualizo datos de Domicilio
if (isset($_POST['dom_calle'])) {

    ## Comienzo INSERT para tabla de Entidad_dom. 
    for($e=0; $e<count($_POST['dom_calle']); $e++){
        $d_id_tipo_dom = $_POST['dom_id_tipo_dom'][$e];
        $d_calle = $_POST['dom_calle'][$e];
        $d_numero = $_POST['dom_numero'][$e];
        $d_piso = $_POST['dom_piso'][$e];
        $d_of_local = $_POST['dom_of_local'][$e];
        $d_cp = $_POST['dom_cp'][$e];
        $d_id_ciudad = $_POST['dom_id_ciudad'][$e];
        $d_id = $_POST['dom_id'][$e];        
        
        $miCliente->actualizaEntidadDomicilio($d_id_tipo_dom,$d_calle,$d_numero,$d_piso,$d_of_local,$d_cp,$d_id_ciudad,$d_id);
        $my_error2 = $miCliente->error();        
    } // Fin for
} // Fin if 

# Ingreso datos de Domicilio
if (isset($_POST['calle'])) {

    ## Comienzo INSERT para tabla de Entidad_dom. 
    for($e=0; $e<count($_POST['calle']); $e++){
        $s_id_tipo_dom = $_POST['id_tipo_dom'][$e];
        $s_calle = $_POST['calle'][$e];
        $s_numero = $_POST['numero'][$e];
        $s_piso = $_POST['piso'][$e];
        $s_of_local = $_POST['of_local'][$e];
        $s_cp = $_POST['cp'][$e];
        $s_id_ciudad = $_POST['id_ciudad'][$e];        
        
        $miCliente->agregaEntidadDomicilio($_POST['id'],$s_id_tipo_dom,$s_calle,$s_numero,$s_piso,$s_of_local,$s_cp,$s_id_ciudad);
        $my_error3 = $miCliente->error();        
    } // Fin for
} // Fin if    

# Actualizo datos de Medio de Comunicación.
if (isset($_POST['med_id_tipo_medio'])) {
    $midmediocontacto=0;
    ## Comienzo INSERT para tabla de Entidad_medio. 
    for($i=0; $i<count($_POST['med_id_tipo_medio']); $i++){
        $m_tipomedioid = $_POST['med_id_tipo_medio'][$i];
        $m_datosmedio = $_POST['med_datos_medio'][$i];
        $m_id = $_POST['med_id'][$i]; 

        $miCliente->actualizaEntidadMedio($_POST['id'],$m_tipomedioid,$m_datosmedio,$midmediocontacto,$m_id);
        $my_error4 = $miCliente->error();        
    } // Fin for
} // Fin if

# Ingreso datos de Medio de Comunicación.
if (isset($_POST['id_tipo_medio'])) {
    $idmediocontacto=0;
    ## Comienzo INSERT para tabla de Entidad_medio. 
    for($i=0; $i<count($_POST['id_tipo_medio']); $i++){
        $tipomedioid = $_POST['id_tipo_medio'][$i];
        $datosmedio = $_POST['datos_medio'][$i];

        $miCliente->agregaEntidadMedio($_POST['id'],$tipomedioid,$datosmedio,$idmediocontacto);
        $my_error5 = $miCliente->error();        
    } // Fin for
} // Fin if

# Ingreso datos de Contacto
if (isset($_POST['id_contacto']) && isset($_POST['clasecli']) && $_POST['clasecli'] == 2) {
    ## Comienzo INSERT para tabla de Entidad_contacto.   
            for($j=0; $j<count($_POST['id_contacto']); $j++){
                $id_contacto = $_POST['id_contacto'][$j];                        
                $puesto = $_POST['puesto'][$j];      
                # Insert para Juridica
                $miCliente->agregaEntidadContacto($_POST['id'],$id_contacto,$puesto);
                $my_error6 = $miCliente->error();                
            } // Fin for
} // Fin if    
    
if (isset($_POST['id_juridico']) && $_POST['id_juridico']>0) {
    # Insert contacto para Fisica
    $miCliente->actualizaEmpresaContacto($_POST['id_juridico'],$_POST['id'],$_POST['puesto_juridico']);
    $my_error7 = $miCliente->error();       
} // Fin if    
    
    if(!empty($my_error1) || !empty($my_error2) || !empty($my_error3) || !empty($my_error4) || !empty($my_error5) || !empty($my_error6) || !empty($my_error7)) 
        {
        if(!empty($my_error1)){$my_error1="my_error1: ".$my_error1;}
        if(!empty($my_error2)){$my_error2="my_error2: ".$my_error2;}        
        if(!empty($my_error3)){$my_error3="my_error3: ".$my_error3;}
        if(!empty($my_error4)){$my_error4="my_error4: ".$my_error4;}        
        if(!empty($my_error5)){$my_error5="my_error5: ".$my_error5;}
        if(!empty($my_error6)){$my_error6="my_error6: ".$my_error6;}
        if(!empty($my_error7)){$my_error7="my_error7: ".$my_error7;}        
            //$htitulo="Edici&oacute;n de Entidad";
            $_POST['reincide']=2;
            $_POST['falta']=1;
            $_POST['merror']="Ha habido un error al editar los valores: <small>".$my_error1.$my_error2.$my_error3.$my_error4.$my_error5.$my_error6.$my_error7."</small>";                                        
            main();
	} else {                                       
            $_POST['id']=$_POST['id'];
            $_POST['reincide']=2;                                        
            $_POST['ver']=2;
            $_POST['mconfirma']="El Registro ha sido editado satisfactoriamente.";
            main();
    	}  
} ### Fin de dbmodif ###


#########################
### FUNCION elimina() ###
#########################

function elimina_contacto_entidad() {

$htitulo="Baja de Entidad";

require_once(dirname(__FILE__) . "/../../clases/cliente.class.inc");
$miCliente = new Cliente();
$miCliente->eliminaContactoEntidad($_GET['id_ctcent']);

// Ahora comprobaremos que todo ha ido correctamente
$my_error = $miCliente->error();

if(!empty($my_error)) 
    {
        $_POST['merror']="Ha habido un error al eliminar el contacto: <small>".$my_error."</small>";
        $_POST['id']=$_GET['id'];
        $_POST['reincide']=2;                                        
        $_POST['modif']=1;
        formulario(2);
    } else {
        $_POST['mconfirma']="El Conctacto ha sido eliminado satisfactoriamente.";
        $_POST['id']=$_GET['id'];
        $_POST['reincide']=2;                                        
        $_POST['modif']=1;
        formulario(2);
    } 
} ### FINAL DE elimina ###


###########################
### FUNCION desactiva() ###
###########################

function desactiva() {

$htitulo="Desactiva Entidad";

require_once(dirname(__FILE__) . "/../../clases/cliente.class.inc");
$miCliente = new Cliente();
$miCliente->desactivaEntidad($_GET['id']);

// Ahora comprobaremos que todo ha ido correctamente
$my_error = $miCliente->error();

if(!empty($my_error)) 
    {
        $_POST['merror']="Ha habido un error al actualizar el registro: <small>".$my_error."</small>";                                        
        listado();
    } else {
        $_POST['mconfirma']="El Registro ha sido actualizado satisfactoriamente.";
        listado();
    } 
} ### Final de Desactiva ###

##########################
### FUNCION reactiva() ###
##########################

function reactiva() {

$htitulo="Reactiva Entidad";

## Configuro la variable para el campo fecha
$fechoy=date('Y')."-".date('m')."-".date('d');

require_once(dirname(__FILE__) . "/../../clases/cliente.class.inc");
$miCliente = new Cliente();
$miCliente->reactivaEntidad($fechoy,$_GET['id']);

// Ahora comprobaremos que todo ha ido correctamente
$my_error = $miCliente->error();

if(!empty($my_error)) 
    {
        $_POST['merror']="Ha habido un error al actualizar el registro: <small>".$my_error."</small>";                                        
        listado();
    } else {
        $_POST['mconfirma']="El Registro ha sido actualizado satisfactoriamente.";
        listado();
    } 
} ### Final de Desactiva ###
 * 
 */