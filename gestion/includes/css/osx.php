
<?php 
header('content-type:text/css'); 
include ("css_conf.php");
echo <<<FINCSS
/*
 * SimpleModal OSX Style Modal Dialog
 * http://www.ericmmartin.com/projects/simplemodal/
 * http://code.google.com/p/simplemodal/
 *
 * Copyright (c) 2009 Eric Martin - http://ericmmartin.com
 *
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 *
 * Revision: $Id: osx.css 214 2009-09-17 04:53:03Z emartin24 $
 *
 */

body {height:100%; margin:0;}


/* BChoc */
#BChoc-content, #BChoc-data {display:none;}

/* Overlay */
#BChoc-overlay {background-color:#000; cursor:wait;}

/* Container */
#BChoc-container {background-color:#FEF0CB; color:#000; font-family:"Lucida Grande",Arial,sans-serif; font-size:.9em; padding-bottom:4px; width:600px; -moz-border-radius-bottomleft:6px; -webkit-border-bottom-left-radius:6px; -moz-border-radius-bottomright:6px; -webkit-border-bottom-right-radius:6px; -moz-box-shadow:0 0 64px #000; -webkit-box-shadow:0 0 64px #000;}
#BChoc-container a {color:#ddd;}
#BChoc-container #BChoc-title {color:#804000; background-color:#FFDF80; border-bottom:1px solid #804000; font-weight:bold; padding:6px 8px; text-shadow:0 1px 0 #FEF0CB;}
#BChoc-container .close {display:none; float:right;}
#BChoc-container .close a {display:block; color:#777; font-size:.8em; font-weight:bold; padding:6px 12px 0; text-decoration:none; text-shadow:0 1px 0 #f4f4f4;}
#BChoc-container .close a:hover {color:#000;}
#BChoc-container #BChoc-data {padding:6px 12px;}
#BChoc-container h2 {margin:10px 0 6px;}
#BChoc-container p {
			margin-bottom:10px; 
			text-align:justify;
			font-size: 14px;
			font-family: Georgia; 
			font-style: italic; 
			font-weight: bold;
			color:#804000;
}
#BChoc-container span {color:#777; font-size:.9em;}

/*****************************************************************************************************/

/* Cheese */
#Cheese-content, #Cheese-data {display:none;}

/* Overlay */
#Cheese-overlay {background-color:#000; cursor:wait;}

/* Container */
#Cheese-container {background-color:#FEF0CB; color:#000; font-family:"Lucida Grande",Arial,sans-serif; font-size:.9em; padding-bottom:4px; width:600px; -moz-border-radius-bottomleft:6px; -webkit-border-bottom-left-radius:6px; -moz-border-radius-bottomright:6px; -webkit-border-bottom-right-radius:6px; -moz-box-shadow:0 0 64px #000; -webkit-box-shadow:0 0 64px #000;}
#Cheese-container a {color:#ddd;}
#Cheese-container #Cheese-title {color:#804000; background-color:#FFDF80; border-bottom:1px solid #804000; font-weight:bold; padding:6px 8px; text-shadow:0 1px 0 #FEF0CB;}
#Cheese-container .close {display:none; float:right;}
#Cheese-container .close a {display:block; color:#777; font-size:.8em; font-weight:bold; padding:6px 12px 0; text-decoration:none; text-shadow:0 1px 0 #f4f4f4;}
#Cheese-container .close a:hover {color:#000;}
#Cheese-container #Cheese-data {padding:6px 12px;}
#Cheese-container h2 {margin:10px 0 6px;}
#Cheese-container p {
			margin-bottom:10px; 
			text-align:justify;
			font-size: 14px;
			font-family: Georgia; 
			font-style: italic; 
			font-weight: bold;
			color:#804000;
}
#Cheese-container span {color:#777; font-size:.9em;}

/*****************************************************************************************************/

/* LemonPie */
#LemonPie-content, #LemonPie-data {display:none;}

/* Overlay */
#LemonPie-overlay {background-color:#000; cursor:wait;}

/* Container */
#LemonPie-container {background-color:#FEF0CB; color:#000; font-family:"Lucida Grande",Arial,sans-serif; font-size:.9em; padding-bottom:4px; width:600px; -moz-border-radius-bottomleft:6px; -webkit-border-bottom-left-radius:6px; -moz-border-radius-bottomright:6px; -webkit-border-bottom-right-radius:6px; -moz-box-shadow:0 0 64px #000; -webkit-box-shadow:0 0 64px #000;}
#LemonPie-container a {color:#ddd;}
#LemonPie-container #LemonPie-title {color:#804000; background-color:#FFDF80; border-bottom:1px solid #804000; font-weight:bold; padding:6px 8px; text-shadow:0 1px 0 #FEF0CB;}
#LemonPie-container .close {display:none; float:right;}
#LemonPie-container .close a {display:block; color:#777; font-size:.8em; font-weight:bold; padding:6px 12px 0; text-decoration:none; text-shadow:0 1px 0 #f4f4f4;}
#LemonPie-container .close a:hover {color:#000;}
#LemonPie-container #LemonPie-data {padding:6px 12px;}
#LemonPie-container h2 {margin:10px 0 6px;}
#LemonPie-container p {
			margin-bottom:10px; 
			text-align:justify;
			font-size: 14px;
			font-family: Georgia; 
			font-style: italic; 
			font-weight: bold;
			color:#804000;
}
#LemonPie-container span {color:#777; font-size:.9em;}
/*****************************************************************************************************/

/* MChocB */
#MChocB-content, #MChocB-data {display:none;}

/* Overlay */
#MChocB-overlay {background-color:#000; cursor:wait;}

/* Container */
#MChocB-container {background-color:#FEF0CB; color:#000; font-family:"Lucida Grande",Arial,sans-serif; font-size:.9em; padding-bottom:4px; width:600px; -moz-border-radius-bottomleft:6px; -webkit-border-bottom-left-radius:6px; -moz-border-radius-bottomright:6px; -webkit-border-bottom-right-radius:6px; -moz-box-shadow:0 0 64px #000; -webkit-box-shadow:0 0 64px #000;}
#MChocB-container a {color:#ddd;}
#MChocB-container #MChocB-title {color:#804000; background-color:#FFDF80; border-bottom:1px solid #804000; font-weight:bold; padding:6px 8px; text-shadow:0 1px 0 #FEF0CB;}
#MChocB-container .close {display:none; float:right;}
#MChocB-container .close a {display:block; color:#777; font-size:.8em; font-weight:bold; padding:6px 12px 0; text-decoration:none; text-shadow:0 1px 0 #f4f4f4;}
#MChocB-container .close a:hover {color:#000;}
#MChocB-container #MChocB-data {padding:6px 12px;}
#MChocB-container h2 {margin:10px 0 6px;}
#MChocB-container p {
			margin-bottom:10px; 
			text-align:justify;
			font-size: 14px;
			font-family: Georgia; 
			font-style: italic; 
			font-weight: bold;
			color:#804000;
}
#MChocB-container span {color:#777; font-size:.9em;}
/*****************************************************************************************************/

/* MChoc */
#MChoc-content, #MChoc-data {display:none;}

/* Overlay */
#MChoc-overlay {background-color:#000; cursor:wait;}

/* Container */
#MChoc-container {background-color:#FEF0CB; color:#000; font-family:"Lucida Grande",Arial,sans-serif; font-size:.9em; padding-bottom:4px; width:600px; -moz-border-radius-bottomleft:6px; -webkit-border-bottom-left-radius:6px; -moz-border-radius-bottomright:6px; -webkit-border-bottom-right-radius:6px; -moz-box-shadow:0 0 64px #000; -webkit-box-shadow:0 0 64px #000;}
#MChoc-container a {color:#ddd;}
#MChoc-container #MChoc-title {color:#804000; background-color:#FFDF80; border-bottom:1px solid #804000; font-weight:bold; padding:6px 8px; text-shadow:0 1px 0 #FEF0CB;}
#MChoc-container .close {display:none; float:right;}
#MChoc-container .close a {display:block; color:#777; font-size:.8em; font-weight:bold; padding:6px 12px 0; text-decoration:none; text-shadow:0 1px 0 #f4f4f4;}
#MChoc-container .close a:hover {color:#000;}
#MChoc-container #MChoc-data {padding:6px 12px;}
#MChoc-container h2 {margin:10px 0 6px;}
#MChoc-container p {
			margin-bottom:10px; 
			text-align:justify;
			font-size: 14px;
			font-family: Georgia; 
			font-style: italic; 
			font-weight: bold;
			color:#804000;
}
#MChoc-container span {color:#777; font-size:.9em;}
/*****************************************************************************************************/

/* Rogel */
#Rogel-content, #Rogel-data {display:none;}

/* Overlay */
#Rogel-overlay {background-color:#000; cursor:wait;}

/* Container */
#Rogel-container {background-color:#FEF0CB; color:#000; font-family:"Lucida Grande",Arial,sans-serif; font-size:.9em; padding-bottom:4px; width:600px; -moz-border-radius-bottomleft:6px; -webkit-border-bottom-left-radius:6px; -moz-border-radius-bottomright:6px; -webkit-border-bottom-right-radius:6px; -moz-box-shadow:0 0 64px #000; -webkit-box-shadow:0 0 64px #000;}
#Rogel-container a {color:#ddd;}
#Rogel-container #Rogel-title {color:#804000; background-color:#FFDF80; border-bottom:1px solid #804000; font-weight:bold; padding:6px 8px; text-shadow:0 1px 0 #FEF0CB;}
#Rogel-container .close {display:none; float:right;}
#Rogel-container .close a {display:block; color:#777; font-size:.8em; font-weight:bold; padding:6px 12px 0; text-decoration:none; text-shadow:0 1px 0 #f4f4f4;}
#Rogel-container .close a:hover {color:#000;}
#Rogel-container #Rogel-data {padding:6px 12px;}
#Rogel-container h2 {margin:10px 0 6px;}
#Rogel-container p {
			margin-bottom:10px; 
			text-align:justify;
			font-size: 14px;
			font-family: Georgia; 
			font-style: italic; 
			font-weight: bold;
			color:#804000;
}
#Rogel-container span {color:#777; font-size:.9em;}
/*****************************************************************************************************/

/* TBanana */
#TBanana-content, #TBanana-data {display:none;}

/* Overlay */
#TBanana-overlay {background-color:#000; cursor:wait;}

/* Container */
#TBanana-container {background-color:#FEF0CB; color:#000; font-family:"Lucida Grande",Arial,sans-serif; font-size:.9em; padding-bottom:4px; width:600px; -moz-border-radius-bottomleft:6px; -webkit-border-bottom-left-radius:6px; -moz-border-radius-bottomright:6px; -webkit-border-bottom-right-radius:6px; -moz-box-shadow:0 0 64px #000; -webkit-box-shadow:0 0 64px #000;}
#TBanana-container a {color:#ddd;}
#TBanana-container #TBanana-title {color:#804000; background-color:#FFDF80; border-bottom:1px solid #804000; font-weight:bold; padding:6px 8px; text-shadow:0 1px 0 #FEF0CB;}
#TBanana-container .close {display:none; float:right;}
#TBanana-container .close a {display:block; color:#777; font-size:.8em; font-weight:bold; padding:6px 12px 0; text-decoration:none; text-shadow:0 1px 0 #f4f4f4;}
#TBanana-container .close a:hover {color:#000;}
#TBanana-container #TBanana-data {padding:6px 12px;}
#TBanana-container h2 {margin:10px 0 6px;}
#TBanana-container p {
			margin-bottom:10px; 
			text-align:justify;
			font-size: 14px;
			font-family: Georgia; 
			font-style: italic; 
			font-weight: bold;
			color:#804000;
}
#TBanana-container span {color:#777; font-size:.9em;}
/*****************************************************************************************************/

/* BrowDC */
#BrowDC-content, #BrowDC-data {display:none;}

/* Overlay */
#BrowDC-overlay {background-color:#000; cursor:wait;}

/* Container */
#BrowDC-container {background-color:#FEF0CB; color:#000; font-family:"Lucida Grande",Arial,sans-serif; font-size:.9em; padding-bottom:4px; width:600px; -moz-border-radius-bottomleft:6px; -webkit-border-bottom-left-radius:6px; -moz-border-radius-bottomright:6px; -webkit-border-bottom-right-radius:6px; -moz-box-shadow:0 0 64px #000; -webkit-box-shadow:0 0 64px #000;}
#BrowDC-container a {color:#ddd;}
#BrowDC-container #BrowDC-title {color:#804000; background-color:#FFDF80; border-bottom:1px solid #804000; font-weight:bold; padding:6px 8px; text-shadow:0 1px 0 #FEF0CB;}
#BrowDC-container .close {display:none; float:right;}
#BrowDC-container .close a {display:block; color:#777; font-size:.8em; font-weight:bold; padding:6px 12px 0; text-decoration:none; text-shadow:0 1px 0 #f4f4f4;}
#BrowDC-container .close a:hover {color:#000;}
#BrowDC-container #BrowDC-data {padding:6px 12px;}
#BrowDC-container h2 {margin:10px 0 6px;}
#BrowDC-container p {
			margin-bottom:10px; 
			text-align:justify;
			font-size: 14px;
			font-family: Georgia; 
			font-style: italic; 
			font-weight: bold;
			color:#804000;
}
#BrowDC-container span {color:#777; font-size:.9em;}
/*****************************************************************************************************/

/* BrowFR */
#BrowFR-content, #BrowFR-data {display:none;}

/* Overlay */
#BrowFR-overlay {background-color:#000; cursor:wait;}

/* Container */
#BrowFR-container {background-color:#FEF0CB; color:#000; font-family:"Lucida Grande",Arial,sans-serif; font-size:.9em; padding-bottom:4px; width:600px; -moz-border-radius-bottomleft:6px; -webkit-border-bottom-left-radius:6px; -moz-border-radius-bottomright:6px; -webkit-border-bottom-right-radius:6px; -moz-box-shadow:0 0 64px #000; -webkit-box-shadow:0 0 64px #000;}
#BrowFR-container a {color:#ddd;}
#BrowFR-container #BrowFR-title {color:#804000; background-color:#FFDF80; border-bottom:1px solid #804000; font-weight:bold; padding:6px 8px; text-shadow:0 1px 0 #FEF0CB;}
#BrowFR-container .close {display:none; float:right;}
#BrowFR-container .close a {display:block; color:#777; font-size:.8em; font-weight:bold; padding:6px 12px 0; text-decoration:none; text-shadow:0 1px 0 #f4f4f4;}
#BrowFR-container .close a:hover {color:#000;}
#BrowFR-container #BrowFR-data {padding:6px 12px;}
#BrowFR-container h2 {margin:10px 0 6px;}
#BrowFR-container p {
			margin-bottom:10px; 
			text-align:justify;
			font-size: 14px;
			font-family: Georgia; 
			font-style: italic; 
			font-weight: bold;
			color:#804000;
}
#BrowFR-container span {color:#777; font-size:.9em;}
/*****************************************************************************************************/

/* BLimon */
#BLimon-content, #BLimon-data {display:none;}

/* Overlay */
#BLimon-overlay {background-color:#000; cursor:wait;}

/* Container */
#BLimon-container {background-color:#FEF0CB; color:#000; font-family:"Lucida Grande",Arial,sans-serif; font-size:.9em; padding-bottom:4px; width:600px; -moz-border-radius-bottomleft:6px; -webkit-border-bottom-left-radius:6px; -moz-border-radius-bottomright:6px; -webkit-border-bottom-right-radius:6px; -moz-box-shadow:0 0 64px #000; -webkit-box-shadow:0 0 64px #000;}
#BLimon-container a {color:#ddd;}
#BLimon-container #BLimon-title {color:#804000; background-color:#FFDF80; border-bottom:1px solid #804000; font-weight:bold; padding:6px 8px; text-shadow:0 1px 0 #FEF0CB;}
#BLimon-container .close {display:none; float:right;}
#BLimon-container .close a {display:block; color:#777; font-size:.8em; font-weight:bold; padding:6px 12px 0; text-decoration:none; text-shadow:0 1px 0 #f4f4f4;}
#BLimon-container .close a:hover {color:#000;}
#BLimon-container #BLimon-data {padding:6px 12px;}
#BLimon-container h2 {margin:10px 0 6px;}
#BLimon-container p {
			margin-bottom:10px; 
			text-align:justify;
			font-size: 14px;
			font-family: Georgia; 
			font-style: italic; 
			font-weight: bold;
			color:#804000;
}
#BLimon-container span {color:#777; font-size:.9em;}
/*****************************************************************************************************/

/* CupCake */
#CupCake-content, #CupCake-data {display:none;}

/* Overlay */
#CupCake-overlay {background-color:#000; cursor:wait;}

/* Container */
#CupCake-container {background-color:#FEF0CB; color:#000; font-family:"Lucida Grande",Arial,sans-serif; font-size:.9em; padding-bottom:4px; width:600px; -moz-border-radius-bottomleft:6px; -webkit-border-bottom-left-radius:6px; -moz-border-radius-bottomright:6px; -webkit-border-bottom-right-radius:6px; -moz-box-shadow:0 0 64px #000; -webkit-box-shadow:0 0 64px #000;}
#CupCake-container a {color:#ddd;}
#CupCake-container #CupCake-title {color:#804000; background-color:#FFDF80; border-bottom:1px solid #804000; font-weight:bold; padding:6px 8px; text-shadow:0 1px 0 #FEF0CB;}
#CupCake-container .close {display:none; float:right;}
#CupCake-container .close a {display:block; color:#777; font-size:.8em; font-weight:bold; padding:6px 12px 0; text-decoration:none; text-shadow:0 1px 0 #f4f4f4;}
#CupCake-container .close a:hover {color:#000;}
#CupCake-container #CupCake-data {padding:6px 12px;}
#CupCake-container h2 {margin:10px 0 6px;}
#CupCake-container p {
			margin-bottom:10px; 
			text-align:justify;
			font-size: 14px;
			font-family: Georgia; 
			font-style: italic; 
			font-weight: bold;
			color:#804000;
}
#CupCake-container span {color:#777; font-size:.9em;}
/*****************************************************************************************************/

/* PastaF */
#PastaF-content, #PastaF-data {display:none;}

/* Overlay */
#PastaF-overlay {background-color:#000; cursor:wait;}

/* Container */
#PastaF-container {background-color:#FEF0CB; color:#000; font-family:"Lucida Grande",Arial,sans-serif; font-size:.9em; padding-bottom:4px; width:600px; -moz-border-radius-bottomleft:6px; -webkit-border-bottom-left-radius:6px; -moz-border-radius-bottomright:6px; -webkit-border-bottom-right-radius:6px; -moz-box-shadow:0 0 64px #000; -webkit-box-shadow:0 0 64px #000;}
#PastaF-container a {color:#ddd;}
#PastaF-container #PastaF-title {color:#804000; background-color:#FFDF80; border-bottom:1px solid #804000; font-weight:bold; padding:6px 8px; text-shadow:0 1px 0 #FEF0CB;}
#PastaF-container .close {display:none; float:right;}
#PastaF-container .close a {display:block; color:#777; font-size:.8em; font-weight:bold; padding:6px 12px 0; text-decoration:none; text-shadow:0 1px 0 #f4f4f4;}
#PastaF-container .close a:hover {color:#000;}
#PastaF-container #PastaF-data {padding:6px 12px;}
#PastaF-container h2 {margin:10px 0 6px;}
#PastaF-container p {
			margin-bottom:10px; 
			text-align:justify;
			font-size: 14px;
			font-family: Georgia; 
			font-style: italic; 
			font-weight: bold;
			color:#804000;
}
#PastaF-container span {color:#777; font-size:.9em;}
/*****************************************************************************************************/

/* TTMousses */
#TTMousses-content, #TTMousses-data {display:none;}

/* Overlay */
#TTMousses-overlay {background-color:#000; cursor:wait;}

/* Container */
#TTMousses-container {background-color:#FEF0CB; color:#000; font-family:"Lucida Grande",Arial,sans-serif; font-size:.9em; padding-bottom:4px; width:600px; -moz-border-radius-bottomleft:6px; -webkit-border-bottom-left-radius:6px; -moz-border-radius-bottomright:6px; -webkit-border-bottom-right-radius:6px; -moz-box-shadow:0 0 64px #000; -webkit-box-shadow:0 0 64px #000;}
#TTMousses-container a {color:#ddd;}
#TTMousses-container #TTMousses-title {color:#804000; background-color:#FFDF80; border-bottom:1px solid #804000; font-weight:bold; padding:6px 8px; text-shadow:0 1px 0 #FEF0CB;}
#TTMousses-container .close {display:none; float:right;}
#TTMousses-container .close a {display:block; color:#777; font-size:.8em; font-weight:bold; padding:6px 12px 0; text-decoration:none; text-shadow:0 1px 0 #f4f4f4;}
#TTMousses-container .close a:hover {color:#000;}
#TTMousses-container #TTMousses-data {padding:6px 12px;}
#TTMousses-container h2 {margin:10px 0 6px;}
#TTMousses-container p {
			margin-bottom:10px; 
			text-align:justify;
			font-size: 14px;
			font-family: Georgia; 
			font-style: italic; 
			font-weight: bold;
			color:#804000;
}
#TTMousses-container span {color:#777; font-size:.9em;}
/*****************************************************************************************************/
FINCSS;
?>
