<?php
header('content-type:text/css');
 
include ("css_conf.php");

echo <<<FINCSS

/*
#navigation-block {
	position:relative;
	top:100px;
	left:200px;

}
*/

#hide {
	position:absolute;
	top:30px;
	left:-190px;
}


ul#sliding-navigation
{
	#position:relative;
	#top:0px;
	#left:-40px;
	#z-index: 1;

	width: 195px;
	#width: 180px; 
	list-style: none;
	font-size: .75em;
	margin: 20px 0 0 0;
	#margin: 20px -40px 0 40px;	
	padding: 0 0 20px 0;
	#padding: 20px -20px 20px 0;
	border-bottom: 1px solid #B86829; 

}


ul#sliding-navigation li.sliding-element h3,
ul#sliding-navigation li.sliding-element a
{
	display: block;
	width: 150px;
	padding: 0px 18px;
	margin: 0;
	margin-bottom: 5px;
}


ul#sliding-navigation li.sliding-element h3
{
	height: 30px;
/*	background:#E0ECF8; */
	background: url(../../images/lmenubkH05.png);
	background-repeat: no-repeat;
/*	background-position: 75% 75%; 
		background-position: center middle; */
	background-position: 5px 0px;

	font-weight: normal;	
	font-size: 16px;
	font-family: Times New Roman; 
	font-style: italic; 
	font-weight: bold;
	color: #804000;
	padding-top: 3px;
}


ul#sliding-navigation li.sliding-element a
{
	color: #999;
/*	background:#222 url(../../images/tab_bg.jpg) repeat-y;*/
/*	border: 1px solid #003366;  */
	text-decoration: none;

	font-size: 16px;
	font-family: Times New Roman; 
	font-style: italic; 
	font-weight: normal;
	color: #804000;
}

ul#sliding-navigation li.sliding-element a:hover { color: #804000; }


FINCSS;
?>