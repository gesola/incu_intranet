<?php
########################################################
##                                                    ##
##           ACTUALIZACIONES DE VERSION               ##
##                                                    ##
##  - Actualización 09-09-2014 - GES - Version 4.0    ##
##                                                    ##
########################################################
##  Clase: consulta                                   ##
##  Descripcion: Crea la conexion con la base de datos##
##   al contruir el objeto, y se encarga de realizar  ##
##   las consultas utilizadas por la aplicacion       ##
########################################################
## error_reporting(E_ALL ^ E_DEPRECATED);


class Consulta {
## Atributos
	private $connect;           ## Conexion con la base de datos
	private $sql;               ## Cadena sql de la ultima consulta
	private $tabla;             ## RecordSet con los datos de la ultima consulta
	private $camposVariables;   ## Valor para consultas con cantidad de campos variables
	private $cuadroNotasTitulo; ## titulos para metodo cuadroDeNotasDeCurso
	private $seleccion;	        ## columna seleccionada en metodo cuadroDeNotas
	private $tipoMotor;         ## identifica el motor utilizado 

## Constructor
	function __construct(){
		$this->tipoMotor = "mysqli";		## Aqui se elije el motor de base de datos a usar(mysql|mysqli)
		@require(dirname(__FILE__) . "/" . $this->tipoMotor . ".inc");
		$this->connect = $connect;
	}


###########################################################################################################################
##
## CONSULTAS
##
###########################################################################################################################

##############################################################################################
## Consultas a las tablas
## Momenclatura: Cuando los metodos comienzan con lista se refiere a un conjunto de registros
##       Cuando comienzan con cuadro se trata de un conjunto de registros, que aparete un solo registro puede
##       contener datos de muchos registros (ej todas las notas de un alumno, en la base cada nota es un registro)


## METODO:  validaUsuario (USADO EN: loguin.php)
## Entrada: recibe codigo de usuario y contraseña.
## Salida:  |Todos los registros de la tabla usuario|
## Retorna registro del usuario a validar.
	function validaUsuario($codUsuario, $clave){
		$this->sql = "SELECT * FROM usuarios WHERE codigo='{$codUsuario}' AND clave='{$clave}'";
		$this->query();
		return $this->tabla;
	}


## METODO:  creaMenu (USADO EN: menupri.inc)
## Entrada: recibe parent.
## Salida:  |Todos los registros de la tabla menu|
## Retorna Todos los registros correpondientes al menu del usuario.
	function creaMenu($parent){
		$this->sql = "SELECT a.id, a.nombre, a.link, Deriv1.Count FROM menu a LEFT OUTER JOIN (SELECT id_padre, COUNT(*) AS Count FROM menu GROUP BY id_padre) Deriv1 ON a.id = Deriv1.id_padre WHERE a.id_padre='{$parent}'";
		$this->query();
		return $this->tabla;
	}


## METODO:  validaAccesoMenu (USADO EN: menupri.inc)
## Entrada: recibe id del menu y id del usuario.
## Salida:  |los registros de la tabla menu|
## Retorna Todos los registros correpondientes al menu del usuario.
	function validaAccesoMenu($rowid, $usuarioid){
		$this->sql = "SELECT acceso FROM macceso WHERE id_menu='{$rowid}' AND id_usuario='{$usuarioid}'";
		$this->query();
		return $this->tabla;
	}
	

## METODO:  listaEstructura (USADO EN: estructura.inc)
## Salida:  |todos los registros de la tabla estructura|
## Retorna Todos los registros correpondientes a la tabla estructura.
#	function listaEstructura(){
#		$this->sql = "SELECT acceso FROM macceso WHERE id_menu='{$rowid}' AND id_usuario='{$usuarioid}'";
#		$this->query();
#		return $this->tabla;
#	}
	
	
###########################################################################################################################
##
## CREACION (INSERTS)
##
###########################################################################################################################	

## METODO:  agregaAnoLectivo (USADO EN: anolectivo_alta.php)
## Entrada: Recibe las fechas del año lectivo para agregar.
## Salida:  
## Agrega fechas de año lectivo.
	function agregaAnoLectivo($anolectivo,$fechin1c, $fechout1c, $fechin2c, $fechout2c){
		$this->sql = "INSERT INTO alectivo (anolectivo,fechin_1c,fechout_1c,fechin_2c,fechout_2c) VALUES ('{$anolectivo}', '{$fechin1c}', '{$fechout1c}', '{$fechin2c}', '{$fechout2c}')";
		$this->query();
	}


################################################################################################################################################
################################################################################################################################################
#
## METODO:  listaAnoLectivo (USADO EN: curso_list.php)
## Entrada: No recibe datos.
## Salida:  |Id de año lectivo|Año lectivo|
## Retorna la lista de años lectivos que tienen cursos asignados.
	function listaAnoLectivo(){
		$this->sql = "SELECT id,anolectivo FROM curso GROUP BY anolectivo ORDER BY anolectivo ASC";
		$this->query();
		return $this->tabla;
	}


## METODO:  listaRegistrosAsistencia (USADO EN: curso_list.php)
## Entrada: Recibe el ID de cursada
## Salida:  |Id de asistencia
## Retorna todos los registros de asistencia de una cursada.
	function listaRegistrosAsistencia($curso){
		$this->sql = "SELECT id FROM asistencia WHERE cod_cursada = {$curso}";
		$this->query();
		return $this->tabla;
	}

## METODO:  listaUsuarios (USADO EN: loguin_su.php)
## Entrada: recibe un codigo de Tipo de Usuario.
## Salida:  |Todos los registros de la tabla usuario|
## Retorna la lista de materia de la carrera elejida o todas si el id de carreras es NULL.
	function listaUsuarios($idTipoUsuario){
		$this->sql = "SELECT Id,UPPER(nombres) AS nombre,UPPER(apellidos) AS apellido,codigo,tipo,clave,activo,valid FROM usuarios WHERE tipo='{$idTipoUsuario}' ORDER BY apellidos ASC, nombres ASC";
		$this->query();
		return $this->tabla;
	}

## Modificacion de las tablas

## METODO:  actualizaNota (USADO EN: notas_list)
## Entrada: Recibe todos los campos de un registro de NOTAS pudiendo ser nulos.
## Proceso: Si no hay nota elimina el registro.
##          Si hay nota y existe el registro lo actualiza.
##          Si hay nota y no existe el registro lo inserta.
	function actualizaNota($id, $cod_cursada, $cod_evaluacion, $cod_alumno, $nota){
	    $nota = trim($nota);
		if(is_numeric($nota)){
			if(is_numeric($id)){
				//Hay nota y existe el registro => Actualizo el registro
				$this->sql = "UPDATE notas SET nota={$nota} WHERE {$id} = id";

			}else{
				//Hay nota pero no existe el registro => Agrego un registro con la nota
				$this->sql = "INSERT INTO notas (cod_cursada, cod_evaluacion, cod_alumno, nota) VALUES ({$cod_cursada}, {$cod_evaluacion}, {$cod_alumno}, {$nota})";
			}
		}else{
			//No hay nota => elimino el registro si existe
			$this->sql = "DELETE FROM notas WHERE cod_evaluacion={$cod_evaluacion} AND cod_alumno={$cod_alumno}";
		}

		$this->query();
	}

## METODO:  actualizaFeriado (USADO EN: calendario.php)
## Entrada: Recibe la operacion a realizar y los datos pertinentes segun su operacion.
## Proceso: Eliminar recibe una lista de fecha a eliminar.
##          Modificar recibe una fecha y la descripcion que va a modificar.
##          Insertar recibe la fecha y descripcion a insertar.
	function actualizaFeriado($operacion, $datos){
		if($operacion == 'eliminar'){        // datos 0-fecha1| 1-fecha2| .....
			$temp = "";
			$disyuncion ="";
			foreach($datos as $fecha){
				$temp = $temp . $disyuncion . "fecha ='" . $fecha. "'";
				$disyuncion = " OR ";
			}
			$this->sql = "DELETE FROM feriados WHERE " . $temp;
		}else if($operacion == "modificar"){ // datos 0-fecha| 1-descripcion
			$this->sql = "UPDATE feriados SET descripcion='{$datos[1]}' WHERE '{$datos[0]}' = fecha";
		}else if($operacion == "insertar"){  // datos 0-fecha| 1-descripcion
			$this->sql = "INSERT INTO feriados (fecha, descripcion) VALUES ('{$datos[0]}', '{$datos[1]}')";
		}

		$this->query();
	}


## METODO:  agregaAnoLectivo (USADO EN: anolectivo_alta.php)
## Entrada: Recibe las fechas del año lectivo para agregar.
## Salida:  
## Agrega fechas de año lectivo.
#	function agregaAnoLectivo($anolectivo,$fechin1c, $fechout1c, $fechin2c, $fechout2c){
#		$this->sql = "INSERT INTO alectivo (anolectivo,fechin_1c,fechout_1c,fechin_2c,fechout_2c) VALUES ('{$anolectivo}', '{$fechin1c}', '{$fechout1c}', '{$fechin2c}', '{$fechout2c}')";
#		$this->query();
#	}	
	
## METODO:  actualizaTipoNota (USADO EN: tiponota_modif.php)
## Entrada: Recibe el tipo de nota para actualizar y el id.
## Salida:  
## Actualiza un tipo de nota.
	function actualizaTipoNota($id, $tipoNota){
		$this->sql = "UPDATE tiponota SET tiponota='{$tipoNota}' WHERE id='{$id}'";
		$this->query();
	}

## METODO:  eliminaCalendarioAnual (USADO EN: calendario.php)
## Entrada: Recibe el año requerido.
## Salida:  
## Elimina todos los dias de el año seleccionado
	function eliminaCalendarioAnual($anio){
		$this->sql = "DELETE FROM calendario WHERE YEAR(fecha) = '{$anio}'";
		$this->query();
	}


## METODO:  eliminaTipoNota (USADO EN: tipanota_list.php)
## Entrada: Id del tipo de nota.
## Salida:  
## Elimina el tipo de nota elegido
	function eliminaTipoNota($id){
		$this->sql = "DELETE FROM tiponota WHERE id='{$id}'";
		$this->query();
	}


###########################################################################################################################
##
## METODOS
##
###########################################################################################################################

## METODOS PARA MANEJAR MYSQL 
## Devuelve un resaltador cuando $indice coincida con la evaluacion seleccionada en cuadroDeNotasDeCurso
	function cuadroSeleccion($indice, $inicioFin){
		return ($this->seleccion == $indice+1)? (($inicioFin == "FIN")?"</strong>":"<strong>"):"";
	}

## Titulos de cuadroDeNotasDeCurso
	function cuadroTitulos($indice){
		return "Nota: ".$this->cuadroNotasTitulo[$indice];
	}
## Camtidad de registros de la ultima consulta
	function cantidadUltimaTabla(){
		return $this->cantidad($this->tabla);
	}
## Cantidad de registros
	function cantidad($registros){
		if($this->tipoMotor == "mysql"){
			return @mysql_num_rows($registros);
		}elseif($this->tipoMotor == "mysqli"){
			return $registros->num_rows;
		}
	}
	
	function cantidadCamposVariables(){
		return $this->camposVariables;
	}
## Procesa consulta
	private function query(){
		if($this->tipoMotor == "mysql"){
			$this->tabla = @mysql_query($this->sql, $this->connect);
		}elseif($this->tipoMotor == "mysqli"){
			$this->tabla = $this->connect->query($this->sql);
		}
	}
## Errores
	function error(){
		if($this->tipoMotor == "mysql"){
			return @mysql_error($this->connect);
		}elseif($this->tipoMotor == "mysqli"){
			return $this->connect->error;
		}
	}
## Debug
	function reportar($texto){
		file_put_contents('php://stderr', print_r('                               Debug: '.$texto, TRUE));
	}

	function mostrarCelda($texto,$alineacion){
		$align = "";
		if($alineacion=="center"){
			$align = "style='text-align: center;'";
		}
		echo "<abbr title='{$texto}'><div {$align} class='celda'>{$texto}</div></abbr>";
	}

	function fetch_array($registros){
		if($this->tipoMotor == "mysql"){
			return @mysql_fetch_array($registros);
		}elseif($this->tipoMotor == "mysqli"){
			return $registros->fetch_array(MYSQLI_BOTH);
		}

	}

	function data_seek($registros, $pos){
		if($this->tipoMotor == "mysql"){
			return @mysql_data_seek($registros, $pos);
		}elseif($this->tipoMotor == "mysqli"){
			return $registros->data_seek($pos);
		}
	}

	//$miConsulta->fetch_assoc($fresult) Ejemplo de uso
	function fetch_assoc($registros){
		if($this->tipoMotor == "mysql"){
			return @mysql_fetch_assoc($registros);
		}elseif($this->tipoMotor == "mysqli"){
			return $registros->fetch_assoc();
		}

	}

	function insert_id(){
		if($this->tipoMotor == "mysql"){
			return @mysql_insert_id($this->connect);
		}elseif($this->tipoMotor == "mysqli"){
			return $this->connect->insert_id;
		}

	}

}
