<?php 
include ("../../config/config.inc");
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"> 
<!-- <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> -->
<html>
<head>
	<title>GAD - Gesti&oacute;n Administrativa (by GeSist)</title>
	<link rel="shortcut icon" href="../../images/favicon.gif" >

<link rel="stylesheet" type="text/css" href="../../includes/css/gadstyle.php" media="all">
<link rel="stylesheet" type="text/css" href="../../includes/css/nivelstyle.css" media="all">
<link rel="stylesheet" type="text/css" href="../../includes/css/main.css" media="all">
<link rel="stylesheet" type="text/css" href="../../includes/css/jquery.clickme.php" media="all">
<link rel="stylesheet" type="text/css" href="../../includes/css/droplinebar.php" media="all"> <!-- DropLineTab Menu -->
<link rel="stylesheet" type="text/css" href="../../includes/css/jquery.alerts.php" media="all">
<link rel="stylesheet" type="text/css" href="../../includes/css/blue/style.php" media="print, projection, screen" />
<link rel="stylesheet" type="text/css" href="../../includes/css/jquery.tablesorter.pager.php" media="all">
<link rel="stylesheet" type="text/css" href="../../includes/css/dropdown.css" media="all"> <!-- Menú Horizontal -->

<script src="../../includes/js/jquery.js" language="javascript" type="text/javascript"></script> 
<script src="../../includes/js/jquery.clickme.js" language="javascript" type="text/javascript"></script>
<script src="../../includes/js/droplinemenu.js" language="javascript" type="text/javascript"></script> <!-- DropLineTab Menu -->
<script src="../../includes/js/jquery.alerts.js" language="javascript" type="text/javascript"></script> 
<script src="../../includes/js/jquery.form.js" language="javascript" type="text/javascript"></script> 
<script src="../../includes/js/jquery.tablesorter.js" language="javascript" type="text/javascript"></script>
<script src="../../includes/js/jquery.tablesorter.pager.js" language="javascript" type="text/javascript"></script>
<script src="../../includes/js/jquery.cascade.js" language="javascript" type="text/javascript"></script>
<script src="../../includes/js/jquery.cascade.ext.js" language="javascript" type="text/javascript"></script>
<script src="../../includes/js/jquery.templating.js" language="javascript" type="text/javascript"></script>
<!--
<script src="../../includes/js/jquery.loadingdotdotdot.js" language="javascript" type="text/javascript"></script>
-->

<script type="text/javascript">
//build menu with DIV ID="myslidemenu" on page:
droplinemenu.buildmenu("mydroplinemenu")
</script>

</head>
<body bgcolor="<?php echo $colbodfond;?>" text="<?php echo $colbodfont;?>" leftmargin="2" topmargin="5" marginwidth="0" marginheight="0">

<?php /*
<!-- Script para el menu del encabezado -->
<script type="text/javascript">
<!--
	$(document).ready(function() {
		$(".nav").clickMe();
	});
--> 
</script>
*/ ?>

<table width="1024" align="center" border="1" style="border-collapse: collapse; border-color: #003366;" cellspacing="0" cellpadding="0">
  <tr> 
    <td align="left"><br>&nbsp;&nbsp;<img src="../../images/agip.png" width="150">
	<h1 align="center"><?php echo $htitulo; ?></h1>
    </td>
  </tr>
  <tr> 
    <td>
        <?php 
		if ($_SESSION["Valid"] == 1) 
			{
				include ("../../includes/menupri.inc");
			} else {
				include ("../../includes/menumi.inc");
			}
?>

    </td>
  </tr>

  <tr> 
 <td align="center">
<div class="content">
