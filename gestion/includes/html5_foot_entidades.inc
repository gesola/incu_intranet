<!-- /page content -->
        <!-- footer content -->
        <footer>   
          <div>                             
              <p align="center"><b>GesTec - Gesti&oacute;n Tecnol&oacute;gica | Desde 2019 | &copy; <?php echo date('Y')."&nbsp;&nbsp;GES.";?></b></p>                
          </div>            
          <div class="pull-right">            
                <b>Desarrollo:</b>&nbsp;&nbsp;<a href="www.gesist.com.ar">Lic. Guillermo E. Sola.</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br> 
		<b>E-Mail:</b>&nbsp;&nbsp;<a href="mailto:agip.gob.ar">gesola@agip.gob.ar</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          </div>

            <div class="clearfix"></div>
        </footer>
        
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Bootstrap-confirmation -->
    <script src="../../vendors/Bootstrap-Confirmation-master/bootstrap-confirmation.js"></script>

    <!-- FastClick -->
    <script src="../../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../../vendors/nprogress/nprogress.js"></script>
    <!-- validator -->
    <script src="../../vendors/validator/validator.js"></script>
    <!-- Chart.js -->
    <script src="../../vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="../../vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="../../vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="../../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../../vendors/pdfmake/build/vfs_fonts.js"></script>
    <!-- Skycons -->
    <script src="../../vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="../../vendors/Flot/jquery.flot.js"></script>
    <script src="../../vendors/Flot/jquery.flot.pie.js"></script>
    <script src="../../vendors/Flot/jquery.flot.time.js"></script>
    <script src="../../vendors/Flot/jquery.flot.stack.js"></script>
    <script src="../../vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="../../vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="../../vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="../../vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="../../vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="../../vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="../../vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="../../vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    
   
    <!-- bootstrap-daterangepicker -->
    <script src="../../vendors/moment/min/moment.min.js"></script>
    <script src="../../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- jQuery Tags Input -->
    <script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Switchery -->
    <script src="../../vendors/switchery/dist/switchery.min.js"></script>
    <!-- Select2 -->
    <script src="../../vendors/select2/dist/js/select2.full.min.js"></script> 
    <!-- Parsley -->
    <script src="../../vendors/parsleyjs/dist/parsley.min.js"></script> 
    <!-- Autosize -->
    <script src="../../vendors/autosize/dist/autosize.min.js"></script> 
    <!-- jQuery autocomplete -->
    <script src="../../vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script> 
    <!-- starrr -->
    <script src="../../vendors/starrr/dist/starrr.js"></script>
    <!-- bootstrap-datetimepicker -->    
    <script src="../../vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <!-- jquery.inputmask -->
    <script src="../../vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>    
 <?php /*  
    <!-- Ion.RangeSlider -->
    <script src="../../vendors/ion.rangeSlider/js/ion.rangeSlider.min.js"></script>
    <!-- Bootstrap Colorpicker -->
    <script src="../../vendors/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>

    <!-- jQuery Knob -->
    <script src="../../vendors/jquery-knob/dist/jquery.knob.min.js"></script>
    <!-- Cropper -->
    <script src="../../vendors/cropper/dist/cropper.min.js"></script>
   
     <!-- Combo select -->
    <script src="../../includes/js/jquery.cascade.js" language="javascript" type="text/javascript"></script>
    <script src="../../includes/js/jquery.cascade.ext.js" language="javascript" type="text/javascript"></script>
*/ ?> 
    <!-- Custom Theme Scripts -->  
   <!-- <script src="../../build/js/custom.min.js"></script> -->
 <script src="../../build/js/custom.js"></script>   
 <script src="../../build/js/subida.js"></script>
 

<script>
  console.log('Bootstrap ' + $.fn.tooltip.Constructor.VERSION);
  console.log('Bootstrap Confirmation ' + $.fn.confirmation.Constructor.VERSION);

  $('[data-toggle=confirmation]').confirmation({
    rootSelector: '[data-toggle=confirmation]',
    container: 'body'
  });
  $('[data-toggle=confirmation-singleton]').confirmation({
    rootSelector: '[data-toggle=confirmation-singleton]',
    container: 'body'
  });
  $('[data-toggle=confirmation-popout]').confirmation({
    rootSelector: '[data-toggle=confirmation-popout]',
    container: 'body'
  });

  $('#confirmation-delegate').confirmation({
    selector: 'button'
  });

  var currency = '';
  $('#custom-confirmation').confirmation({
    rootSelector: '#custom-confirmation',
    container: 'body',
    title: null,
    onConfirm: function(currency) {
      alert('You choosed ' + currency);
    },
    buttons: [
      {
        class: 'btn btn-danger',
        icon: 'glyphicon glyphicon-usd',
        value: 'US Dollar'
      },
      {
        class: 'btn btn-primary',
        icon: 'glyphicon glyphicon-euro',
        value: 'Euro'
      },
      {
        class: 'btn btn-warning',
        icon: 'glyphicon glyphicon-bitcoin',
        value: 'Bitcoin'
      },
      {
        class: 'btn btn-default',
        icon: 'glyphicon glyphicon-remove',
        cancel: true
      }
    ]
  });

  $('#custom-confirmation-links').confirmation({
    rootSelector: '#custom-confirmation-link',
    container: 'body',
    title: null,
    buttons: [
      {
        label: 'Twitter',
        attr: {
          href: 'https://twitter.com'
        }
      },
      {
        label: 'Facebook',
        attr: {
          href: 'https://facebook.com'
        }
      },
      {
        label: 'Pinterest',
        attr: {
          href: 'https://pinterest.com'
        }
      }
    ]
  });
</script>

<!--    
<script>
  document.write('<script src="//' + location.host.split(':')[0] + ':35729/livereload.js" async defer><' + '/script>');
</script>
-->

   <!-- Initialize datetimepicker -->
<script>   
    $('#myDatepicker2').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    
    $('#myDatepicker3').datetimepicker({
        format: 'HH:mm'
    });
    
    $('#myDatepicker4').datetimepicker({
        format: 'HH:mm'
    });    
</script>

<!-- Scripts Entidades -->

<script type="text/javascript">

$(document).ready(function() {

    $(document).on('change', '.pais', function(){
        var id_pais = $(this).val();
        var provincia_id = $(this).data('provincia_id');
/*      alert("id_pais: " + id_pais);
        alert("provincia_id: " + provincia_id);   */    
	$.ajax({
	type: "POST",
	url: "getprovincia.php",
	data:'id_pais='+id_pais,
	success: function(data){
		$("#provincia-list"+provincia_id).html(data);
	}
	});       
      });
      
    $(document).on('change', '.provincia', function(){
        var id_prov = $(this).val();
        var ciudad_id = $(this).data('ciudad_id');  
	$.ajax({
	type: "POST",
	url: "getciudad.php",
	data:'id_prov='+id_prov,
	success: function(data){
		$("#ciudad-list"+ciudad_id).html(data);
	}
	});       
      });      
      
// Oculto personas fisica y juridica
   $(window).bind("load",function() {       
       if ($("#fisica").is(":checked"))
            {
                $("#pers_fisica").css("display", "block");
                $("#pers_juridica").css("display", "none");
                $("#contacto_fisico").css("display", "block");
                $("#contacto_juridico").css("display", "none");                
            } else {          
                if ($("#juridica").is(":checked"))
                    {
                        $("#pers_fisica").css("display", "none");
                        $("#pers_juridica").css("display", "block");
                        $("#contacto_fisico").css("display", "none");
                        $("#contacto_juridico").css("display", "block");
                    } else {
                        $("#pers_juridica").css("display", "none");
                        $("#pers_fisica").css("display", "none");
                        $("#contacto_fisico").css("display", "none");
                        $("#contacto_juridico").css("display", "none");
                    }
            }        
   });

// Activo persona fisica y desactivo juridica
   $("#fisica").on('ifChanged', function(event){
         $("#pers_fisica").css("display", "block");
         $("#pers_juridica").css("display", "none");
         $("#contacto_fisico").css("display", "block");
         $("#contacto_juridico").css("display", "none");
   });
                 
// Activo persona juridica y desactivo fisica
   $("#juridica").on('ifChanged', function(event){
         $("#pers_juridica").css("display", "block");
         $("#pers_fisica").css("display", "none");
         $("#contacto_fisico").css("display", "none");
         $("#contacto_juridico").css("display", "block");
   });

// Agrego registros nuevos en tabla domicilio de formulario entidad.
/* var count = 0; */
var count = $("#vueltadom").val(); 

$("#agrega_dom").click(function(){
    count++;    
<?php 
require_once(dirname(__FILE__) . "/../clases/cliente.class.inc");
$miCliente = new Cliente();

// Query para tipo de Domicilio.
$sresulttd = $miCliente->listaTipoDom();
$scanttd = $miCliente->cantidadUltimaTabla();

require_once(dirname(__FILE__) . "/../clases/localizacion.class.inc");
$miLocalizacion = new Localizacion();

$spaisresult = $miLocalizacion->listaPaises();
$spacant = $miLocalizacion->cantidadUltimaTabla();

?>
           
/* alert("Variable php: " + vphp); */
    var item = `
        <tr>
        <td>
            <select class="form-control" style="font-size:12px;padding:0px;margin-right:10px;height:20px;" name="id_tipo_dom[]" required="required">
		<option value="">Seleccione...</option>
                    <?php while($srowtd = $miCliente->fetch_array($sresulttd))
                        { 
                        ?>
                            <option value="<?php print $srowtd["id"]; ?>"><?php print $srowtd["domicilio"]; ?></option>                        
                    <?php                    
                        }
?>                        
	    </select>
        </td>
        <td><input class="form-control" style="font-size:12px;padding:0px;margin-right:10px;height:20px;" type="text" name="calle[]" placeholder="Calle"/></td>
        <td><input class="form-control" style="font-size:12px;padding:0px;margin-right:10px;height:20px;" type="text" name="numero[]" placeholder="Numero"/></td>
        <td><input class="form-control" style="font-size:12px;padding:0px;margin-right:10px;height:20px;" type="text" name="piso[]" placeholder="Piso"/></td>            
        <td><input class="form-control" style="font-size:12px;padding:0px;margin-right:10px;height:20px;" type="text" name="of_local[]" placeholder="Oficina/Local"/></td>
        <td><input class="form-control" style="font-size:12px;padding:0px;margin-right:10px;height:20px;" type="text" name="cp[]" placeholder="C&oacute;odigo Postal"/></td>
            
        <td><select name="pais[]" id="pais-list" class="form-control demoInputBox pais" style="font-size:12px;padding:0px;margin-right:10px;height:20px;" data-provincia_id="`+ count +`">
		<option value="">Seleccione Pa&iacute;s</option>
                    <?php while($srowpais = $miLocalizacion->fetch_array($spaisresult))
                        { 
                        ?>
                            <option value="<?php print $srowpais["id"]; ?>"><?php print $srowpais["nombre"]; ?></option>                        
                    <?php                    
                        }
?>                        
	    </select>
        </td>
        <td>
            <select name="id_provincia[]" id="provincia-list`+ count +`" class="form-control demoInputBox provincia" style="font-size:12px;padding:0px;margin-right:10px;height:20px;" data-ciudad_id="`+ count +`">    
		<option value="">Seleccione Provincia</option>
	    </select>
        </td>
        <td>
            <select name="id_ciudad[]" id="ciudad-list`+ count +`" class="form-control demoInputBox" style="font-size:12px;padding:0px;margin-right:10px;height:20px;">    
		<option value="">Seleccione Ciudad</option>
	    </select>
        </td>                                 
      </tr>
    `;
    $("#domicilio").append(item)
  });


// Agrego registros nuevos en tabla medios de formulario entidad.

$("#agrega_medio").click(function(){
<?php 
require_once(dirname(__FILE__) . "/../clases/cliente.class.inc");
$miCliente = new Cliente();

// Query para tipo de Medio de Comunicacion.
$ssresulttd = $miCliente->listaTipoMedioCom();
$sscanttd = $miCliente->cantidadUltimaTabla();
?>
/* alert("Hello! I am an alert box!!"); */

    var item = `
        <tr>
        <td>
            <select class="form-control" name="id_tipo_medio[]" required="required">
		<option value="">Seleccione...</option>
                    <?php while($ssrowtd = $miCliente->fetch_array($ssresulttd))
                        { 
                        ?>
                            <option value="<?php print $ssrowtd["id"]; ?>"><?php print $ssrowtd["medio"]; ?></option>                        
                    <?php                    
                        }
?>                        
	    </select>
        </td>
        <td><input class="form-control" type="text" name="datos_medio[]" placeholder="Detalle"/></td>                     
      </tr>
    `;        

    $("#medio").append(item)
  });


$("#agrega_contacto").click(function(){
<?php 
require_once(dirname(__FILE__) . "/../clases/cliente.class.inc");
$miCliente = new Cliente();

// Query para Contacto.
$resultmcnt = $miCliente->listaContacto(1);
$cantmcnt = $miCliente->cantidadUltimaTabla();
?>

    var icontact = `
        <tr>
        <td>
            <select class="form-control" name="id_contacto[]" required="required">
		<option value="">Seleccione...</option>
                    <?php while($rowmcnt = $miCliente->fetch_array($resultmcnt))
                        { 
                        ?>
                            <option value="<?php print $rowmcnt["id"]; ?>"><?php print $rowmcnt["nombre"]; ?></option>                        
                    <?php                    
                        }
?>                        
	    </select>
        </td>
        <td><input class="form-control" type="text" name="puesto[]" placeholder="Puesto"/></td>                     
      </tr>
    `;        

    $("#contacto").append(icontact)
  });

           
   // Verifico tipo al hacer click en grabar
   $("#boton").click(function(evento){
     	if ($("#cliente").attr("checked") || $("#proveedor").attr("checked")){ } else {
		jAlert("<h3>Debe seleccionar Entidad.</h3>", "Error en formulario");
		return false;
	}
      });
});

// Completo Razon Social
$("#nombre_empresa").blur(function(evento){
    $("#razon_social").val($("#nombre_empresa").val());
  });
</script>

</body>
</html>

