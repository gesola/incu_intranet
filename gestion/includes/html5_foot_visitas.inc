<!-- /page content -->

        <!-- footer content -->
        <footer>   
          <div>  
              <p align="center"><b>GAD - Gesti&oacute;n Administrativa | Desde 2005 | &copy; <?php echo date('Y')."&nbsp;&nbsp;GeSist.";?></b></p>                
          </div>            
          <div class="pull-right">            
                <b>Desarrollo:</b>&nbsp;&nbsp;<a href="www.gesist.com.ar">Gesist - Gesti&oacute;n en Sistemas.</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br> 
		<b>E-Mail:</b>&nbsp;&nbsp;<a href="mailto:informes@gesist.com.ar">informes@gesist.com.ar</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          </div>

            <div class="clearfix"></div>
        </footer>
        
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Bootstrap-confirmation -->
    <script src="../../vendors/Bootstrap-Confirmation-master/bootstrap-confirmation.js"></script>
   
    <!-- FastClick -->
    <script src="../../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../../vendors/nprogress/nprogress.js"></script>
    <!-- validator -->
    <script src="../../vendors/validator/validator.js"></script>
    <!-- Chart.js -->
    <script src="../../vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="../../vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="../../vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="../../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../../vendors/pdfmake/build/vfs_fonts.js"></script>
    <!-- Skycons -->
    <script src="../../vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="../../vendors/Flot/jquery.flot.js"></script>
    <script src="../../vendors/Flot/jquery.flot.pie.js"></script>
    <script src="../../vendors/Flot/jquery.flot.time.js"></script>
    <script src="../../vendors/Flot/jquery.flot.stack.js"></script>
    <script src="../../vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="../../vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="../../vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="../../vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="../../vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="../../vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="../../vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="../../vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../../vendors/moment/min/moment.min.js"></script>
    <script src="../../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- bootstrap-datetimepicker -->    
    <script src="../../vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
 <?php /* 
    <!-- Ion.RangeSlider -->
    <script src="../../vendors/ion.rangeSlider/js/ion.rangeSlider.min.js"></script>
    <!-- Bootstrap Colorpicker -->
    <script src="../../vendors/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
    <!-- jquery.inputmask -->
    <script src="../../vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <!-- jQuery Knob -->
    <script src="../../vendors/jquery-knob/dist/jquery.knob.min.js"></script>
    <!-- Cropper -->
    <script src="../../vendors/cropper/dist/cropper.min.js"></script>
*/ ?>   
     <!-- Combo select -->
    <script src="../../includes/js/jquery.cascade.js" language="javascript" type="text/javascript"></script>
    <script src="../../includes/js/jquery.cascade.ext.js" language="javascript" type="text/javascript"></script>
 
    <!-- Custom Theme Scripts -->
    
   <!-- <script src="../../build/js/custom.min.js"></script> --> 
    <script src="../../build/js/custom.js"></script> 
<script>
		jQuery(document).ready(function()
		{	
			jQuery("#optional_child").cascade("#optional",{						
				list: list1,			
				template: commonTemplate,
				match: commonMatch 
			})
			.bind("loaded.cascade",function(e,target) { 
				jQuery(this).find("option:first")[0].selected = true;
			});
		});
</script> 
<script>
  console.log('Bootstrap ' + $.fn.tooltip.Constructor.VERSION);
  console.log('Bootstrap Confirmation ' + $.fn.confirmation.Constructor.VERSION);

  $('[data-toggle=confirmation]').confirmation({
    rootSelector: '[data-toggle=confirmation]',
    container: 'body'
  });
  $('[data-toggle=confirmation-singleton]').confirmation({
    rootSelector: '[data-toggle=confirmation-singleton]',
    container: 'body'
  });
  $('[data-toggle=confirmation-popout]').confirmation({
    rootSelector: '[data-toggle=confirmation-popout]',
    container: 'body'
  });

  $('#confirmation-delegate').confirmation({
    selector: 'button'
  });

  var currency = '';
  $('#custom-confirmation').confirmation({
    rootSelector: '#custom-confirmation',
    container: 'body',
    title: null,
    onConfirm: function(currency) {
      alert('You choosed ' + currency);
    },
    buttons: [
      {
        class: 'btn btn-danger',
        icon: 'glyphicon glyphicon-usd',
        value: 'US Dollar'
      },
      {
        class: 'btn btn-primary',
        icon: 'glyphicon glyphicon-euro',
        value: 'Euro'
      },
      {
        class: 'btn btn-warning',
        icon: 'glyphicon glyphicon-bitcoin',
        value: 'Bitcoin'
      },
      {
        class: 'btn btn-default',
        icon: 'glyphicon glyphicon-remove',
        cancel: true
      }
    ]
  });

  $('#custom-confirmation-links').confirmation({
    rootSelector: '#custom-confirmation-link',
    container: 'body',
    title: null,
    buttons: [
      {
        label: 'Twitter',
        attr: {
          href: 'https://twitter.com'
        }
      },
      {
        label: 'Facebook',
        attr: {
          href: 'https://facebook.com'
        }
      },
      {
        label: 'Pinterest',
        attr: {
          href: 'https://pinterest.com'
        }
      }
    ]
  });
</script>

<script>
  document.write('<script src="//' + location.host.split(':')[0] + ':35729/livereload.js" async defer><' + '/script>');
</script>


   <!-- Initialize datetimepicker -->
<script>   
    $('#myDatepicker2').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    
    $('#myDatepicker3').datetimepicker({
        format: 'HH:mm'
    });
    
    $('#myDatepicker4').datetimepicker({
        format: 'HH:mm'
    });    
</script>


<!-- Scripts Visitas -->

<script type="text/javascript">
   
 /*$(document).ready(function() { */
/*$(window).bind("load",function() { */

   function padNmb(nStr, nLen){
    var sRes = String(nStr);
    var sCeros = "0000000000";
    return sCeros.substr(0, nLen - sRes.length) + sRes;
   }

   function stringToSeconds(tiempo){
    var sep1 = tiempo.indexOf(":");
    var sep2 = tiempo.lastIndexOf(":");
    var hor = tiempo.substr(0, sep1);
    var min = tiempo.substr(sep1 + 1, sep2 - sep1 - 1);
    var sec = tiempo.substr(sep2 + 1);
    return (Number(sec) + (Number(min) * 60) + (Number(hor) * 3600));
   }

   function secondsToTime(secs){
    var hor = Math.floor(secs / 3600);
    var min = Math.floor((secs - (hor * 3600)) / 60);
    var sec = secs - (hor * 3600) - (min * 60);
    return padNmb(hor, 2) + ":" + padNmb(min, 2) + ":" + padNmb(sec, 2);
   }

   function secondsToHoras(secs){
    var hhor = Math.floor(secs / 3600);
    var hmin = Math.floor((secs - (hhor * 3600)) / 60);
    var hsec = secs - (hhor * 3600) - (hmin * 60);
    return padNmb(hhor, 2);
   }

   function secondsToMinutos(secs){
    var mhor = Math.floor(secs / 3600);
    var mmin = Math.floor((secs - (mhor * 3600)) / 60);
    var msec = secs - (mhor * 3600) - (mmin * 60);
    return padNmb(mmin, 2);
   }

   function substractTimes(t1, t2){
    var secs1 = stringToSeconds(t1);
    var secs2 = stringToSeconds(t2);
    var secsDif = secs1 - secs2;
    return secondsToTime(secsDif);
   }

   function substractHoras(t1, t2){
    var secs1 = stringToSeconds(t1);
    var secs2 = stringToSeconds(t2);
    var secsDif = secs1 - secs2;
    return secondsToHoras(secsDif);
   }

   function substractMinutos(t1, t2){
    var secs1 = stringToSeconds(t1);
    var secs2 = stringToSeconds(t2);
    var secsDif = secs1 - secs2;
    return secondsToMinutos(secsDif);
   }

$("#horaini").blur(function(evento){
	$("#tvisita").attr("value", "Tiempo de Visita: "+substractTimes(($("#horafin").val()) + ":00", ($("#horaini").val()) + ":00")+" Hs.");

	$("#horas").attr("value", substractHoras(($("#horafin").val()) + ":00", ($("#horaini").val()) + ":00"));

	$("#minutos").attr("value", substractMinutos(($("#horafin").val()) + ":00", ($("#horaini").val()) + ":00"));

  });

$("#horafin").blur(function(evento){

	$("#tvisita").attr("value", "Tiempo de Visita: "+substractTimes(($("#horafin").val()) + ":00", ($("#horaini").val()) + ":00")+" Hs.");

	$("#horas").attr("value", substractHoras(($("#horafin").val()) + ":00", ($("#horaini").val()) + ":00"));

	$("#minutos").attr("value", substractMinutos(($("#horafin").val()) + ":00", ($("#horaini").val()) + ":00"));

  });

   
/* Combo para mostrar/ocultar por hora/operacion */

$(window).bind("load",function() {
	$("#horaope").css("display", "none");
	$("#horadic").css("display", "none");
	$("#fechaacep").css("display", "none");
});


/* Script para verificar precios de convenio con cliente o por defecto */
$("#preconv").change(function(evento){ 

var conv=0;
var cond=0;

        <?php while($cnvrow = $_SESSION["miTarifario"]->fetch_array($_SESSION["cnvresult"])) { ?>        

		if (<?php echo $cnvrow["tipo_ope"];?> == $("#tipoope").val() && <?php echo $cnvrow["moneda"];?> == $("#preconv").val())
			{
				if (<?php echo $cnvrow["horope"];?>==1)
					{
						$("#porhora").attr("checked", true); 
						$("#horaope").css("display", "block");
						$("#horadic").css("display", "block");

						if ($("#minutos").val() > 35)
							{
								var phoras=$("#horas").val();
								phoras++;

								$("#preciou").val("<?php echo $cnvrow["precio"];?>");
								$("#preciot").val("<?php echo $cnvrow["precio"];?>"*phoras);

								$("#vpreciou").val("<?php echo $cnvrow["precio"];?>");
								$("#vpreciot").val("<?php echo $cnvrow["precio"];?>"*phoras);

							} else {
								$("#preciou").val("<?php echo $cnvrow["precio"];?>");
								$("#preciot").val("<?php echo $cnvrow["precio"];?>"*$("#horas").val());

								$("#vpreciou").val("<?php echo $cnvrow["precio"];?>");
								$("#vpreciot").val("<?php echo $cnvrow["precio"];?>"*$("#horas").val());
							}

						conv++;
						$("#tipconv").val("Precio de convenio con el cliente.");
					}
				
				if (<?php echo $cnvrow["horope"];?>==2)
					{
						$("#porope").attr("checked", true); 
						$("#horaope").css("display", "none");
						$("#horadic").css("display", "none");
						$("#preciou").val("<?php echo $cnvrow["precio"];?>");
						$("#preciot").val("<?php echo $cnvrow["precio"];?>");
						$("#vpreciou").val("<?php echo $cnvrow["precio"];?>");
						$("#vpreciot").val("<?php echo $cnvrow["precio"];?>");
						conv++;
						$("#tipconv").val("Precio de convenio con el cliente.");
					}

 			} 
	<?php       
                } 
        ?>
	
if (conv == 0)
	{

        <?php while($cdvrow = $_SESSION["miTarifario"]->fetch_array($_SESSION["cvdresult"])) 
                { 
            ?>       

		if (<?php echo $cdvrow["tipo_ope"];?> == $("#tipoope").val() && <?php echo $cdvrow["moneda"];?> == $("#preconv").val())
			{
				if (<?php echo $cdvrow["horope"];?>==1)
					{
						$("#porhora").attr("checked", true); 
						$("#horaope").css("display", "block");
						$("#horadic").css("display", "block");

						if ($("#minutos").val() > 35)
							{
								var phoras=$("#horas").val();
								phoras++;

								$("#preciou").val("<?php echo $cdvrow["precio"];?>");
								$("#preciot").val("<?php echo $cdvrow["precio"];?>"*phoras);
								$("#vpreciou").val("<?php echo $cdvrow["precio"];?>");
								$("#vpreciot").val("<?php echo $cdvrow["precio"];?>"*phoras);
							} else {
								$("#preciou").val("<?php echo $cdvrow["precio"];?>");
								$("#preciot").val("<?php echo $cdvrow["precio"];?>"*$("#horas").val());
								$("#vpreciou").val("<?php echo $cdvrow["precio"];?>");
								$("#vpreciot").val("<?php echo $cdvrow["precio"];?>"*$("#horas").val());
							}

						cond++;
						$("#tipconv").val("Precio de convenio por defecto.");
					}
				
				if (<?php echo $cdvrow["horope"];?>==2)
					{
						$("#porope").attr("checked", true); 
						$("#horaope").css("display", "none");
						$("#horadic").css("display", "none");
						$("#preciou").val("<?php echo $cdvrow["precio"];?>");
						$("#preciot").val("<?php echo $cdvrow["precio"];?>");
						$("#vpreciou").val("<?php echo $cdvrow["precio"];?>");
						$("#vpreciot").val("<?php echo $cdvrow["precio"];?>");
						cond++;
						$("#tipconv").val("Precio de convenio por defecto.");
					}

 			} 
		<?php } //Fin While 
                ?>
	}
	
if (conv==0 && cond==0) {$("#tipconv").val("No hay precios registrados para esta operación.");}
  
});

function Bonif() {
    var checkBox = document.getElementById("bonif");
    var tipbonif = document.getElementById("tipbonif");
    if (checkBox.checked == true){
        tipbonif.style.display = "block";
		$("#preciou").val("0");
		$("#preciot").val("0");        
    } else {
       tipbonif.style.display = "none";
		$("#preciou").val($("#vpreciou").val());
		$("#preciot").val($("#vpreciot").val());       
    }
}

function Pagado() {
    var checkBox = document.getElementById("pagado");
    var tipbonif = document.getElementById("tippag");
    if (checkBox.checked == true){
        tippag.style.display = "block";      
    } else {
       tippag.style.display = "none";     
    }
}

</script>     

</body>
</html>

