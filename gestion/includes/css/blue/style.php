<?php
header('content-type:text/css');
 
include ("../css_conf.php");

echo <<<FINCSS

/* tables */

table.tablesorter thead tr th {

        background-color: $th_strip;
        border: 1px outset silver;
        color: $txt1;
        border-bottom: 2px $menu1 solid;
        border-collapse: collapse;
        font-size: 12px;
}

table.tablesorter tfoot tr th {

        background-color: $th_strip;
        border: 0px outset silver;
        color: $txt1;
        border-bottom: 2px $menu1 solid;
        border-collapse: collapse;
        font-size: 12px;
}

table.tablesorter thead tr .header {
	background-image: url(bg.gif);
	background-repeat: no-repeat;
	background-position: center right;
	cursor: pointer;
}

table.tablesorter tbody tr.odd td {

	background-color: $background;
        border-bottom: 1px $th_strip solid;
        border-top: 1px $th_strip solid;
/*	background-color:#F0F0F6;*/
}

table.tablesorter thead tr .headerSortUp {
	background-image: url(asc.gif);
}

table.tablesorter thead tr .headerSortDown {
	background-image: url(desc.gif);
}


table.tablesorter thead tr .headerSortDown, table.tablesorter thead tr .headerSortUp {
background-color: #8dbdd8;
}

#nada {
	background-image: url(blank.png);
}


FINCSS;
?>