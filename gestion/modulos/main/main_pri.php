<?php 

//Reanudamos la sesión
session_start();

$nombre="main_pri";
$descripcion="Pantalla de inicio del sistema";

// Configuro permisos.
$su=1;
$ad=1;
$op=1;
$us=1;

include ("../../includes/mysqli.inc");
include ("../../includes/estructura.inc");
include ("../../includes/validacion.inc");


##########################
### FUNCION validado() ###
##########################

function validado() {
	mainsus();
} ### FIN FUNCION validado ###


########################
### FUNCION logout() ###
########################

function logout() {

$htitulo="Gesti&oacute;n Administrativa"; //Texto de la barra de t�tulo
include ("../../includes/head2.inc");
?>

<div id="boxes">

<div id="dialog" class="window">
<strong>Su usuario no es valido para realizar la acci&oacute;n solicitada.</strong> 
<br><br>
<a href="#" class="close" id="boton">Cerrar</a>
</div>
  
<!-- Mask to cover the whole screen -->
  <div id="mask"></div>
</div>

<form name="volver" method="post" action="../../logout.php"></form>

<?php 
include ("../../includes/foot.inc");

} 
### Fin funcion logout() ###


#########################
### FUNCION mainsus() ###
#########################
	
function mainsus() {

$htitulo="Gesti&oacute;n Administrativa"; //Texto de la barra de t�tulo
	
require_once(dirname(__FILE__) . "/../../clases/validacion.class.inc");			    
$miValidacion = new Validacion();

$resultusu = $miValidacion->validaUsuario($_SESSION["Cod"]); 
$cantusu   = $miValidacion->cantidadUltimaTabla();                     

while($rowusu = $miValidacion->fetch_array($resultusu))
    {
		include ("../../includes/head2.inc");
?>

<table width=100% border="0" cellpadding="0" cellspacing="0" bordercolor="#336699">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td width="10">&nbsp;</td>
    <td><font size="5" face="Times New Roman, Times, serif"><strong>Bienvenido 
      			<?php print $rowusu["nombres"]." ".$rowusu["apellidos"]; ?>.</strong></font></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
<hr>
<?php 
} // FIN DEL WHILE

/*******************************************/

/* Includes para cada tipo de usuarios acá */

/*******************************************/
echo "<table width=100% border=\"0\"><tr valign=\"top\">";

/* SUPERUSUARIO */

// Informes de saldos deudores de clientes.
if ($_SESSION["Tip"]==1)
    {
        echo "<td width=50%>";
        include ("../../basico/operaciones/saldos_info.php");
        echo "</td>";       
    }
    
 // Informes de vencimientos de planes.
if ($_SESSION["Tip"]==1)
    {
        echo "<td width=50%>";    
        include ("../../planes/operaciones/planes_vencim.php");
        echo "</td>";         
    }
    
echo "</tr><tr valign=\"top\">";

 // Informes de vencimientos de licencias.
if ($_SESSION["Tip"]==1)
    {
        echo "<td width=100% colspan=\"2\">";    
        include ("../../licencias/main/licencias_vencim.php");
        echo "</td>";         
    }
    
echo "</tr></table><hr>";

  // Listado de Clientes con Planes de hosteo.

if ($_SESSION["Tip"]==1)
    {
        include ("../../planes/operaciones/planes_hosteo.php");       
    } 
    
echo "<hr>"; 

 // Listado de Clientes con Licencias.

if ($_SESSION["Tip"]==1)
    {
        include ("../../licencias/main/licencias_clientes_list.php");
    }
  
echo "<hr>";  

 // Listado de Clientes con Abono.

if ($_SESSION["Tip"]==1)
    {
        include ("../../planes/operaciones/planes_abono.php");
    }    
/*******************************************/
    
include ("../../includes/foot.inc");

} 
### FIN DE mainsus ###
?>
