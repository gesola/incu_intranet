<?php
header('content-type:text/css');
 
include ("css_conf.php");

echo <<<FINCSS

body {
	background-color: $background;
	color: $menu1;
}


      table {
/*
        background-color: black; 
        border: 0px #003366 solid;
*/
        border-collapse: collapse;
		border-color: $menu1; 
		background-color: $fondo;
      }

th {
        border: 0px outset silver;
        background-color: $th_strip;
        color: $txt1;
        border-bottom: 2px $menu1 solid;
        border-collapse: collapse;
/*
	border-bottom-width: thick;
	border-bottom-style: solid;
	border-bottom-color: blue;
*/
}

tr {
        background-color: #E0ECF8;
}

tr.striped {
	background-color: $menu1;
	color: $fondo;
        border-bottom: 2px $th_strip solid;
        border-top: 2px $th_strip solid;
}


td { 
	font-size: 12px;
}


hr { 
	color: $menu1;
}

select { 
	background-color: $txt1;
	font-size: 12px;
	color: $menu1;
}

textarea { 
	background-color: $txt1;
	font-size: 12px;
	color: $menu1;
}

textarea[readonly] {
    border-width: 0px; 
    font-size: 12px; 
    #font-weight: bold;
    background-color: $fondo;        
}        
        
input { 
	background-color: $txt1;
	font-size: 12px;
	color: $menu1;
}
        
input[readonly] {
        border-width: 0px;         
	background-color: $fondo;
	font-size: 12px;
}

   
pre { 
	color: $menu1;
	font-size: 12px;
	
}

        
h1 {  /* Titulo encabezado */
	margin: 10px 0px 0px 0px;
	color: $txt1;
	background-color: $menu1;
	font-size: 30px;
}


h2 { /* Mensajes de error - Titulo */
	margin: 10px 0px 0px 0px;
	color: $menu1;
	font-size: 18px;
}

h3 { /* Mensajes de error - Detalle */
	margin: 0px 0px 0px 0px;
	color: $txt2;
	font-size: 14px;
}

h4 { /* Título centrado  */
	margin: 0px 0px 0px 0px;
	padding: 10px 0px 0px 0px;
	color: $menu1;
	font-size: 24px;
	background-color: $fondo;
	text-align: center;
}

a {
	color: $menuhov; 
	text-decoration:none;
}


.foot { 
		font-size: 14px;
}

.footbarra { 
		background-color: $menu1;
		font-size: 14px;
		color: $fondo;
}

.boton{
	font-size: 16px;
	font-family: Georgia; 
	font-style: italic; 
	font-weight: bold;
	color: $menu1;
        background:inherit;
        border:0px;
        /*width:80px;*/
        /*height:19px;*/
       }

  .boton:hover{
	font-size: 16px;
	font-family: Georgia; 
	font-style: italic; 
	font-weight: bold;
	color: $menuhov;
    background:inherit;
    border:0px;
    /*width:80px;*/
    /*height:19px;*/
}

#boton {
	font-size: 16px;
	font-family: Georgia; 
	font-style: italic; 
	font-weight: bold;
	color: $menu1;
        background:inherit;
        border:0px;
}

#boton:hover{
	font-size: 16px;
	font-family: Georgia; 
	font-style: italic; 
	font-weight: bold;
	color: $menuhov;
    background:inherit;
    border:0px;
}

#boton2 {
	font-size: 13px;
	font-family: Georgia; 
	font-style: italic; 
	font-weight: bold;
	color: $menu1;
    background:inherit;
    border:0px;
}

#boton2:hover{
	font-size: 13px;
	font-family: Georgia; 
	font-style: italic; 
	font-weight: bold;
	color: $menuhov;
    background:inherit;
    border:0px;
}

/* Fuentes para detalles de registros (Ver) */
#vert {
	font-size: 16px;
	font-family: Times New Roman; 
	font-style: normal; 
	font-weight: bold;
	color: $menu1;
}

#verb {
	font-size: 16px;
	font-family: Times New Roman; 
	font-style: normal; 
	font-weight: normal;
	color: $menu1;
        background:inherit;
}
/* Fin Ver */

#tliq {
	font-size: 20px;
	font-family: Times New Roman; 
	font-style: italic; 
	font-weight: bold;
	color: $menu1;
}

/* Ventana de aviso */
#mask {
  position:absolute;
  left:0;
  top:0;
  z-index:9000;
  background-color: $menu1;
  display:none;
}
  
#boxes .window {
  position:absolute;
  left:0;
  top:0;
  width:440px;
  height:50px;
  display:none;
  z-index:9999;
  padding:20px;
}

#boxes #dialog {
  width:300px; 
  height:100px;
  padding:10px;
  background-color: $txt1;
	border-width: 5px;
	border-style: outset;
	border-color: $menu1;
}
/* Fin ventana de aviso */

FINCSS;
?>