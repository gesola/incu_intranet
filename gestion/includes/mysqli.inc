<?php 

########################################################
##                                                    ##
##           ACTUALIZACIONES DE VERSION               ##
##                                                    ##
##  - Actualización 07-09-2014 - GES - Version 4.0    ##
##                                                    ##
########################################################
##													  ##
## Descripcion: Parámetros para acceder a la Base de  ##
## Datos.  											  ##
##													  ##
########################################################

### ARCHIVO QUE REALIZA LA CONEXION CON LA BASE DE DATOS ###

$confXML = simplexml_load_file(dirname(__FILE__) . "/../config/config.xml");
$host = $confXML->mysql->connect->host;
$user = $confXML->mysql->connect->user;
$passwd = $confXML->mysql->connect->passwd;
$database = $confXML->mysql->connect->database;

$host2 = $confXML->mysql->connect->host2;
$user2 = $confXML->mysql->connect->user2;
$passwd2 = $confXML->mysql->connect->passwd2;
$database2 = $confXML->mysql->connect->database2;

$host3 = $confXML->mysql->connect->host3;
$user3 = $confXML->mysql->connect->user3;
$passwd3 = $confXML->mysql->connect->passwd3;
$database3 = $confXML->mysql->connect->database3;
	
 
$connect = new mysqli($host, $user, $passwd, $database);
$connect2 = new mysqli($host2, $user2, $passwd2, $database2);
$connect3 = new mysqli($host3, $user3, $passwd3, $database3);

$connect->set_charset("utf8");
$connect2->set_charset("utf8");
$connect3->set_charset("utf8");

if (mysqli_connect_errno()) {
printf("Connect failed: %s\n", mysqli_connect_error());
exit();
} 
 
?>
